<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");

?>

<? $APPLICATION->IncludeComponent(
    "susu:birthdays.friends",
    "list",
    Array(
        "DAYS_AFTER" => "60",
        "FORMAT" => "j F",
        "TITLE" => "Дни рождения друзей",
        "TITLE_LINK" => "/friends-birthdays/",
        "COUNT_ITEMS" => 99,
        "THUMBNAIL_LIST_SIZE" => 42,
        "PATH_TO_USER" => '/club/user/#user_id#/',
        "NAME_TEMPLATE" => "#NAME# #LAST_NAME#",
    )
); ?>


    <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>