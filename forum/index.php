<? require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


$request = \Bitrix\Main\Context::getCurrent()->getRequest();

switch ($request["FID"]) {
    case 1:
        // news
        $arFilter = array('IBLOCK_ID' => IBLOCK_ID__NEWS, 'PROPERTY_35_VALUE' => $request["TID"]);
        break;
    case 5:
        // photogallery
        $arFilter = array('IBLOCK_ID' => IBLOCK_ID__GALLERY, 'PROPERTY_42_VALUE' => $request["TID"]);
        break;
    case 6:
        // photogallery_user
        $arFilter = array('IBLOCK_ID' => IBLOCK_ID__GALLERY_USER, 'PROPERTY_44_VALUE' => $request["TID"]);
        break;
    default:
        die();
}

CModule::includeModule('iblock');
$res = CIBlockElement::GetList(array('ID' => 'ASC'), $arFilter);
if ($el = $res->getNext()) {
    LocalRedirect(str_replace('//','/', '/' . $el["DETAIL_PAGE_URL"] . '#message' . $request["MID"]));
}