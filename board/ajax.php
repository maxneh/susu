<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if (!\Bitrix\Main\Context::getCurrent()->getRequest()->isAjaxRequest()) die;
?>

<?
global $arrBoardFilter;
$filterName = 'arrBoardFilter';
$arSections = array();
foreach ($_REQUEST[$filterName . '_ff']['SECTION_ID'] as $section){
    $arSections[] = htmlspecialcharsbx($section);
}

$arrBoardFilter = Array(
    'SECTION_ID'=>$arSections,
    '>=DATE_ACTIVE_FROM'=>htmlspecialcharsbx($_REQUEST[$filterName . '_DATE_ACTIVE_FROM_1']),
    "<=DATE_ACTIVE_FROM"=>htmlspecialcharsbx($_REQUEST[$filterName . '_DATE_ACTIVE_FROM_2']),
);
?>



<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "board",
    Array(
        "ADD_ELEMENT_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "Y",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_SHADOW" => "Y",
        "AJAX_OPTION_STYLE" => "Y",
        "BROWSER_TITLE" => "BROWSER_TITLE",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CATEGORY_CODE" => "THEMES",
        "CATEGORY_IBLOCK" => array(
            0 => "3",
        ),
        "CATEGORY_ITEMS_COUNT" => "4",
        "CATEGORY_THEME_#ID(XML_ID=content-news)#" => "list",
        "CATEGORY_THEME_3" => "list",
        "CHECK_DATES" => "Y",
        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
        "DETAIL_DISPLAY_TOP_PAGER" => "N",
        "DETAIL_FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "DETAIL_PAGER_SHOW_ALL" => "N",
        "DETAIL_PAGER_TEMPLATE" => "",
        "DETAIL_PAGER_TITLE" => "Страница",
        "DETAIL_PROPERTY_CODE" => array(
            0 => "SOURCE",
            1 => "PHOTO",
            2 => "",
        ),
        "DETAIL_SET_CANONICAL_URL" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "N",
        "DISPLAY_PANEL" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FILTER_FIELD_CODE" => array(
            0 => "DATE_ACTIVE_FROM",
            1 => "SECTION_ID",
            2 => "",
        ),
        "FILTER_NAME" => $filterName,
        "FILTER_PROPERTY_CODE" => array(
            0 => "",
            1 => "",
        ),
        "FORUM_ID" => "",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "12",
        "IBLOCK_TYPE" => "services",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
        "LIST_FIELD_CODE" => array(
            0 => "DETAIL_TEXT",
            1 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "PHOTO",
            1 => "",
        ),
        "MESSAGES_PER_PAGE" => "10",
        "MESSAGE_404" => "",
        "META_DESCRIPTION" => "-",
        "META_KEYWORDS" => "-",
        "NEWS_COUNT" => "5",
        "NUM_DAYS" => "360",
        "NUM_NEWS" => "20",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_TITLE" => "Объявления",
        "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
        "PREVIEW_TRUNCATE_LEN" => "400",
        "REVIEW_AJAX_POST" => "Y",
        "SEF_FOLDER" => "/board/",
        "SEF_MODE" => "Y",
        "SET_LAST_MODIFIED" => "N",
        "SET_STATUS_404" => "Y",
        "SET_TITLE" => "Y",
        "SHOW_404" => "N",
        "SHOW_LINK_TO_FORUM" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "Y",
        "URL_TEMPLATES_READ" => "#SECTION_CODE#/#ELEMENT_CODE#/#message#TID#",
        "USE_CAPTCHA" => "N",
        "USE_CATEGORIES" => "Y",
        "USE_FILTER" => "N",
        "USE_PERMISSIONS" => "N",
        "USE_RATING" => "N",
        "USE_REVIEW" => "N",
        "USE_RSS" => "N",
        "USE_SEARCH" => "N",
        "USE_SHARE" => "N",
        "YANDEX" => "N",
        "COMPONENT_TEMPLATE" => "board",
        "FILE_404" => "",
        "SEF_URL_TEMPLATES" => array(
            "news" => "",
            "section" => "#SECTION_CODE#/",
            "detail" => "#SECTION_CODE#/#ELEMENT_CODE#/",
        )
    ),
    false
);?>
