<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.profile",
    ".default",
    array(
        "SET_TITLE" => "N",
        "COMPONENT_TEMPLATE" => ".default",
        "USER_PROPERTY" => array(
            "UF_USER_DEPARTMENT"
        ),
        "SEND_INFO" => "N",
        "CHECK_RIGHTS" => "N",
        "USER_PROPERTY_NAME" => ""
    ),
    false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>