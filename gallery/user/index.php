<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Фотогалерея");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");

$galleryCode = false;

if ($GLOBALS["USER"]->IsAuthorized()) {

    CModule::IncludeModule("iblock");
    $rsSect = CIBlockSection::GetList(array('DEPTH_LEVEL' => 'ASC'), array('IBLOCK_ID' => IBLOCK_ID__GALLERY_USER, 'CREATED_BY' => $USER->GetID()),
        false, array('ID',
            'CODE', 'DEPTH_LEVEL'));
    $rsSect->NavStart(1);
    while ($arSect = $rsSect->GetNext()) {
        $galleryCode = $arSect['CODE'];
    }
}

?>

    <div class="page-action__content page-action__content_upload">
        <? if (!$galleryCode) {
            ?>
            <div class="page-action__content page-action__content_upload">
                <p><br></p>
                <p>
                    У вас еще нет своей галереи, вы можете создать ее.
                </p>
                <a rel="nofollow" class=" btn_white" href="/gallery/user/NEW_ALIAS/action/CREATE/">Создать свою галерею</span></a>
                <p><br></p>
            </div>
        <? } else { ?>

            <? $APPLICATION->IncludeComponent(
                "bitrix:photogallery.upload",
                ".default",
                array(
                    "HLBLOCK_ID" => HLBLOCK_ID__GALLERY_CAT_USER,
                    "ALBUM_PHOTO_THUMBS_WIDTH" => "120",
                    "IBLOCK_ID" => IBLOCK_ID__GALLERY_USER,
                    "IBLOCK_TYPE" => "gallery",
                    "INDEX_URL" => "index.php",
                    "JPEG_QUALITY" => "100",
                    "JPEG_QUALITY1" => "100",
                    "MODERATION" => "N",
                    "ORIGINAL_SIZE" => "1280",
                    "PATH_TO_FONT" => "default.ttf",
                    "SECTION_URL" => "/gallery/user/#USER_ALIAS#/#SECTION_ID#/",
                    "USER_ALIAS" => $galleryCode,
                    "BEHAVIOUR" => "USER",
                    "SET_TITLE" => "N",
                    "THUMBNAIL_SIZE" => "90",
                    "UPLOAD_MAX_FILE_SIZE" => "7",
                    "USE_WATERMARK" => "N",
                    "WATERMARK_MIN_PICTURE_SIZE" => "800",
                    "WATERMARK_RULES" => "USER",
                    "WATERMARK_TYPE" => "BOTH",
                    "COMPONENT_TEMPLATE" => ".default"
                ),
                false
            ); ?>
        <? } ?>
    </div>

<? $APPLICATION->IncludeComponent(
    "bitrix:photogallery_user",
    ".default",
    array(
        "ADDITIONAL_SIGHTS" => array(),
        "ALBUM_PHOTO_SIZE" => "340",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "COMMENTS_COUNT" => "10",
        "COMMENTS_TYPE" => "forum",
        "DATE_TIME_FORMAT_DETAIL" => "d.m.Y",
        "DATE_TIME_FORMAT_SECTION" => "d.m.Y",
        "DRAG_SORT" => "Y",
        "ELEMENTS_PAGE_ELEMENTS" => "50",
        "ELEMENT_SORT_FIELD" => "sort",
        "ELEMENT_SORT_ORDER" => "desc",
        "FORUM_ID" => "6",
        "IBLOCK_ID" => IBLOCK_ID__GALLERY_USER,
        "IBLOCK_TYPE" => "gallery",
        "HLBLOCK_ID" => HLBLOCK_ID__GALLERY_CAT_USER,
        "JPEG_QUALITY" => "100",
        "JPEG_QUALITY1" => "100",
        "NAME_TEMPLATE" => "",
        "ORIGINAL_SIZE" => "1280",
        "PAGE_NAVIGATION_TEMPLATE" => "",
        "PATH_TO_FONT" => "default.ttf",
        "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
        "PATH_TO_USER" => "",
        "PHOTO_LIST_MODE" => "Y",
        "POST_FIRST_MESSAGE" => "N",
        "PREORDER" => "N",
        "SECTION_LIST_THUMBNAIL_SIZE" => "340",
        "SECTION_PAGE_ELEMENTS" => "15",
//		"SECTION_SORT_BY" => "UF_DATE",
        "SECTION_SORT_BY" => "NAME",
        "SECTION_SORT_ORD" => "ASC",
        "SEF_FOLDER" => "/gallery/user/",
        "SEF_MODE" => "Y",
        "SET_TITLE" => "N",
        "SHOWN_ITEMS_COUNT" => "10",
        "SHOW_LINK_ON_MAIN_PAGE" => array(
            0 => "id",
            1 => "shows",
            2 => "rating",
            3 => "comments",
        ),
        "SHOW_NAVIGATION" => "N",
        "SHOW_TAGS" => "N",
        "THUMBNAIL_SIZE" => "340*200",
        "THUMBNAIL_SIZE_EDIT" => "150",
        "UPLOAD_MAX_FILE_SIZE" => "100",
        "URL_TEMPLATES_PROFILE_VIEW" => "",
        "URL_TEMPLATES_READ" => "",
        "USE_CAPTCHA" => "N",
        "USE_COMMENTS" => "Y",
        "USE_LIGHT_VIEW" => "N",
        "USE_RATING" => "Y",
        "USE_WATERMARK" => "N",
        "WATERMARK_MIN_PICTURE_SIZE" => "800",
        "WATERMARK_RULES" => "USER",
        "COMPONENT_TEMPLATE" => ".default",
        "GALLERY_GROUPS" => array(
            0 => "6",
        ),
        "ONLY_ONE_GALLERY" => "Y",
        "MODERATION" => "N",
        "ANALIZE_SOCNET_PERMISSION" => "Y",
        "GALLERY_AVATAR_SIZE" => "50",
        "ALBUM_PHOTO_THUMBS_SIZE" => "120",
        "INDEX_PAGE_TOP_ELEMENTS_COUNT" => "45",
        "SHOW_ONLY_PUBLIC" => "N",
        "MODERATE" => "N",
        "SHOW_CONTROLS_BUTTONS" => "Y",
 /*       "MAX_VOTE" => "5",
        "VOTE_NAMES" => array(
            0 => "1",
            1 => "2",
            2 => "3",
            3 => "4",
            4 => "5",
            5 => "",
        ),
        "DISPLAY_AS_RATING" => "rating",*/
        "SHOW_LINK_TO_FORUM" => "N",
        "SEF_URL_TEMPLATES" => array(
            "index" => "index.php",
            "galleries" => "galleries/#USER_ID#/",
            "gallery" => "#USER_ALIAS#/",
            "gallery_edit" => "#USER_ALIAS#/action/#ACTION#/",
            "section" => "#USER_ALIAS#/#SECTION_ID#/",
            "section_edit" => "#USER_ALIAS#/#SECTION_ID#/action/#ACTION#/",
            "section_edit_icon" => "#USER_ALIAS#/#SECTION_ID#/icon/action/#ACTION#/",
            "upload" => "#USER_ALIAS#/#SECTION_ID#/action/upload/",
            "detail" => "#USER_ALIAS#/#SECTION_ID#/#ELEMENT_ID#/",
            "detail_edit" => "#USER_ALIAS#/#SECTION_ID#/#ELEMENT_ID#/action/#ACTION#/",
            "detail_slide_show" => "#USER_ALIAS#/#SECTION_ID#/#ELEMENT_ID#/slide_show/",
            "detail_list" => "list/",
            "search" => "search/",
            "tags" => "tags/",
        )
    ),
    false
); ?>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>