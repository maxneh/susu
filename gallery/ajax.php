<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if (!\Bitrix\Main\Context::getCurrent()->getRequest()->isAjaxRequest()) die;
?>

<?
global $arrFilterGallery;
$filterName = 'arrFilterGallery';
$arSections = array();
foreach ($_REQUEST[$filterName . '_UF_GALLERY_CAT'] as $section){
    $arSections[] = htmlspecialcharsbx($section);
}

$arrFilterGallery = Array(
    'UF_GALLERY_CAT'=>$arSections,
    '>=UF_DATE'=>htmlspecialcharsbx($_REQUEST[$filterName . '_>=UF_DATE']),
    "<=UF_DATE"=>htmlspecialcharsbx($_REQUEST[$filterName . '_<=UF_DATE']),
);
?>


<?$APPLICATION->IncludeComponent(
    "susu:photogallery.section.list",
    "",
    Array(
        'USE_FILTER'=>'N',
        'FILTER_NAME' => $filterName,
        "ADDITIONAL_SIGHTS" => array(),
        "ALBUM_PHOTO_SIZE" => "340",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "COMMENTS_COUNT" => "10",
        "COMMENTS_TYPE" => "forum",
        "DATE_TIME_FORMAT_DETAIL" => "d.m.Y",
        "DATE_TIME_FORMAT_SECTION" => "d.m.Y",
        "DRAG_SORT" => "Y",
        "ELEMENTS_PAGE_ELEMENTS" => "50",
        "ELEMENT_SORT_FIELD" => "sort",
        "ELEMENT_SORT_ORDER" => "desc",
        "FORUM_ID" => "5",
        "IBLOCK_ID" => IBLOCK_ID__GALLERY,
        "IBLOCK_TYPE" => "gallery",
        "HLBLOCK_ID" => HLBLOCK_ID__GALLERY_CAT,
        "JPEG_QUALITY" => "100",
        "JPEG_QUALITY1" => "100",
        "NAME_TEMPLATE" => "",
        "ORIGINAL_SIZE" => "1280",
        "PAGE_NAVIGATION_TEMPLATE" => "",
        "PATH_TO_FONT" => "default.ttf",
        "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
        "PATH_TO_USER" => "",
        "PHOTO_LIST_MODE" => "Y",
        "POST_FIRST_MESSAGE" => "N",
        "PREORDER" => "N",
        "SECTION_LIST_THUMBNAIL_SIZE" => "340",
        "SECTION_PAGE_ELEMENTS" => "15",
//        "SECTION_SORT_BY" => "UF_DATE",
        "SECTION_SORT_BY" => "ID",
        "SECTION_SORT_ORD" => "DESC",
        "SEF_FOLDER" => "/gallery/",
        "SEF_MODE" => "Y",
        "SET_TITLE" => "N",
        "SHOWN_ITEMS_COUNT" => "10",
        "SHOW_LINK_ON_MAIN_PAGE" => array(
            0 => "id",
            1 => "shows",
            2 => "rating",
            3 => "comments",
        ),
        "SHOW_NAVIGATION" => "N",
        "SHOW_TAGS" => "N",
        "THUMBNAIL_SIZE" => "340*200",
        "THUMBNAIL_SIZE_EDIT" => "150",
        "UPLOAD_MAX_FILE_SIZE" => "100",
        "URL_TEMPLATES_PROFILE_VIEW" => "",
        "URL_TEMPLATES_READ" => "",
        "USE_CAPTCHA" => "N",
        "USE_COMMENTS" => "Y",
        "USE_LIGHT_VIEW" => "N",
        "USE_RATING" => "Y",
        "USE_WATERMARK" => "N",
        "WATERMARK_MIN_PICTURE_SIZE" => "800",
        "WATERMARK_RULES" => "USER",
        "COMPONENT_TEMPLATE" => ".default",
        "SEF_URL_TEMPLATES" => array(
            "index" => "index.php",
            "gallery" => "#USER_ALIAS#/",
            "section" => "#SECTION_ID#/",
            "section_edit" => "#SECTION_ID#/action/#ACTION#/",
            "section_edit_icon" => "#SECTION_ID#/icon/action/#ACTION#/",
            "upload" => "#SECTION_ID#/action/upload/",
            "detail" => "#SECTION_ID#/#ELEMENT_ID#/",
            "detail_edit" => "#SECTION_ID#/#ELEMENT_ID#/action/#ACTION#/",
            "detail_list" => "list/",
            "search" => "search/",
        ),

    ),
    $component,
    array("HIDE_ICONS" => "Y")
)
?>

<script>
    // $(document).find('.preview-album-slider').slick('reinit');
 /*   $(document).find('.preview-album-slider').slick({
        dots: true,
        autoplay: false,
        fade: true,
        autoplaySpeed: 400,
        arrows: false,

    });*/
</script>