<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Услуги университета");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>

    <div class="uslugi-page">
        <h2 class="uslugi-page__title  blue-text">Спорткомплекс</h2>
        <div class="row uslugi-page__hero">
            <div class="col-xs-12 col-md-6">
                <div class="contact-card">
                    <h4 class="contact-card__title">Контакты</h4>
                    <div class="contact-card__text">
                        Челябинск, ул. Сони Кривой, 60<br>
                        Тел.: +7 267-97-59 (дежурный администратор на первом этаже спорткомплекса)<br>
                        info@usk-sport.ru – приёмная директора<br>
                        reklama@usk-sport.ru – рекламный отдел<br>
                        www.usk-sport.ru
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <img src="/images/services/sport.jpg" alt="" class="img-responsive w100">
            </div>
        </div>
        <p class="uslugi-page__description">
            Предлагаем Вам все удовольствия в нашем спорткомплексе ЮУрГУ! Бассейн — свободное плавание, услуги по обучению плаванию, детская школа
            плавания «Юника», фитнес — все групповые программы, тренажерный центр «Олимпия», Аквааэробика, сауны с выходом на бассейн, VIP-сауны,
            детский спортивный клуб «Олимпик», манеж и теннисные корты, массаж, солярии, Студия красоты «Magic» — Вы обязательно найдете занятие себе
            по душе! Ждем Вас!
        </p>
        <p>Наш Спорткомплекс самый крупный в городе, и включает в себя много подразделений и различных услуг:</p>
        <p>– Плавательный бассейн 25 х 50 метров;</p>
        <p>– Наш Спорткомплекс самый крупный в городе, и включает в себя много подразделений и различных услуг:</p>
        <p>– Плавательный бассейн 25 х 50 метров;</p>
        <p>– Тренажерный центр «Олимпия»;</p>
        <p>– Сауны (компакт, «Люкс», «5 элемент», «Робинзон»);</p>
        <p>– Зал групповых программ;</p>
        <p>– Теннисные корты;</p>
        <p>– Легкоатлетический манеж;</p>
        <p>– Детский клуб "Олимпик";</p>
        <p>– Услуги массажиста, солярии;</p>
        <p>– Научно-исследовательский центр спортивной науки;</p>
        <p>– Студия красоты «Magic». Тренажерный центр «Олимпия»;</p>
        <p>– Сауны (компакт, «Люкс», «5 элемент», «Робинзон»);</p>
        <p>– Зал групповых программ;</p>
        <p>– Теннисные корты;</p>
        <p>– Легкоатлетический манеж;</p>
        <p>– Детский клуб "Олимпик";</p>
        <p>– Услуги массажиста, солярии;</p>
        <p>– Научно-исследовательский центр спортивной науки;</p>
        <p>– Студия красоты «Magic».</p>

        <br>
        <h2 class="blue-text">Прайс</h2>
        <p class="uslugi-page__description">Легкоатлетический манеж, теннисные корты, У-ШУ, бокс, ОФП</p>
        <div class="table-wrapper">
        <table class="table">
            <thead>
            <tr>
                <th>Группа</th>
                <th></th>
                <th>Стоимость одного занятия, руб.</th>
                <th>Стоимость абонемента, руб.</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Общая физическая подготовка для детей</td>
                <td></td>
                <td>150</td>
                <td>1 200</td>
            </tr>
            <tr>
                <td>Гимнастика У-ШУ</td>
                <td>Группа 1 <br>
                    Группа 2<br>
                    Группа 3<br>
                    Группа 4<br>
                    Группа 5
                </td>
                <td>125<br>
                    125<br>
                    125<br>
                    125<br>
                    125
                </td>
                <td>1 500<br>
                    1 000<br>
                    1 500<br>
                    1 000<br>
                    1 000
                </td>
            </tr>
            <tr>
                <td>Теннис</td>
                <td>Группа 1<br>
                    Группа 2<br>
                    Группа 3
                </td>
                <td>280<br>
                    250<br>
                    280
                </td>
                <td>2 500<br>
                    3 000<br>
                    2 500
                </td>
            </tr>
            <tr>
                <td>Бокс</td>
                <td></td>
                <td>170</td>
                <td>1 360</td>

            </tr>
            </tbody>
        </table>
        </div>
    </div>
<?$APPLICATION->IncludeComponent(
    "bitrix:form",
    "services",
    Array(
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_ADDITIONAL" => "N",
        "EDIT_STATUS" => "N",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "NOT_SHOW_FILTER" => array("",""),
        "NOT_SHOW_TABLE" => array("",""),
        "RESULT_ID" => "",
        "SEF_MODE" => "N",
        "SHOW_ADDITIONAL" => "N",
        "SHOW_ANSWER_VALUE" => "N",
        "SHOW_EDIT_PAGE" => "N",
        "SHOW_LIST_PAGE" => "N",
        "SHOW_STATUS" => "N",
        "SHOW_VIEW_PAGE" => "N",
        "START_PAGE" => "new",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => Array("action"=>"action"),
        "WEB_FORM_ID" => "3"
    )
);?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>