<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Услуги университета");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>

    <div class="uslugi-page">
        <h2 class="uslugi-page__title blue-text">Издательский центр</h2>
        <div class="row uslugi-page__hero">
            <div class="col-xs-12 col-md-6">
                <div class="contact-card">
                    <h4 class="contact-card__title">Контакты</h4>
                    <div class="contact-card__text">
                        Челябинск, ул. Сони Кривой, 79<br>
                        Тел.: +7 (351) 267-98-50, 267-99-67<br>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <img src="/images/services/izdal.jpg" alt="" class="img-responsive w100">
            </div>
        </div>

        <p class="uslugi-page__description">Издательский центр &mdash; это предприятие полного цикла, которое осуществляет:</p>
        <p class="uslugi-page__description">&ndash; редактуру, корректуру, верстку рукописей;</p>
        <p class="uslugi-page__description">&ndash; дизайнерское оформление материалов;</p>
        <p class="uslugi-page__description">&ndash; изготовление всевозможной учебно-методической и научной литературы.</p>
        <br>
        <p class="uslugi-page__description">Особенность Издательского центра &ndash; выпуск уникальных высокохудожественных печатных изданий,
            например:</p>
        <p>&ndash; монография В.Ю. Юдина &laquo;Деревянные иконостасы Урала&raquo; завоевала Гран-при областного конкурса &laquo;Южноуральская книга
            &ndash; 2009&raquo; в номинации &laquo;Издано на Южном Урале&raquo;.</p>
        <p>&ndash; &laquo;История ЮУрГУ в документах и материалах&raquo; &ndash; научное исследование, посвященное эпохе становления и развития
            университета.</p>
        <p>&ndash; &laquo;Моя судьба меня хранила&raquo; &ndash; иллюстрированное биографическое эссе о жизни и творчестве примы-балерины Челябинского
            академического театра оперы и балета им. М.И. Глинки Галины Михайловны Борейко.</p>
        <br>
        <p class="uslugi-page__description">Издательский центр ЮУрГУ выпускает весь спектр научной, учебной и методической литературы:</p>
        <p>&ndash; монографии;</p>
        <p>&ndash; учебные пособия;</p>
        <p>&ndash; методические материалы и руководства;</p>
        <p>&ndash; курсы лекций;</p>
        <p>&ndash; материалы конференций, сборники докладов;</p>
        <p>&ndash; научные журналы и многое другое.</p>
        <p>При необходимости изданиям присваивается ISBN.</p>
        <br>
        <p class="uslugi-page__description">Издательский центр изготавливает рекламную и имиджево-презентационную полиграфическую продукцию:</p>
        <p>&ndash; папки (альбом) выпускников;</p>
        <p>&ndash; афиши;</p>
        <p>&ndash; открытки;</p>
        <p>&ndash; приглашения;</p>
        <p>&ndash; фирменные конверты (в т.ч. именные).</p>
        <p>Издательским центром ЮУрГУ реализуется механизм &laquo;печати по требованию&raquo; &ndash; при наличии оригинал-макета в электронном виде
            материалы могут быть изготовлены, а также допечатаны в любое время по необходимости, любым тиражом.</p>
        <p>В издательском центре внедрена система контроля качества на всех этапах изготовления заказа.</p>
        <p>Есть возможность передачи данных (оригинал-макетов) в электронном виде.</p>
    </div>

<?$APPLICATION->IncludeComponent(
    "bitrix:form",
    "services",
    Array(
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_ADDITIONAL" => "N",
        "EDIT_STATUS" => "N",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "NOT_SHOW_FILTER" => array("",""),
        "NOT_SHOW_TABLE" => array("",""),
        "RESULT_ID" => "",
        "SEF_MODE" => "N",
        "SHOW_ADDITIONAL" => "N",
        "SHOW_ANSWER_VALUE" => "N",
        "SHOW_EDIT_PAGE" => "N",
        "SHOW_LIST_PAGE" => "N",
        "SHOW_STATUS" => "N",
        "SHOW_VIEW_PAGE" => "N",
        "START_PAGE" => "new",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => Array("action"=>"action"),
        "WEB_FORM_ID" => "3"
    )
);?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>