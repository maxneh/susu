<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Услуги университета");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>

    <div class="uslugi-page">
        <h2 class="uslugi-page__title blue-text">ИСТиС - Тур</h2>
        <div class="row uslugi-page__hero">
            <div class="col-xs-12 col-md-6">
                <div class="contact-card">
                    <h4 class="contact-card__title">Контакты</h4>
                    <div class="contact-card__text">
                        Челябинск, ул. Сони Кривой 60, офис 2<br>
                        Тел. круглосуточной поддержки туристов: 8 922 019-33-33<br>
                        Российские и международные туры: +7 (351) 272-30-50<br>
                        istis@istis-tour.ru<br>
                        www.istis-tour.ru
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <img src="/images/services/tur.jpg" alt="" class="img-responsive w100">
            </div>
        </div>


        <p class="uslugi-page__description">Туристическая компания &laquo;ИСТиС-Тур&raquo; была открыта в ноябре 2014 года на базе Института спорта,
            туризма и сервиса ЮУрГУ.</p>
        <p>Идейным вдохновителем и организатором стал Эрлих Вадим Викторович &ndash; директор Института спорта, туризма и сервиса, человек хорошо
            известный в бизнес-кругах Челябинска и работающий в системе высшего профессионального образования на протяжении 13 лет.</p>
        <p>ИСТиС-Тур считается уникальным проектом на рынке туристических услуг, так как имеет прочную государственную поддержку и работает как малое
            инновационное предприятие национального исследовательского университета.</p>
    </div>
<?$APPLICATION->IncludeComponent(
    "bitrix:form",
    "services",
    Array(
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_ADDITIONAL" => "N",
        "EDIT_STATUS" => "N",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "NOT_SHOW_FILTER" => array("",""),
        "NOT_SHOW_TABLE" => array("",""),
        "RESULT_ID" => "",
        "SEF_MODE" => "N",
        "SHOW_ADDITIONAL" => "N",
        "SHOW_ANSWER_VALUE" => "N",
        "SHOW_EDIT_PAGE" => "N",
        "SHOW_LIST_PAGE" => "N",
        "SHOW_STATUS" => "N",
        "SHOW_VIEW_PAGE" => "N",
        "START_PAGE" => "new",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => Array("action"=>"action"),
        "WEB_FORM_ID" => "3"
    )
);?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>