<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Услуги университета");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>
<div class="uslugi-page">
	<h2 class="uslugi-page__title blue-text">Медцентр</h2>
	<div class="row uslugi-page__hero">
		<div class="col-xs-12 col-md-6">
			<div class="contact-card">
				<h4 class="contact-card__title">Контакты</h4>
				<div class="contact-card__text">
					 Челябинск, ул. Сони Кривой 60, 2 этаж<br>
					 Тел.: +7 (351) 261-91-43<br>
					 Пр. Ленина, 80, Тел.: +7(351) 267-93-82<br>
 <a href="mailto:medcentrsusu@mail.ru">medcentrsusu@mail.ru</a><br>
 <a href="mailto:ksf@susu.ac.ru">ksf@susu.ac.ru</a><br>
					 vk.com/medcentrsusu<br>
 <a href="http://www.medcentr.susu.ru">www.medcentr.susu.ru</a><br>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
 <img src="/images/services/med.jpg" alt="" class="img-responsive w100">
		</div>
	</div>
	<p class="uslugi-page__description">
		 Медицинский центр Южно-Уральского государственного университета был основан в 1999 году. Основными направлениями работы центра является оказание амбулаторно- поликлинической помощи, лечение острых и хронических заболеваний.
	</p>
	<h3>Амбулаторно-поликлиническое отделение</h3>
	<p>
		 Амбулаторно-поликлииническое отделение основано в 2009 году. В структуру отделения входят процедурный кабинет (все виды лабороторных анализов, инъекции, капельницы), физиотерапевтический кабинет (лазеротерапия, магнитотерапия, ингаляторий, СМТ терапия, УФО терапия, фонофорез).
	</p>
	<p>
		 Прием ведут: кардиолог (ЭКГ, суточное мониторирование ЭКГ и АД), невролог, врач общей практики (офтальмологический и лор приёмы), терапевт.
	</p>
	<p>
		 Экспертиза временной нетрудоспособности.
	</p>
	<p>
		 Предварительные и переодические медицинские осмотры с оформлением санитарной книжки, профосмотры.
	</p>
	<p>
		 Приём по предварительной записи.
	</p>
	<h3>Реабилитационное отделение</h3>
	<p>
		 Кабинет мануальной терапии, массажа, озонотерапии. Приём ведут врачи высшей категории: мануальный терапевт, невролог, врач восстановительной медицины. Лечим остеохондроз позвоночника, межпозвоночные грыжи и их проявления, сколиотическую болезнь у детей. Проводятся процедуры лечебного, лимфодренажного, антицеллюлитного массажа. Используется методика постизометрической релаксации, чем добивается стойкая ремиссия.
	</p>
	<h3>Урологическое отделение</h3>
	<p>
		 Открыто в 2010 году. Приём ведут: уролог, хирург,акушер-гинеколог, врач ультразвуковой диагностики. Используются современные методики диагностики и лечения.
	</p>
	<h2 class="blue-text">Прайс</h2>
	<p class="uslugi-page__description">
		 Амбулаторно-поликлиническая помощь
	</p>
	<p>
		 При осуществлении специализированной медицинской помощи по мануальной терапии
	</p>
	<div class="table-wrapper">
	<table class="table">
	<thead>
	<tr>
		<th>
			 Наименование услуги
		</th>
		<th>
			 Единица измерения
		</th>
		<th>
			 Стоимость одной услуги без НДС, руб.
		</th>
		<th>
			 Стоимость одной услуги без НДС со скидкой, руб.
		</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td>
			 Консультация врача мануальной терапии (врач высшей категории)<br>
			 – первичная<br>
			 – повторная
		</td>
		<td>
 <br>
 <br>
			 1 конс.<br>
			 1 конс.
		</td>
		<td>
 <br>
 <br>
			 1 500<br>
			 800
		</td>
		<td>
 <br>
 <br>
			 1 350<br>
			 720
		</td>
	</tr>
	<tr>
		<td>
			 Консультация врача мануальной терапии<br>
			 – первичная<br>
			 – повторная
		</td>
		<td>
			 1 конс.<br>
			 1 конс.<br>
			 10 мин.
		</td>
		<td>
			 1 000<br>
			 700<br>
			 800
		</td>
		<td>
			 900<br>
			 630<br>
			 720
		</td>
	</tr>
	<tr>
		<td>
			 Мануальная терапия
		</td>
		<td>
			 20 мин.
		</td>
		<td>
			 1 500
		</td>
		<td>
			 1 350
		</td>
	</tr>
	<tr>
		<td>
			 Мануальная терапия методом краниосокральной остеропатии
		</td>
		<td>
			 20 мин.<br>
			 40 мин.
		</td>
		<td>
			 750<br>
			 1 500
		</td>
		<td>
			 680<br>
			 1350
		</td>
	</tr>
	<tr>
		<td>
			 Проведение лечебных блокад (без стоимости препарата):<br>
			 – миофасцикулярных гипертонусов:<br>
			 &nbsp;&nbsp;&nbsp;&nbsp;3 точки<br>
			 &nbsp;&nbsp;&nbsp;&nbsp;6 точек<br>
			 — лестничных мышц<br>
			 — грушевидной мышцы<br>
			 — паравертебральная<br>
			 — блокада с озонокислородной смесью
		</td>
		<td>
 <br>
 <br>
 <br>
			 1 манипул.<br>
			 1 манипул.<br>
			 1 манипул.<br>
			 1 манипул.<br>
			 1 манипул.<br>
			 1 манипул.<br>
		</td>
		<td>
 <br>
 <br>
 <br>
			 500<br>
			 700<br>
			 500<br>
			 1 000<br>
			 1 000<br>
			 600
		</td>
		<td>
 <br>
 <br>
			 450<br>
			 630<br>
			 450<br>
			 900<br>
			 900<br>
			 550
		</td>
	</tr>
	<tr>
		<td>
			 Периартикулярное введение озонокислородной смеси в область:<br>
			 — коленного сустава<br>
			 — локтевого сустава<br>
			 — голеностопного сустава
		</td>
		<td>
 <br>
 <br>
			 1 манипул.<br>
			 1 манипул.<br>
			 1 манипул.
		</td>
		<td>
 <br>
 <br>
			 1 000<br>
			 500<br>
			 800
		</td>
		<td>
 <br>
 <br>
			 900<br>
			 450<br>
			 720
		</td>
	</tr>
	</tbody>
	</table>
	</div>
</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:form",
	"services",
	Array(
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_ADDITIONAL" => "N",
		"EDIT_STATUS" => "N",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"NOT_SHOW_FILTER" => array("",""),
		"NOT_SHOW_TABLE" => array("",""),
		"RESULT_ID" => "",
		"SEF_MODE" => "N",
		"SHOW_ADDITIONAL" => "N",
		"SHOW_ANSWER_VALUE" => "N",
		"SHOW_EDIT_PAGE" => "N",
		"SHOW_LIST_PAGE" => "N",
		"SHOW_STATUS" => "N",
		"SHOW_VIEW_PAGE" => "N",
		"START_PAGE" => "new",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"VARIABLE_ALIASES" => Array("action"=>"action"),
		"WEB_FORM_ID" => "3"
	)
);?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>