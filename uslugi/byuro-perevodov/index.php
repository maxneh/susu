<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Услуги университета");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>

    <div class="uslugi-page">
        <h2 class="uslugi-page__title blue-text">Бюро переводов</h2>
        <div class="row uslugi-page__hero">
            <div class="col-xs-12 col-md-6">
                <div class="contact-card">
                    <h4 class="contact-card__title">Контакты</h4>
                    <div class="contact-card__text">
                        Челябинск, пр. Ленина, 76<br>
                        Аудитория 166 главного корпуса<br>
                        Тел.: +7(351) 272-32-09
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <img src="/images/services/perevod.jpg" alt="" class="img-responsive w100">
            </div>
        </div>
                <p class="uslugi-page__description">Институт международного образования является единственным агентством переводов по оформлению приложения к
            диплому европейского образца для выпускников ЮУрГУ, которое признаётся всеми университетами Европы. ИМО выполняет официальные переводы
            дипломов, которые принимаются всеми вузами США, Канады, Австралии и других стран, а также любых документов, необходимых для обучения в
            иностранных университетах. Это является ключевой компетенцией наших профессиональных переводчиков. Нас выгодно отличает низкая стоимость и
            безукоризненное качество перевода дипломов.</p>
        <p>За годы работы мы оформили свыше 200 Diploma Supplement, более 5000 нотариально заверенных переводов дипломов и документов, которые
            позволили нашим студентам и выпускникам продолжить образование за рубежом.</p>
        <p>Специализация Института международного образования этим не ограничивается: специалисты переведут вашу научную работу, статьи для
            международных конференций и журналов, деловую документацию и многое другое.</p>
        <br>
        <p class="uslugi-page__description">Функции:</p>
        <p>&ndash; переводы специальной литературы, деловой документации, документов об образовании;</p>
        <p>&ndash; редактирование и нотариальное заверение документов;</p>
        <p>&ndash; оформление Приложения к диплому Европейского образца.</p>
        <p>&nbsp;</p>

    </div>
<?$APPLICATION->IncludeComponent(
    "bitrix:form",
    "services",
    Array(
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_ADDITIONAL" => "N",
        "EDIT_STATUS" => "N",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "NOT_SHOW_FILTER" => array("",""),
        "NOT_SHOW_TABLE" => array("",""),
        "RESULT_ID" => "",
        "SEF_MODE" => "N",
        "SHOW_ADDITIONAL" => "N",
        "SHOW_ANSWER_VALUE" => "N",
        "SHOW_EDIT_PAGE" => "N",
        "SHOW_LIST_PAGE" => "N",
        "SHOW_STATUS" => "N",
        "SHOW_VIEW_PAGE" => "N",
        "START_PAGE" => "new",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => Array("action"=>"action"),
        "WEB_FORM_ID" => "3"
    )
);?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>