<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Услуги университета");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>
<div class="uslugi-page">
	<h2 class="uslugi-page__title blue-text">Сувенирная продукция ЮУрГУ</h2>
	<div class="row uslugi-page__hero">
		<div class="col-xs-12 col-md-6">
			<div class="contact-card">
				<h4 class="contact-card__title">Контакты</h4>
				<div class="contact-card__text">
					 Челябинск, ул. Сони Кривой, 79<br>
					 Тел.: +7 (351) 267-98-50, 267-99-67<br>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
 <img src="/images/services/suvenir.jpg" alt="" class="img-responsive w100">
		</div>
	</div>
	<p class="uslugi-page__description">
		 В 2007 году на базе факультета Коммерции ЮУрГУ был открыт первый в регионе, современный Учебно-производственный «Центр рекламных технологий», целью которого является обеспечение качественного непрерывного практического обучения студентов специальностей «Маркетинг», «Менеджмент организации» и «Реклама» факультета Коммерции.
	</p>
	<p>
		 Учебно-производственный «Центр рекламных технологий» руководствуется в своей деятельности следующими документами:
	</p>
	<p>
		 – действующим законодательством Российской Федерации в области рекламы и маркетинговых коммуникаций;
	</p>
	<p>
		 – постановлениями, распоряжениями, приказами, другими руководящими и нормативными документами федеральных, региональных и местных органов государственной власти и управления, определяющими приоритетные направления развития рекламы, маркетинга и порядок осуществления рекламной деятельности;
	</p>
	<p>
		 – Уставом ФГБОУ ВПО ЮУрГУ (НИУ);
	</p>
	<p>
		 – Положением о факультете Коммерции;
	</p>
	<p>
		 – решениями Учёного Совета ФГБОУ ВПО ЮУрГУ (НИУ);
	</p>
	<p>
		 – приказами и распоряжениями Ректора ФГБОУ ВПО ЮУрГУ (НИУ), декана факультета Коммерции.
	</p>
 <br>
	<h2 class="blue-text">Прайс</h2>
	<p class="uslugi-page__description">
		 Легкоатлетический манеж, теннисные корты, У-ШУ, бокс, ОФП
	</p>
	<p>
	</p>
	<div class="table-wrapper">
	<table  class="table">
 <thead>
	<tr>
		<th>
			 Тип
		</th>
		<th>
			 Изделие
		</th>
		<th>
			 Формат рис.
		</th>
		<th>
			 Цена, руб.
		</th>
	</tr></thead>
        <tbody>
	<tr>
		<td rowspan="14" colspan="1">
			 Термопресс<br>
		</td>
		<td rowspan="2" colspan="1">
			 Футболка белая<br>
		</td>
		<td>
			 А4
		</td>
		<td>
			 293.40
		</td>
	</tr>
	<tr>
		<td>
			 А3
		</td>
		<td>
			 338.80
		</td>
	</tr>
	<tr>
		<td rowspan="2" colspan="1">
			 Футболка цветная<br>
		</td>
		<td>
			 А4
		</td>
		<td>
			 520.50
		</td>
	</tr>
	<tr>
		<td>
			 А3
		</td>
		<td>
			 805.80
		</td>
	</tr>
	<tr>
		<td rowspan="2" colspan="1">
			 Футболка женская приталеная<br>
		</td>
		<td>
			 А4
		</td>
		<td>
			 446.80
		</td>
	</tr>
	<tr>
		<td>
			 А3
		</td>
		<td>
			 492.20
		</td>
	</tr>
	<tr>
		<td rowspan="2" colspan="1">
			 Футболка «Поло»<br>
		</td>
		<td>
			 А4
		</td>
		<td>
			 588.40
		</td>
	</tr>
	<tr>
		<td>
			 А3
		</td>
		<td>
			 633.80
		</td>
	</tr>
	<tr>
		<td rowspan="2" colspan="1">
			 Ветровка белая<br>
		</td>
		<td>
			 А4
		</td>
		<td>
			 954.20
		</td>
	</tr>
	<tr>
		<td>
			 А3
		</td>
		<td>
			 999.60
		</td>
	</tr>
	<tr>
		<td rowspan="2" colspan="1">
			 Ветровка цветная<br>
		</td>
		<td>
			 А4
		</td>
		<td>
			 1181.30
		</td>
	</tr>
	<tr>
		<td>
			 А3
		</td>
		<td>
			 1466.60
		</td>
	</tr>
	<tr>
		<td>
			 Бейсболка белая
		</td>
		<td>
			 А4
		</td>
		<td>
			 140.90
		</td>
	</tr>
	<tr>
		<td>
			 Бейсболка цветная
		</td>
		<td>
			 А3
		</td>
		<td>
			 197.30
		</td>
	</tr>
	<tr>
		<td rowspan="4" colspan="1">
			 Термопленка<br>
		</td>
		<td>
			 Футболка
		</td>
		<td>
			 1 изд.
		</td>
		<td>
			 248.00
		</td>
	</tr>
	<tr>
		<td>
			 Ветровка
		</td>
		<td>
			 1 изд.
		</td>
		<td>
			 908.60
		</td>
	</tr>
	<tr>
		<td>
			 Бейсболка
		</td>
		<td>
			 1 изд.
		</td>
		<td>
			 129.10
		</td>
	</tr>
	<tr>
		<td>
			 Изображение
		</td>
		<td>
			 1 дм2
		</td>
		<td>
			 22.10
		</td>
	</tr>
	<tr>
		<td rowspan="2" colspan="1">
			 Сублимация<br>
		</td>
		<td rowspan="2" colspan="1">
			 Футболка белая<br>
		</td>
		<td>
			 А4
		</td>
		<td>
			 364.30
		</td>
	</tr>
	<tr>
		<td>
			 А3
		</td>
		<td>
			 400.80
		</td>
	</tr>
	</tbody>
	</table>
	</div>
</div>
<?$APPLICATION->IncludeComponent(
    "bitrix:form",
    "services",
    Array(
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_ADDITIONAL" => "N",
        "EDIT_STATUS" => "N",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "NOT_SHOW_FILTER" => array("",""),
        "NOT_SHOW_TABLE" => array("",""),
        "RESULT_ID" => "",
        "SEF_MODE" => "N",
        "SHOW_ADDITIONAL" => "N",
        "SHOW_ANSWER_VALUE" => "N",
        "SHOW_EDIT_PAGE" => "N",
        "SHOW_LIST_PAGE" => "N",
        "SHOW_STATUS" => "N",
        "SHOW_VIEW_PAGE" => "N",
        "START_PAGE" => "new",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => Array("action"=>"action"),
        "WEB_FORM_ID" => "3"
    )
);?>
<br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>