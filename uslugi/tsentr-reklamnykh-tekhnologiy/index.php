<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Услуги университета");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>
    <div class="uslugi-page">
        <h2 class="uslugi-page__title  blue-text">Центр рекламных технологий</h2>
        <div class="row uslugi-page__hero">
            <div class="col-xs-12 col-md-6">
                <div class="contact-card">
                    <h4 class="contact-card__title">Контакты</h4>
                    <div class="contact-card__text">
                        Челябинск, ул. Сони Кривой, 79<br>
                        Тел.: (351) 267-98-50, 267-99-67<br>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <img src="/images/services/rekl.jpg" alt="" class="img-responsive w100">
            </div>
        </div>
        <p class="uslugi-page__description"> На базе института экономики, торговли и технологий, под патронажем кафедры «Маркетинговые коммуникации» работает первое в регионе
            настоящее Студенческое рекламное агентство, целью которого является обеспечение качественного непрерывного практического обучения
            студентов направлений «Менеджмент», «Реклама и связи с общественностью», «Технология полиграфического и упаковочного производства».</p>
        <p class="uslugi-page__description">
            Учебно-производственный Центр рекламных технологий занимается производством печатной, полиграфической и сувенирной рекламной продукции.
            Будущие рекламисты, PR-специалисты, бренд-менеджеры учатся в Центре проектированию и изготовлению рекламной продукции.
        </p>
        <p>
            В Центре студенты проходят учебно-ознакомительную и производственную практики, выполняют экспериментальную часть курсовых и дипломных
            работ, совершенствуют знания по дисциплинам «Разработка и технология производства рекламного продукта», «Проектирование упаковки», «Основы
            рекламы», «Основы полиграфии». Студенты принимают участие в производстве рекламы, предпечатной и постпечатной обработке рекламной
            продукции.
        </p>
        <p>
            Пройдя практику в Центре рекламных технологий, на производство студенты Тнашего института приходят уже настоящими специалистами. Рекламные
            листовки и календари, буклеты и афиши, футболки и магниты, кепки и брелоки, кружки с фирменной символикой и наклейки на сувениры - все это
            проектируется и изготавливается руками студентов. После такой практики слова "подборка" "подрезка", "биговка", "брошюрование",
            "фальцовка", "тиснение" звучат для наших выпускников как музыка.
        </p>
        <h2 class="blue-text">Прайс</h2>
        <h3>Полноцветная цифровая печать</h3>
        <div class="table-wrapper">
        <table class="table">
            <thead>
            <tr>
                <th >Материал</th>
                <th colspan="1" rowspan="2" >
                    <div>Формат</div>
                </th>
                <th colspan="1" rowspan="2" >
                    <div>Характеристика</div>
                </th>
                <th colspan="2" rowspan="1" >Цена, руб./отп.</th>
            </tr>
            <tr>
                <th >Вид</th>
                <th >одностор.</th>
                <th >двусторон.</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="1" rowspan="7" >
                    <div>Color Copy</div>
                </td>
                <td colspan="1" rowspan="5" >
                    <div>A3</div>
                </td>
                <td >90 г/м2</td>
                <td >15.40</td>
                <td >29.30</td>
            </tr>
            <tr>
                <td >120 г/м2</td>
                <td >16.30</td>
                <td >30.60</td>
            </tr>
            <tr>
                <td >160 г/м2</td>
                <td >17.30</td>
                <td >31.90</td>
            </tr>
            <tr>
                <td >200 г/м2</td>
                <td >18.50</td>
                <td >33.60</td>
            </tr>
            <tr>
                <td >250 г/м2</td>
                <td >19.80</td>
                <td >35.20</td>
            </tr>
            <tr>
                <td colspan="1" rowspan="2" >
                    <div>SR А3</div>
                </td>
                <td >280 г/м2 SR</td>
                <td >23.40</td>
                <td >40.60</td>
            </tr>
            <tr>
                <td >300 г/м2 SR</td>
                <td >23.90</td>
                <td >41.20</td>
            </tr>
            <tr>
                <td colspan="1" rowspan="2" >
                    <div>Дизай-нерская бумага</div>
                </td>
                <td colspan="1" rowspan="2" >
                    <div>330х488 мм</div>
                </td>
                <td >Golden Star R бел. 90 г/м2</td>
                <td >37.20</td>
                <td >61.20</td>
            </tr>
            <tr>
                <td >Sirio Peal PD, 300 г/м2</td>
                <td >63.90</td>
                <td >63.90</td>
            </tr>
            <tr>
                <td colspan="1" rowspan="2" >
                    <div>Мелованная бумага</div>
                </td>
                <td colspan="1" rowspan="2" >
                    <div>А3</div>
                </td>
                <td colspan="1" rowspan="2" >
                    <div>&laquo;Лён&raquo; д/сторон.бел., 280 г/м2</div>
                </td>
                <td >42.50</td>
                <td >68.20</td>
            </tr>
            <tr>
                <td >23.30</td>
                <td >42.40</td>
            </tr>
            <tr>
                <td colspan="1" rowspan="2" >
                    <div>Цветная бумага</div>
                </td>
                <td colspan="1" rowspan="2" >
                    <div>А3</div>
                </td>
                <td >300 г/м2</td>
                <td >41.70</td>
                <td >68.80</td>
            </tr>
            <tr>
                <td >80 г/м2</td>
                <td >16.90</td>
                <td >32.40</td>
            </tr>
            <tr>
                <td colspan="1" rowspan="2" >
                    <div>Самоклей. бумага</div>
                </td>
                <td colspan="1" rowspan="2" >
                    <div>305х430</div>
                </td>
                <td colspan="1" rowspan="2" >
                    <div>160 г/м2</div>
                </td>
                <td >19.30</td>
                <td >35.80</td>
            </tr>
            <tr>
                <td >26.70</td>
                <td >-</td>
            </tr>
            <tr>
                <td colspan="1" rowspan="3" >
                    <div>Пленка под печать</div>
                </td>
                <td >А3</td>
                <td >самоклей. прозрачная</td>
                <td >188.80</td>
                <td >-</td>
            </tr>
            <tr>
                <td colspan="1" rowspan="2" >
                    <div>А4</div>
                </td>
                <td >самоклей. белая</td>
                <td >106.20</td>
                <td >-</td>
            </tr>
            <tr>
                <td >прозрачная</td>
                <td >21.30</td>
                <td >-</td>
            </tr>
            </tbody>
        </table>
        </div>
    </div>
<?$APPLICATION->IncludeComponent(
    "bitrix:form",
    "services",
    Array(
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_ADDITIONAL" => "N",
        "EDIT_STATUS" => "N",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "NOT_SHOW_FILTER" => array("",""),
        "NOT_SHOW_TABLE" => array("",""),
        "RESULT_ID" => "",
        "SEF_MODE" => "N",
        "SHOW_ADDITIONAL" => "N",
        "SHOW_ANSWER_VALUE" => "N",
        "SHOW_EDIT_PAGE" => "N",
        "SHOW_LIST_PAGE" => "N",
        "SHOW_STATUS" => "N",
        "SHOW_VIEW_PAGE" => "N",
        "START_PAGE" => "new",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => Array("action"=>"action"),
        "WEB_FORM_ID" => "3"
    )
);?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>