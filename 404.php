<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");

?>
    <h2>Страница не найдена</h2>

    <p>Запрошенная страница отсутствует на сайте.</p>
    <p>Проверьте правильность написания адреса,  <a href="javascript:window.history.back();">вернитесь назад</a> или <a href="/">начните
            сначала</a> </p>
    <p>&nbsp;</p>
<?
/*$APPLICATION->IncludeComponent("bitrix:main.map", ".default", Array(
	"LEVEL"	=>	"3",
	"COL_NUM"	=>	"2",
	"SHOW_DESCRIPTION"	=>	"Y",
	"SET_TITLE"	=>	"Y",
	"CACHE_TIME"	=>	"3600"
	)
);*/

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>