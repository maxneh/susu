<?
/*$detect = new Mobile_Detect;
$isMobile = $detect->isMobile() && !$detect->isTablet();
if ($isMobile) { $aMenuLinks = Array(
    Array(
        "Новости и события",
        "/",
        Array(),
        Array("ICON"=>"news"),
        ""
    ),
    Array(
        "Доска почёта",
        "/doska-pocheta/",
        Array(),
        Array("ICON"=>"honor"),
        ""
    ),
    Array(
        "Мы - ЮУрГУ",
        "/about/",
        Array(),
        Array("ICON"=>"u"),
        ""
    ),
    Array(
        "Услуги университета",
        "/uslugi/",
        Array(),
        Array("ICON"=>"service"),
        ""
    ),
    Array(
        "Структура ВУЗа",
        "/spravochnik/struktura-vuza/",
        Array(),
        Array("ICON"=>"structure"),
        ""
    ),
    Array(
        "Фотогалерея",
        "/gallery/",
        Array(),
        Array("ICON"=>"photo"),
        ""
    ),
    Array(
        "Интранет",
        "/club/",
        Array(),
        Array("ICON"=>"briefcase"),
        ""
    )
);
} else {*/
$aMenuLinks = Array(
	Array(
		"Новости и события", 
		"/", 
		Array(), 
		Array("ICON"=>"news"), 
		"" 
	),
	Array(
		"Доска почёта", 
		"/doska-pocheta/", 
		Array(), 
		Array("ICON"=>"honor"), 
		"" 
	),
	Array(
		"Мы - ЮУрГУ", 
		"/about/", 
		Array(), 
		Array("ICON"=>"u"), 
		"" 
	),
	Array(
		"Услуги университета", 
		"/uslugi/", 
		Array(), 
		Array("ICON"=>"service"), 
		"" 
	),
    Array(
        "Структура ВУЗа",
        "/spravochnik/struktura-vuza/",
        Array(),
        Array("ICON"=>"structure"),
        ""
    ),
    Array(
        "Сотрудники и контакты",
        "/spravochnik/kontakty-sotrudnikov/",
        Array(),
        Array("ICON"=>"staff"),
        ""
    ),
	Array(
		"Фотогалерея", 
		"/gallery/", 
		Array(), 
		Array("ICON"=>"photo"), 
		"" 
	),
	Array(
		"Шаблоны документов", 
		"/spravochnik/shablony-dokumentov/", 
		Array(), 
		Array("ICON"=>"documents"), 
		"" 
	),
	Array(
		"Интранет",
		"/club/",
		Array(),
		Array("ICON"=>"briefcase"),
		""
	)
);
//}
?>