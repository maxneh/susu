<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Мы - ЮУрГУ");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>


    <div class="row">
        <div class="col-xs-12">
            <h2 class="black-text">Доступ к беспроводной сети университета</h2>
            <p>Для подключения к беспроводной сети ЮУрГУ выберите сеть SUSU_ENT. Идентификационные данные для подключения совпадают с данными для
                входа в Ваш личный кабинет.</p>
            <p>Процедура авторизации в сети может меняться в зависимости от Вашего устройства и операционной системы. Один из примеров настроек для
                подключения через смартфон: метод аутентификации EAP -- PEAP / MSCHAPV2.</p>
            <h3>Покрытие</h3>
            <p>Научно-образовательная Wi-Fi сеть ЮУрГУ развернута в основных "публичных" местах главного учебного корпуса, а также корпусов 2,
                3А, 3Б и 3Г</p>
            <br>
            <br>
            <div id="iframe-map" class="pointer-event-stopped">
            <iframe allowfullscreen="" frameborder="0" height="600" marginheight="0" marginwidth="0" src="https://www.susu.ru/map/osm-ru.html" width="100%"></iframe>
            </div>
                <br>
            <br>
            <h3>Помощь</h3>
            <p>В случае возникновения проблем, связанных с подключением к сети Wi-Fi, обращайтесь по телефону: (351) 267-93-60</p>
            <h3>Настройка ОС Wndows</h3>
            <p>В случае, если автоматическая настройка подключения не выполняется, необходимо выполнить следующие действия:</p>
            <p>1. Добавить беспроводную сеть с именем SUSU_ENT вручную. Если в списке известных сетей она присутствует, то её надо удалить (”Забыть” в
                Windows 8.x).</p>
            <p>2. Настроить тип безопасности/шифрования ("WPA2-Enterprise"/"AES").</p>
            <p>3. В качестве метода проверки подлинности указать "Microsoft: защищенные EAP (PEAP)".</p>
            <p>4. Необходимо указать режим проверки подлинности (проверка подлинности пользователя) и, при желании, сохранить учётные данные доступа к
                беспроводной сети.
            </p>
        </div>

    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <h2 class="black-text">О предоставлении доступа к системе &laquo;Универис&raquo;</h2>
            <p>Для получения доступа к системе "Универис" Вам необходимо написать служебную записку на имя Раенко В.Г. и подписать ее у руководителя
                вашего подразделения.</p>
            <p>С 4 июля 2016 года, служебные записки образца 2016 года должны быть подписаны новыми руководителями структурных подразделений.</p>
            <p>Если дополнительно необходим доступ к старой структуре университета, необходимо указать это в служебной записке, если такой
                необходимости нет, второй абзац в образце служебной записки следует удалить.</p>
            <p>Заполнить и приложить Ваше обязательство о неразглашении конфиденциальной информации. После заполнения всех документов нужно направить
                на подпись к начальнику управления информатизации Раенко В.Г., 113 ауд/2 корпуса.</p>
            <p>Файлы для заполнения можно скачать ниже:</p>

            <? global $arrAccessTemplate;
            $arrAccessTemplate = array('SECTION_ID'=>203); // доступ к универис
            $APPLICATION->IncludeComponent("bitrix:news.list", "files", array(
                "IBLOCK_ID" => "16",
                "IBLOCK_TYPE" => "guide",
                "NEWS_COUNT" => "999",
                "NUM_COLS" => 2,
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arrAccessTemplate",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "actions_detail.php?ID=#ELEMENT_ID#",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_OPTION_ADDITIONAL" => ""
            ),
                false
            );?></div>
        <div class="col-xs-12 col-md-6">
            <h2 class="black-text">Командировки</h2>
            <?global $arrGoTemplate;
            $arrGoTemplate = array('SECTION_ID'=>202); // командировки
            $APPLICATION->IncludeComponent("bitrix:news.list", "files", array(
                "IBLOCK_ID" => "16",
                "IBLOCK_TYPE" => "guide",
                "NEWS_COUNT" => "999",
                "NUM_COLS" => 2,
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arrGoTemplate",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "actions_detail.php?ID=#ELEMENT_ID#",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_OPTION_ADDITIONAL" => ""
            ),
                false
            );?>
        </div>
    </div>
    <style>
        .pointer-event-stopped iframe{
            pointer-events: none;
        }
    </style>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>