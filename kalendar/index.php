<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Календарь");
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/grid/webform-button.css");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");

$APPLICATION->IncludeComponent(
    "bitrix:calendar.grid",
    ".default",
    array(
        "CALENDAR_TYPE" => "events",
        "COMPONENT_TEMPLATE" => ".default"
    ),
    false
);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");