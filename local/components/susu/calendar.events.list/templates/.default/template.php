<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?if ($arResult['ACCESS_DENIED']):?>
	<?=GetMessage('ECL_T_ACCESS_DENIED')?>
<?elseif($arResult['INACTIVE_FEATURE']):?>
	<?=GetMessage('ECL_T_INACTIVE_FEATURE')?>
<?elseif(count($arResult["ITEMS"]) == 0):?>
	<?=GetMessage('ECL_T_NO_ITEMS')?>
<?else:?>
<?$rus_months = array('января', 'февраля', 'март', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');?>
<?foreach($arResult["ITEMS"] as $arItem):?>
    <? $month = date("m", $arItem["DT_FROM_TS"]);?>
        <div class="widget-item">
            <div class="widget-item__short-date"><?= date("j", $arItem["DT_FROM_TS"])?><br><span><?= $rus_months[$month-1]?></span></div>
            <div>
                <div class="widget-item__type"><?=$arItem['SECTION']['NAME']?></div>
                <p class="widget-item__text">
                    <a class="calendar-link<?=$arItem["_ADD_CLASS"]?>" href="<?=$arItem["_DETAIL_URL"]?>"><span><?=$arItem["NAME"]?></span></a>
                </p>
            </div>
        </div>
<?endforeach;?>
<?endif;?>
