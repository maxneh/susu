<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$arResult['LINK_TITLE'] = htmlspecialcharsbx($arParams['LINK_TITLE']);
$arResult['LINK_URL'] = htmlspecialcharsbx($arParams['LINK_URL']);

$arResult['SHARE_ID'] = (time()+rand(0, 1000));

$this->IncludeComponentTemplate();


return $arResult;
