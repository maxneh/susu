<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Поделиться");

$link_url = htmlspecialcharsbx($_REQUEST['link']);
$link_title = htmlspecialcharsbx($_REQUEST['title']);
$back_url = htmlspecialcharsbx($_REQUEST['back_url']);
$back_url = $back_url == "" ? '/' : $back_url;
$componentParameters = Array(
    "ALLOW_POST_CODE" => "Y",
    "DATE_TIME_FORMAT" => "d.m.Y H:i:s",
    "ID" => "new",
    "TEXT_TO_SHARE" => "[URL={$link_url}]{$link_title}[/URL]",
//    "SOCNET_USER_ID" => $USER->GetID(),
    "USER_ID" => $USER->GetID(),
    "PAGE_VAR" => "",
//    "PATH_TO_BLOG" => $APPLICATION->GetCurUri('SHARESBM'),
    "PATH_TO_BLOG" => "{$back_url}?SHARESBM",
    "PATH_TO_DRAFT" => "/club/user/#user_id#/blog/draft/",
    "PATH_TO_POST" => "/club/user/#user_id#/blog/#post_id#/",
    "PATH_TO_POST_EDIT" => "/club/user/#user_id#/blog/edit/#post_id#/",
    "PATH_TO_SMILE" => "/bitrix/images/socialnetwork/smile/",
    "PATH_TO_USER" => "/club/user/#user_id#/",
    "MICROBLOG" => "Y",
    "POST_PROPERTY" => array(),
    "POST_VAR" => "",
    "SET_TITLE" => "N",
    "USER_VAR" => ''
);


if ($_REQUEST['IFRAME'] == 'Y') {
    $_REQUEST['sessid'] = bitrix_sessid();
    $APPLICATION->IncludeComponent(
        "bitrix:socialnetwork.pageslider.wrapper",
        "",
        array(
            'POPUP_COMPONENT_NAME' => "susu:share.blog.post.add",
            "POPUP_COMPONENT_TEMPLATE_NAME" => "",
            "POPUP_COMPONENT_PARAMS" => $componentParameters,
        )
    );
} else {
    $APPLICATION->IncludeComponent(
        "susu:share.blog.post.add",
        ".default",
        $componentParameters,
        false
    );
} ?>

    <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>