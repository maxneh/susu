BX.ready(function () {
    BX.addCustomEvent("SidePanel.Slider:onReload", function (event) {
        var slider = event.getSlider();
        if (slider.url.lastIndexOf("SHARESBM") !== -1) {
            BX.SidePanel.Instance.close();
        }
    });

    BX.SidePanel.Instance.bindAnchors({
        rules: [
            {
                condition: [
                    '/shareit/'
                ],
                loader: 'add-share-loader',
                options: {
                    width: 900
                }
            },]
    });
});