<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<? if ($USER->IsAuthorized()) {?>

<a href="/shareit/?link=<?=$arParams['LINK_URL']?>&title=<?=$arParams['LINK_TITLE']?>&back_url=<?=$arParams['BACK_URL']?>" class="share"
   title="Поделиться">
    <i class="share__icon"></i>
</a>
<? } ?>
