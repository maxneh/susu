<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
    'GROUPS' => array(),
    'PARAMETERS' => array(
        'IBLOCK_TYPE' => GetMessage('IBLOCK_TYPE'),
        'TITLE',
        'DATE',
        'SECTION_URL',
        'SECTION_ID',
        'TOLEFT_URL',
        'TORIGHT_URL',
        'LIKE',
        'SHOW_COMMENTS',
        'COMMENTS_CNT',
    )
);
?>