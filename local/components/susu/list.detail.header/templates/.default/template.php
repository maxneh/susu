<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!--TPL: <?= $this->GetFolder() ?>-->
<div class="header-detail header-detail_<?= $arParams['IBLOCK_TYPE'] ?>">
    <div class="header-detail__col"><? if (!empty($arParams['TOLEFT_URL'])): ?>
            <a href="<?= $arParams['TOLEFT_URL'] ?>" title="Пред.">
                <i class="toleft-icon"></i>
            </a>
        <? endif ?>
        <? if (!empty($arParams['SECTION_URL'])) { ?>
            <a href="<?= $arParams['SECTION_URL'] ?>"><span class="category-icon category-icon_detail icon icon-<?= $arParams['SECTION_ICON'] ?>"
                                                            title="<?= $arParams['SECTION_NAME'] ?>"></span></a>
        <? } elseif ($arParams['SECTION_ICON']) { ?>
            <span class="category-icon category-icon_detail icon icon-<?= $arParams['SECTION_ICON'] ?>"
                  title="<?= $arParams['SECTION_NAME'] ?>"></span>
        <? } ?>
    </div>
    <div class="header-detail__col header-detail__col_title">
        <? if (!empty($arParams['DATE'])) { ?> <span class="header-detail__date"><?= $arParams['DATE'] ?></span><? } ?>
        <h1 class="header-detail__title"><?= $arParams['TITLE'] ?></h1>
    </div>
    <div class="header-detail__col">
        <? if (!empty($arParams['EDIT_LINK'])) { ?>
            <a href="<?= $arParams['EDIT_LINK'] ?>" title="Редактировать"><i class="icon icon-pencil icon-edit"></i></a>
        <? } ?>
        <? if ($arParams['LIKE'] && is_array($arParams['LIKE'])) {
            $arRatingParams = $arParams['LIKE'];
            $GLOBALS["APPLICATION"]->IncludeComponent("bitrix:rating.vote", "like", $arRatingParams, false, array("HIDE_ICONS" => "Y"));
        } ?>
        <? if ($arParams['SHAREIT'] && is_array($arParams['SHAREIT'])) {
            $arShareParams = $arParams['SHAREIT'];
            $GLOBALS["APPLICATION"]->IncludeComponent("susu:shareit", "", $arShareParams, $component);
        } ?>
        <? if ($arParams['FAVOR'] && is_array($arParams['FAVOR'])) {
            $arFavorParams = $arParams['FAVOR'];
            $GLOBALS["APPLICATION"]->IncludeComponent("susu:favorites", "", $arFavorParams, $component);
        } ?>
        <? if ($arParams['SHOW_COMMENTS'] == 'Y' && $arParams['COMMENTS_CNT']) { ?>

            <a href="#comment-anchor" title="Комментарии"><i class="comments-icon"><?= $arParams['COMMENTS_CNT'] ?></i></a>

        <? } ?>
        <? if (!empty($arParams['TORIGHT_URL'])): ?>
            <a href="<?= $arParams['TORIGHT_URL'] ?>" title="След.">
                <i class="toright-icon"></i>
            </a>
        <? endif ?>
    </div>
</div>