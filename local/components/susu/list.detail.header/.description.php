<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("IBLOCK_NAME"),
	"DESCRIPTION" => GetMessage("IBLOCK_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"CACHE_PATH" => "Y",
	"SORT" => 20,
	"PATH" => array(
		"ID" => "susu_components",
        "NAME" => GetMessage("SUSU_COMPONENTS"),
        "SORT" => 2001,
		"CHILD" => array(
			"ID" => "content",
			"NAME" => GetMessage("T_IBLOCK_DESC"),
			"SORT" => 20,
		),
	),
);

?>