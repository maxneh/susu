<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach ($arResult as $formatedDate => $currentDate):?>
    <!--TPL: <?= $this->GetFolder()?>-->
<div class="widget-item">
    <? $date = explode(' ',$formatedDate)?>
    <div class="widget-item__short-date"><?=$date[0]?><br><span><?=$date[1]?></span></div>
    <div>
        <div class="widget-item__type"><?=$arParams["TITLE"]?></div>
    <?foreach ($currentDate as $userID => $user):?>
        <p class="widget-item__text"><?=$user["NAME"]?> <?=$user["LAST_NAME"]?></p>
    <?endforeach;?>
    </div>
</div>
<?endforeach;?>