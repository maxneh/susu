<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	
$arComponentParameters = array(

	"PARAMETERS" => array(
	
		"TITLE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("TITLE"),
			"TYPE" => "STRING",
			"DEFAULT" => "Ближайшие дни рождения",
			"REFRESH" => "N",
			"COLS" => "40"
		),

		"DAYS_AFTER" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DAYS_AFTER"),
			"TYPE" => "STRING",
			"DEFAULT" => "10",
			"REFRESH" => "N",
			"COLS" => "15"
		),
		"FORMAT" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("FORMAT"),
			"TYPE" => "STRING",
			"DEFAULT" => "j F",
			"REFRESH" => "N",
			"COLS" => "15"
		),
        "CACHE_TIME" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("CACHE_TIME"),
            "TYPE" => "STRING",
            "DEFAULT" => 86400,
            "REFRESH" => "N",
        ),
		
		)
);

?>