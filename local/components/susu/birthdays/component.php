<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$daysAfter = $arParams["DAYS_AFTER"];
if (!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 86400;

$nowDate = date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time());
$tmpDate = ConvertDateTime($nowDate, "MM-DD", "ru");
$caheId = implode('_', (array($nowDate)));
if ($this->StartResultCache(false, $caheId)) {
    for ($i = 0; $i <= $daysAfter; $i++) {

        $tmpDate = MakeTimeStamp($tmpDate, "MM-DD");
        $arrAdd = array(
            "DD" => $i,
            "MM" => 0,
            "YYYY" => 0,
            "HH" => 0,
            "MI" => 0,
            "SS" => 0,
        );
        $tmpDate = AddToTimeStamp($arrAdd, $tmpDate);
        $tmpDate = date("m-d", $tmpDate);

        $filter = Array
        (
            "PERSONAL_BIRTHDAY_DATE" => $tmpDate
        );
        $rsUsers = CUser::GetList(($by = "timestamp_x"), ($order = "desc"), $filter); // выбираем пользователей
        while ($arUser = $rsUsers->GetNext()) {
            $dateFormat = FormatDate($arParams["FORMAT"], MakeTimeStamp($arUser["PERSONAL_BIRTHDAY"]));
            $arResult[$dateFormat][$arUser["ID"]] = $arUser;
        }
    }

    $this->IncludeComponentTemplate();
}
?>