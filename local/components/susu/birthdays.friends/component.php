<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$daysAfter = $arParams["DAYS_AFTER"];

if (!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 86400;

if (!isset($arParams["COUNT_ITEMS"]))
    $arParams["COUNT_ITEMS"] = 0;


    $arParams['NAME_TEMPLATE'] = empty($arParams['NAME_TEMPLATE']) ? CSite::GetNameFormat(false) : str_replace(array("#NOBR#","#/NOBR#"),
        array("",""), $arParams["NAME_TEMPLATE"]);

$nowDate = date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time());
$tmpDate = ConvertDateTime($nowDate, "MM-DD", "ru");
$user_id = $USER->GetID();
$caheId = implode('_', (array($nowDate, $tmpDate, $user_id)));

//if ($this->StartResultCache($arParams["CACHE_TIME"], $caheId)) {
    CModule::IncludeModule('socialnetwork');
    $dbFriends = CSocNetUserRelations::GetRelatedUsers(
        $user_id,
        SONET_RELATIONS_FRIEND,
        false,
        'Y'
    );
    if ($dbFriends) {
        $arFriendsIds = array();
        while ($arFriends = $dbFriends->GetNext()) {
            $pref = ((IntVal($user_id) == $arFriends["FIRST_USER_ID"]) ? "SECOND" : "FIRST");
            $arFriendsIds[] = $arFriends[$pref . "_USER_ID"];
        }
    }

    if($arFriendsIds != array()) {
        for ($i = 0; $i <= $daysAfter; $i++) {

            $tmpDate = MakeTimeStamp($tmpDate, "MM-DD");
            $arrAdd = array(
                "DD" => $i,
                "MM" => 0,
                "YYYY" => 0,
                "HH" => 0,
                "MI" => 0,
                "SS" => 0,
            );
            $tmpDate = AddToTimeStamp($arrAdd, $tmpDate);
            $tmpDate = date("m-d", $tmpDate);

            $filter = Array
            (
                "ID" => implode('|', $arFriendsIds),
                "PERSONAL_BIRTHDAY_DATE" => $tmpDate
            );

            $rsUsers = CUser::GetList(($by = "timestamp_x"), ($order = "desc"), $filter); // выбираем пользователей
            while ($arUser = $rsUsers->GetNext()) {
                $dateFormat = FormatDate($arParams["FORMAT"], MakeTimeStamp($arUser["PERSONAL_BIRTHDAY"]));
                if (intval($arParams["THUMBNAIL_LIST_SIZE"]) > 0) {


                    $arImage = CFile::ResizeImageGet(
                        $arUser["PERSONAL_PHOTO"],
                        array("width" => $arParams["THUMBNAIL_LIST_SIZE"], "height" => $arParams["THUMBNAIL_LIST_SIZE"]),
                        BX_RESIZE_IMAGE_EXACT,
                        false
                    );

                    $arUser["PERSONAL_PHOTO_SRC"] = $arImage['src'];
                    if ($arParams["PATH_TO_USER"]) {
                        $arUser["PROFILE_URL"] = CComponentEngine::MakePathFromTemplate($arParams["PATH_TO_USER"], array("user_id" => $arUser["ID"]));
                    }
                }

                $arUser["NAME_FORMATTED"] = CUser::FormatName($arParams["NAME_TEMPLATE"], $arUser, true, false);
                $arResult['ITEMS'][$dateFormat][$arUser["ID"]] = $arUser;
            }
        }

        $arResult['COUNT'] = count($arResult['ITEMS']) < 100 ? count($arResult['ITEMS']) : "99+";
        if ($arParams["COUNT_ITEMS"] > 0) {
            array_splice($arResult['ITEMS'], intVal($arParams['COUNT_ITEMS']));
        }
    } else {
        $arResult = NULL;
    }


    $this->IncludeComponentTemplate();
//}
?>