<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="birthdays-list">
    <? if ($arResult != NULL) { ?>

        <? foreach ($arResult['ITEMS'] as $formatedDate => $currentDate) { ?>
            <!--TPL: <?= $this->GetFolder() ?>-->
            <div class="birthday-date__wrap">
                <? $date = explode(' ', $formatedDate) ?>

                <h2 class="birthday-date__title"><?= $date[0] ?>&nbsp;<small><?= $date[1] ?></small>
                </h2>
                <div class="sonet-members-item">
                    <div class="sonet-members-wrap row">
                        <? foreach ($currentDate as $userID => $user) { ?>
                            <div class="sonet-members-member-block col-xs-12 col-sm-6 col-md-4">
                                <div class="sonet-members-member-img"
                                     style="background: url('<?= $user["PERSONAL_PHOTO_SRC"] ?>') no-repeat 0 0;"></div>
                                <a class="sonet-members-member-link" href="<?= $user["PROFILE_URL"] ?>"> <?= $user["NAME_FORMATTED"] ?></a>
                                <div class="sonet-members-command-block">
                                    <a class="" href="/club/messages/chat/<?= $user['ID'] ?>/" onclick="if (typeof(BX) != 'undefined' && BX.IM) {
                                            BXIM.openMessenger(<?= $user['ID'] ?>); return false;
                                            } else {
                                            window.open('/club/messages/chat/<?= $user['ID'] ?>/',
                                            '',
                                            'location=yes,status=no,scrollbars=yes,resizable=yes,width=700,height=550,top='+Math.floor((screen.height - 550)/2-14)+',left='+Math.floor((screen.width - 700)/2-5));
                                            return false; }"><span>Написать</span></a>
                                </div>
                            </div>
                        <? } ?>
                    </div>
                </div>
            </div>
        <? } ?>
    <? } else { ?>
        <h4>Список друзей пуст</h4>
    <? } ?>

</div>
