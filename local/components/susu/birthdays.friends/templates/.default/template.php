<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="widget widget_birthdays">
    <a href="<?=$arParams['TITLE_LINK']?>"><h4 class="widget__title">
            <i class="widget__icon icon icon-celebration"></i>
            <span><?=$arParams['TITLE']?></span>
        <? if ($arResult['COUNT'] > 2) {?>
            <i class="badge"><?=$arResult['COUNT']?></i>
        <? } ?>
        </h4>
    </a>

    <? foreach ($arResult['ITEMS'] as $formatedDate => $currentDate): ?>
        <!--TPL: <?= $this->GetFolder() ?>-->
        <div class="widget-item">
            <? $date = explode(' ', $formatedDate) ?>
            <div class="widget-item__short-date"><?= $date[0] ?><br><span><?= $date[1] ?></span></div>
            <div>
                <div class="widget-item__type">День рождения</div>
                <? foreach ($currentDate as $userID => $user): ?>
                    <p class="widget-item__text"><?= $user["NAME_FORMATTED"] ?></p>
                     <? endforeach; ?>
            </div>
        </div>
    <? endforeach; ?>

</div>
