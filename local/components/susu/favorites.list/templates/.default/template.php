<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<!--TPL: <?= $this->GetFolder()?>-->

<div id="filter-favor" class="filter-favor pull-right">
    <button id="filter-favor__button" class="btn btn_white">Категории</button>
    <div id="filter-popup" class="menu-popup" style="display: none;">
        <div class="menu-popup-items">
            <a class="menu-popup-item favor-filter-item" href="#" onclick="selectFilterEls(this)" data-filter="NEWS">
                <span class="menu-popup-item-icon"></span><span class="menu-popup-item-text">Новости</span>
            </a>
            <a class="menu-popup-item favor-filter-item" href="#" onclick="selectFilterEls(this)" data-filter="PHOTOALBUM">
                <span class="menu-popup-item-icon"></span><span class="menu-popup-item-text">Фотоальбомы</span>
            </a>
            <a class="menu-popup-item favor-filter-item" href="#" onclick="selectFilterEls(this)" data-filter="BOARD">
                <span class="menu-popup-item-icon"></span><span class="menu-popup-item-text">Объявления</span>
            </a>
            <a class="menu-popup-item favor-filter-item" href="#" onclick="selectFilterEls(this)" data-filter="FILES">
                <span class="menu-popup-item-icon"></span><span class="menu-popup-item-text">Документы</span>
            </a>
        </div>
    </div>
</div>


<div class="clearfix"></div>
<ul id="favorites-list" class="favorites-list">
    <?

    foreach ($arResult as $item) { ?>

        <li id="favID_<?= $item['ID'] ?>" class="favorites-list__item" data-type="<?= $item['UF_ENTITY_TYPE_ID'] ?>">
            <a href="<?= $item['UF_LINK_URL'] ?>" target="_blank"><span><?= $item['UF_LINK_TITLE'] ?></span></a>
            <div class="icon icon-cross pull-right delete-action" onclick="favDelete(<?= $item['ID'] ?>)"></div>
        </li>
    <? } ?>
</ul>