BX.ready(function () {
    BX.bind(BX('filter-favor__button'), 'click', function () {
        var popup = BX.PopupWindowManager.create("popup-message", BX("filter-favor__button"), {
            content: BX('filter-popup'),
            darkMode: false,
            autoHide: true,
            // angle: { "position": "top", "offset": 70 }
        });
        popup.show();
    });
});
    function selectFilterEls(a) {

        var filterEls, ul, li, i, filter = [];
        BX.toggleClass(a, 'favor-filter-item-selected');
        filterEls = document.getElementsByClassName('favor-filter-item-selected');
        ul = document.getElementById("favorites-list");
        li = ul.getElementsByTagName('li');

        for (i = 0; i < filterEls.length; i++) {
            filter.push(filterEls[i].dataset.filter.toUpperCase());
        }
        for (ii = 0; ii < li.length; ii++) {

            if (filter.length === 0 || filter.indexOf(li[ii].dataset.type.toUpperCase()) > -1) {
                li[ii].style.display = "";
            } else {
                li[ii].style.display = "none";
            }
        }

        return false;
    }

    function favDelete(fav_id) {
        if (confirm("Вы хотите убрать ссылку из избранного?")) {
            BX.ajax({
                url: '/local/components/susu/favorites/fav.ajax.php',
                data: {
                    'FAV': 'Y',
                    'FAV_ID': fav_id,
                    'FAV_ACTION': 'delete',
                    'sessid': BX.bitrix_sessid()
                },
                method: 'POST',
                dataType: 'json',
                timeout: 30,
                async: true,
                processData: true,
                scriptsRunFirst: true,
                emulateOnload: true,
                start: true,
                cache: false,
                onsuccess: function (data) {
                    if (parseInt(data.result) > 0) {
                        BX('favID_' + fav_id).remove();
                    } else {
                        console.log(fav_id + ' fav delete error');
                    }
                },
                onfailure: function () {
                    console.log(fav_id + ' fav delete error');
                }
            });
        } else return false;
    }
