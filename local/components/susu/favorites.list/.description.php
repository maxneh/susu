<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
   "NAME" => 'Список избранного',
   "DESCRIPTION" => 'Список избранного',
   "ICON" => "/",
   "SORT" => 30,
   "PATH" => array(
       "ID" => "susu_components",
       "NAME" => 'Компоненты ЮУрГУ',
   ),
   "CACHE_PATH" => "N"
);
?>