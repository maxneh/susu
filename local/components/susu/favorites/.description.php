<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
   "NAME" => 'Избранное',
   "DESCRIPTION" => 'Избранное',
   "ICON" => "/",
   "SORT" => 30,
   "PATH" => array(
       "ID" => "susu_components",
       "NAME" => 'Компоненты ЮУрГУ',
   ),
   "CACHE_PATH" => "N"
);
?>