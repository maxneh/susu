<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

?>
<? if ($USER->IsAuthorized()) {?>
<? if ($arResult['USER_HAS_FAV'] == 'Y') { ?>
    <span id="fav-<?= htmlspecialcharsbx($arResult['FAV_ID']) ?>" class="fav fav_active" title="Убрать из избранного">
   <i class="fav__icon"></i>
</span>
<? } else { ?>
    <span id="fav-<?= htmlspecialcharsbx($arResult['FAV_ID']) ?>" class="fav" title="Добавить в избранное">
    <i class="fav__icon"></i>
</span>
<? } ?>

<script type="text/javascript">
    BX.ready(function () {
        <?if ($arResult['AJAX_MODE'] == 'Y'):?>

        BX.loadCSS('/local/components/susu/favorites/templates/.default/style.css');
        BX.loadScript('/local/components/susu/favorites/templates/.default/script.js', function () {

            <?endif;?>
            if (!window.Fav && top.Fav)
                Fav = top.Fav;

            if (typeof(Fav) == 'undefined')
                return false;

            Fav.Set(
                '<?=CUtil::JSEscape($arResult['FAV_ID'])?>',
                '<?=CUtil::JSEscape($arResult['ENTITY_TYPE_ID'])?>',
                '<?=IntVal($arResult['ENTITY_ID'])?>',
                '<?=CUtil::JSEscape($arResult['FAV_AVAILABLE'])?>',
                '<?=CUtil::JSEscape($arResult['LINK_TITLE'])?>',
                '<?=CUtil::JSEscape($arResult['LINK_URL'])?>',
            );


            <?if ($arResult['AJAX_MODE'] == 'Y'):?>
        });
        <?endif;?>

    });
</script>

<? } ?>