if (!FAV) {
    var FAV= {};
}

Fav = function (favId, entityTypeId, entityId, available, title, url, pathToAjax) {
    this.enabled = true;
    this.favId = favId;
    this.entityTypeId = entityTypeId;
    this.entityId = entityId;
    this.available = available == 'Y' ? true : false;
    this.title = title;
    this.url = url;
    this.pathToAjax = typeof(pathToAjax) == "string" ? pathToAjax : '/local/components/susu/favorites/fav.ajax.php';

    this.button = BX('fav-'+favId);
    if (this.button === null)
    {
        this.enabled = false;
        return false;
    }
    this.favTimeout = false;


};


Fav.Set = function (favId, entityTypeId, entityId, available,  title, url, pathToAjax) {

    if (!FAV[favId] || FAV[favId].tryToSet <= 5) {
        var tryToSend = FAV[favId] && FAV[favId].tryToSet ? FAV[favId].tryToSet : 1;
        FAV[favId] = new Fav(favId, entityTypeId, entityId, available,  title, url, pathToAjax);

        if (FAV[favId].enabled) {
            Fav.Init(favId);
        }
        else {
            setTimeout(function () {
                FAV[favId].tryToSet = tryToSend + 1;
                Fav.Set(favId, entityTypeId, entityId, available, title, url, pathToAjax);
            }, 500);
        }
    }
};

Fav.Init = function (favId) {

    if (FAV[favId].available) {
        BX.bind(FAV[favId].button, 'click', function (e) {
            clearTimeout(FAV[favId].favTimeout);

            var active = BX.hasClass(this, 'fav_active');

            if (active) {
                FAV[favId].favTimeout = setTimeout(function () {
                    Fav.Update(favId, 'delete');
                }, 300);
            } else {
                FAV[favId].favTimeout = setTimeout(function () {
                    Fav.Update(favId, 'add');
                }, 300);
            }

        });
    }
};

Fav.Update = function (favId, action) {
    BX.ajax({
        url: FAV[favId].pathToAjax,
        method: 'POST',
        dataType: 'json',
        data: {
            'FAV': 'Y',
            'FAV_TYPE_ID': FAV[favId].entityTypeId,
            'FAV_ENTITY_ID': FAV[favId].entityId,
            'LINK_TITLE': FAV[favId].title,
            'LINK_URL': FAV[favId].url,
            'FAV_ACTION': action,
            'sessid': BX.bitrix_sessid()
        },
        onsuccess: function (data) {
            Fav.UpdateStatus(favId, action);
        },
        onfailure: function (data) {}
    });


    return false;
};
Fav.UpdateStatus = function (favId, action) {
    if (action == 'delete')
        BX.removeClass(FAV[favId].button, 'fav_active');
    else
        BX.addClass(FAV[favId].button, 'fav_active');

};






