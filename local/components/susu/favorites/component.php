<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult['HLBLOCK_ID'] = HLBLOCK_ID__FAVORITES;
$arResult['ALLOW_FAV'] = false;
$arResult['FAV_AVAILABLE'] = isset($arParams['FAV_AVAILABLE'])? $arParams['FAV_AVAILABLE'] == 'Y'? 'Y': 'N' : 'Y';
if ($arResult['FAV_AVAILABLE'] == 'Y')
    $arResult['ALLOW_FAV'] = true;


$arResult['ENTITY_TYPE_ID'] = htmlspecialcharsbx($arParams['ENTITY_TYPE_ID']);
$arResult['ENTITY_ID'] = IntVal($arParams['ENTITY_ID']);
$arResult['OWNER_ID'] = $USER->GetID();
$arResult['LINK_TITLE'] = htmlspecialcharsbx($arParams['LINK_TITLE']);
$arResult['LINK_URL'] = htmlspecialcharsbx($arParams['LINK_URL']);


$arResult['AJAX_MODE'] = $arParams['AJAX_MODE'] != 'Y'? 'N': 'Y';

if (!$arResult['ALLOW_FAV'])
	$arResult['FAV_AVAILABLE'] = 'N';

$arResult['FAV_ID'] = $arResult['ENTITY_TYPE_ID'].'-'.$arResult['ENTITY_ID'].'-'.(time()+rand(0, 1000));

$arFilter = Array(
    "UF_ENTITY_TYPE_ID" => $arResult['ENTITY_TYPE_ID'],
    "UF_ENTITY_ID" => $arResult['ENTITY_ID'],
    "UF_OWNER_ID" => $arResult['OWNER_ID'],
);
$arrRes = \Local\Lib\Content\Highload::getList($arResult['HLBLOCK_ID'], $arFilter, array(), $arSelect = array('ID'));
if ($arrRes && $arrRes != array()){
    $arResult['USER_HAS_FAV'] = true;
} else {
    $arResult['USER_HAS_FAV'] = false;
}


//$APPLICATION->AddHeadScript($componentTemplate."/script.js");

$this->IncludeComponentTemplate();


return $arResult;
