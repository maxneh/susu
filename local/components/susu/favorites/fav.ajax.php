<?
define("PUBLIC_AJAX_MODE", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("NO_AGENT_CHECK", true);
define("DisableEventsCheck", true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


$inPersonalSection = false;
if ($_POST['FAV'] == 'Y' && $USER->IsAuthorized()) {
    $data['UF_ENTITY_TYPE_ID'] = $_POST['FAV_TYPE_ID'];
    $data['UF_ENTITY_ID'] = intval($_POST['FAV_ENTITY_ID']);
    $action = htmlspecialchars($_POST['FAV_ACTION']);
    $data['UF_LINK_TITLE'] = htmlspecialchars($_POST['LINK_TITLE']);
    $data['UF_LINK_URL'] = htmlspecialchars($_POST['LINK_URL']);
    $data['UF_OWNER_ID'] = $USER->GetId();
    if (isset($_POST['FAV_ID']) && $action == 'delete') {
        $data['ID'] =  htmlspecialchars($_POST['FAV_ID']);
        $inPersonalSection = true;
    }

        $APPLICATION->RestartBuffer();
        $arComponentFavResult = $arJSON = GetFavResult($action, $data, $inPersonalSection);
} else {
    $arComponentFavResult = array('result' => 0, 'error' => 'не разрешено');
}

Header('Content-Type: application/x-javascript; charset=' . LANG_CHARSET);
echo CUtil::PhpToJsObject($arComponentFavResult);

function GetFavResult($action, $data, $inPersonalSection)
{

    CModule::includeModule('highloadblock');
    switch ($action) {
        case 'add':
            unset($action);
            if (strlen($data['UF_ENTITY_TYPE_ID']) > 0 && intval($data['UF_ENTITY_ID']) > 0)
                $result = \Local\Lib\Content\Highload::add(HLBLOCK_ID__FAVORITES, $data);
            else return array('result' => 0, 'error' => 'не все обязательные параметры');
            break;
        case 'delete':

            if ($inPersonalSection) {
                \Local\Lib\Content\Highload::delete(HLBLOCK_ID__FAVORITES, $data['ID']);
            } else {
                $arrRes = \Local\Lib\Content\Highload::getList(HLBLOCK_ID__FAVORITES, $data, array(), $arSelect = array('ID'));
                foreach ($arrRes as $item) {
                    \Local\Lib\Content\Highload::delete(HLBLOCK_ID__FAVORITES, $item['ID']);
                }
            }


            break;
        default:
            die();
    }


    return Array('result' => 1);
}

CMain::FinalActions();
die();