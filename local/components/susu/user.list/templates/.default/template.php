<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
    <!--TPL: <?= $this->GetFolder()?>-->
<? if (strlen($arResult["ERROR_MESSAGE"]) <= 0): ?>
    <? if (count($arResult["SEARCH_RESULT"]) > 0): ?>
        <div class="row">
            <? foreach ($arResult["SEARCH_RESULT"] as $v): ?>
                <div class="user-list__item col-xs-12 col-md-4">
                    <div class="row">
                        <div class="user-list__image col-xs-3"> <?= $v["IMAGE_IMG"] ?></div>
                        <div class="user-list__properties col-xs-9">
                            <p class="small-text"> <?= $v['WORK_POSITION']?></p>
                            <p class=""><a href="<?= $v["URL"] ?>"><span><?=$v["NAME"]?> <?=$v["SECOND_NAME"]?> <?=$v["LAST_NAME"]?></span></a></p>
                            <p class="small-text"> <?= $v['WORK_PROFILE']?></p>
                            <p>  <?= $v['WORK_PHONE'] ?></p>
                            <p>  <?= $v['PERSONAL_PHONE'] ?></p>
                            <p>  <?= $v['EMAIL']?></p>
                            <? if ($v["UserPropertiesMain"]["SHOW"] == "Y"): ?>
                                <? foreach ($v["UserPropertiesMain"]["DATA"] as $fieldName => $arUserField): ?>
                                    <? if ((is_array($arUserField["VALUE"]) && count($arUserField["VALUE"]) > 0) || (!is_array($arUserField["VALUE"]) && StrLen($arUserField["VALUE"]) > 0)): ?>
                                        <?//= $arUserField["EDIT_FORM_LABEL"] ?>
                                        <?
                                        if ($arUserField["FIELD_NAME"] == "UF_DEPARTMENT")
                                            $arUserField["SETTINGS"]["SECTION_URL"] = $arParams["PATH_TO_CONPANY_DEPARTMENT"];
                                        $APPLICATION->IncludeComponent(
                                            "bitrix:system.field.view",
                                            $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                            array("arUserField" => $arUserField),
                                            null,
                                            array("HIDE_ICONS" => "Y")
                                        );
                                        ?>
                                        <br/>
                                    <? endif; ?>
                                <? endforeach; ?>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
        <? if (strlen($arResult["NAV_STRING"]) > 0): ?>
            <p><?= $arResult["NAV_STRING"] ?></p>
        <? endif; ?>

    <? else: ?>
            <?= GetMessage("SONET_C241_T_NOT_FOUND") ?>
    <? endif; ?>
<? else: ?>
    <?= ShowError($arResult["ERROR_MESSAGE"]); ?>
<? endif; ?>