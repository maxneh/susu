<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/********************************************************************
 * Input params
 ********************************************************************/
$arParams["ALBUM_PHOTO_SIZE"] = intVal($arParams["ALBUM_PHOTO_SIZE"]);

/********************************************************************
 * /Input params
 ********************************************************************/

// TODO: get rid of this
CAjax::Init();
// TODO: get rid of this too
$GLOBALS['APPLICATION']->AddHeadScript('/bitrix/js/main/utils.js');

$GLOBALS['APPLICATION']->AddHeadScript('/bitrix/components/bitrix/photogallery/templates/.default/script.js');

?>
    <!--TPL: <?= $this->GetFolder()?>-->

<? if ($arParams['USE_FILTER'] == 'Y') {
    $APPLICATION->IncludeComponent(
        "susu:photogallery.filter",
        "",
        Array(
            "HLBLOCK_ID" => HLBLOCK_ID__GALLERY_CAT,
        ),
        $component,
        array("HIDE_ICONS" => "Y")
    );
} ?>

<?

if (empty($arResult["SECTIONS"])):?>
    <div class="photo-info-box photo-info-box-sections-list-empty">
        <div class="photo-info-box-inner"><?= GetMessage("P_EMPTY_DATA") ?></div>
    </div>
    <?
    return false;
endif; ?>

    <div class="row photo-items-list photo-album-list<? if ($arParams['PHOTO_LIST_MODE'] == "Y") {
        echo " photo-album-list-first-photos";
    } ?>">
        <? foreach ($arResult["SECTIONS"] as $res): ?>

            <div class="col-xs-12 col-sm-4 photo-album-item photo-album-<?= ($res["ACTIVE"] != "Y" ? "nonactive" : "active") ?> <?= (
            !empty($res["PASSWORD"]) ? "photo-album-password" : "") ?>" id="photo_album_info_<?= $res["ID"] ?>" <?
            if ($res["ACTIVE"] != "Y" || !empty($res["PASSWORD"])) {
                $sTitle = GetMessage("P_ALBUM_IS_NOT_ACTIVE");
                if ($res["ACTIVE"] != "Y" && !empty($res["PASSWORD"]))
                    $sTitle = GetMessage("P_ALBUM_IS_NOT_ACTIVE_AND_PASSWORDED");
                elseif (!empty($res["PASSWORD"]))
                    $sTitle = GetMessage("P_ALBUM_IS_PASSWORDED");
                ?> title="<?= $sTitle ?>" <?
            }
            ?>>


                <? if ($arParams['PHOTO_LIST_MODE'] == "Y"): ?>
                <? if ($res["ACTIVE"] != "Y" || !empty($res["PASSWORD"])) {
                    $sTitle = GetMessage("P_ALBUM_IS_NOT_ACTIVE");
                    if ($res["ACTIVE"] != "Y" && !empty($res["PASSWORD"]))
                        $sTitle = GetMessage("P_ALBUM_IS_NOT_ACTIVE_AND_PASSWORDED");
                    elseif (!empty($res["PASSWORD"]))
                        $sTitle = GetMessage("P_ALBUM_IS_PASSWORDED");
                    $sTitle = ' - ' . $sTitle;
                } ?>

                <div class="album-photos-section">
                    <a href="<?= $res["LINK"] ?>" class="preview-album">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:photogallery.detail.list",
                            "morealbums",
                            Array(

                                "BEHAVIOUR" => (isset($arParams["BEHAVIOUR"]) && $arParams["BEHAVIOUR"] != "") ? $arParams["BEHAVIOUR"] : "SIMPLE",
                                "USER_ALIAS" => $arParams["USER_ALIAS"],

                                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                "SECTION_ID" => $res["ID"],
                                "DRAG_SORT" => "N",
                                "MORE_PHOTO_NAV" => "N",
                                "THUMBNAIL_SIZE" => $arParams["SECTION_LIST_THUMBNAIL_SIZE"],
                                "SHOW_CONTROLS" => "Y",
                                "SHOW_RATING" => "Y",
                                "SHOW_SHOWS" => "N",
                                "SHOW_COMMENTS" => "Y",
                                "MAX_VOTE" => $arParams["MAX_VOTE"],
                                "VOTE_NAMES" => array(),
                                "DISPLAY_AS_RATING" => $arParams["DISPLAY_AS_RATING"],
                                "SET_TITLE" => "N",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => $arParams["CACHE_TIME"],
                                "CACHE_NOTES" => "",
                                "ELEMENT_LAST_TYPE" => "none",
                                "ELEMENT_FILTER" => array("INCLUDE_SUBSECTIONS" => "Y"),
                                "RELOAD_ITEMS_ONLOAD" => "Y",
                                "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                                "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                                "ELEMENT_SORT_FIELD1" => $arParams["ELEMENT_SORT_FIELD1"],
                                "ELEMENT_SORT_ORDER1" => $arParams["ELEMENT_SORT_ORDER1"],
                                "PROPERTY_CODE" => array(),
                                "INDEX_URL" => $arParams["~INDEX_URL"],
                                "DETAIL_URL" => $arParams["~DETAIL_URL"],
                                "GALLERY_URL" => $arParams["~GALLERY_URL"],
                                "SECTION_URL" => $arParams["~SECTION_URL"],
                                "DETAIL_EDIT_URL" => $arParams["~DETAIL_EDIT_URL"],
                                "PERMISSION" => $arParams["PERMISSION"],
                                "GROUP_PERMISSIONS" => array(),
                                "PAGE_ELEMENTS" => $arParams["SHOWN_ITEMS_COUNT"],
                                "DATE_TIME_FORMAT" => $arParams["DATE_TIME_FORMAT_DETAIL"],
                                "SET_STATUS_404" => "N",
                                "ADDITIONAL_SIGHTS" => array(),
                                "PICTURES_SIGHT" => "real",
                                "USE_COMMENTS" => $arParams["USE_COMMENTS"],
                                "COMMENTS_TYPE" => $arParams["COMMENTS_TYPE"],
                                "FORUM_ID" => $arParams["FORUM_ID"],
                                "USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
                                "POST_FIRST_MESSAGE" => $arParams["POST_FIRST_MESSAGE"] == "N" ? "N" : "Y",
                                "SHOW_LINK_TO_FORUM" => "N",
                                "BLOG_URL" => $arParams["~BLOG_URL"],
                                "PATH_TO_BLOG" => $arParams["~PATH_TO_BLOG"],
                                // Display user
                                "PATH_TO_USER" => $arParams["~PATH_TO_USER"],
                                "NAME_TEMPLATE" => $arParams["NAME_TEMPLATE"],
                                "SHOW_LOGIN" => $arParams["SHOW_LOGIN"],
                                "~UNIQUE_COMPONENT_ID" => "bxfg_ucid_from_req_" . $arParams["IBLOCK_ID"] . "_" . $res["ID"],
                                "ACTION_URL" => $res["~LINK"]
                            ),
                            $component,
                            array("HIDE_ICONS" => "Y")
                        ); ?>
                    </a>
                    <? else: ?>
                        <span class="album-no-photos"><?= GetMessage('P_NO_PHOTOS') ?></span>
                    <? endif; ?>
                </div>
                <div class="album-top-section">
                    <a class="album-name" href="<?= $res["LINK"] ?>" title="<?= $res["NAME"] . $sTitle ?>"><?= $res["NAME"] ?></a>
                    <? if (!empty($res["PASSWORD"])): ?>
                        <span class="album-passworded">(<?= GetMessage("P_ALBUM_IS_PASSWORDED_SHORT") ?>)</span>
                    <? endif; ?>
                    <? /* if($res["DATE"]):?>
                    <span class="album-date"><?= $res["DATE"]?></span>
                <?endif;*/ ?>
                    <? if ($res["ELEMENTS_CNT"] > 0): ?>
                        <span class="album-photos">(<a class="more-photos" href="<?= $res["LINK"] ?>"
                                                       title="<?= GetMessage('P_OTHER_PHOTOS_TITLE') ?>"><?= $res["ELEMENTS_CNT"] . " " . GetMessage('P_SECT_PHOTOS') ?></a>)</span>
                    <? endif; ?>
                </div>


            </div>
        <? endforeach; ?>
    </div>
    <div class="empty-clear"></div>

<?
if (!empty($arResult["NAV_STRING"])):
    ?>
    <div class="photo-navigation photo-navigation-bottom">
        <?= $arResult["NAV_STRING"] ?>
    </div>
<?
endif;
?>