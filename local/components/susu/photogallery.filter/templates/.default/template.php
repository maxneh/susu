<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (!empty($arResult['ERROR']))
{
echo $arResult['ERROR'];
return false;
}

$request = \Bitrix\Main\Context::getCurrent()->getRequest();

?>
<!--TPL: <?= $this->GetFolder()?>-->

<div class="gallery-filter">
    <a href="#" class="reset-filter" data-action="del_filter"><span>Сбросить фильтр</span></a>
    <form action="" method="post" id="gallery-filter-form">
        <span class="gallery-filter__text">Рубрика: </span>

        <?  foreach ($arResult['UF_GALLERY_CATS'] as $item) { ?>
            <label class="album-filter-category checkmark checkmark_inv">
                <input type="checkbox" name="<?=$arParams['FILTER_NAME']?>_UF_GALLERY_CAT[]" value="<?= $item['ID'] ?>" class="checkmark__input filter-control"
                    <?= in_array($item['ID'], $request->getPost($arParams['FILTER_NAME']."_UF_GALLERY_CAT")) ? 'checked' : '' ?>>
                <div class="checkmark__icon icon icon-<?= $item['UF_CAT_ICON'] ?>" title="<?=$item['UF_GALLERY_CAT']?>"></div>
            </label>
        <? } ?>
        &nbsp;
        <span class="gallery-filter__text">Дата: </span>
        <label class="gallery-filter__line"><span class="gallery-filter__text">c</span>
            <input class="date-control filter-control" type="text" name="<?=$arParams['FILTER_NAME']?>_>=UF_DATE" onclick="BX.calendar({node: this, field: this,
            bTime: false});"
                       value="<?= $request->getPost($arParams['FILTER_NAME'].'_>=UF_DATE') ?>"><span class="date-control-icon"></span>
        </label>
        &nbsp;
        <label class="gallery-filter__line"><span class="gallery-filter__text">до</span>
            <input class="date-control filter-control" type="text" name="<?=$arParams['FILTER_NAME']?>_<=UF_DATE" onclick="BX.calendar({node: this, field:
        this, bTime: false});"
                        value="<?= $request->getPost($arParams['FILTER_NAME'].'_<=UF_DATE') ?>"><span class="date-control-icon"></span>
        </label>
        <button type="submit" class="hide" style="margin-left: 10px;">Выбрать</button>
    </form>
</div>

<script>
    $(document).ready(function () {
        $('.reset-filter').on('click', function (e) {
            e.preventDefault();
            var form = $('#gallery-filter-form');
            //  form.append('<input type="hidden" name="del_filter" value="Y">');
            form[0].reset();
            var data = BX.ajax.prepareData(BX.ajax.prepareForm(BX('gallery-filter-form')).data);
            filterAjaxSend(data);
            return false;
        });

        $('.filter-control').each(function () {
            $(this).on('change', function (event) {
                var data = BX.ajax.prepareData(BX.ajax.prepareForm(BX('gallery-filter-form')).data);
                filterAjaxSend(data);
            });
        });

        function filterAjaxSend(data) {
            BX.ajax({
                url: '/gallery/ajax.php',
                data: data,
                method: 'POST',
                // dataType: 'raw',
                timeout: 30,
                async: true,
                processData: true,
                scriptsRunFirst: true,
                emulateOnload: true,
                start: true,
                cache: false,
                onsuccess: function (data) {
                    $(".photogallery-list-sliders").html(data);
                    $('.preview-album-slider').slick({
                        dots: true,
                        autoplay: false,
                        fade: true,
                        autoplaySpeed: 400,
                        arrows: false,

                    });
                },
                onfailure: function () {

                }
            });
        }
    });

</script>