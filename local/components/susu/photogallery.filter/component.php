<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Loader::includeModule('highloadblock');

$requiredModules = array('highloadblock');
foreach ($requiredModules as $requiredModule)
{
    if (!CModule::IncludeModule($requiredModule))
    {
        ShowError(GetMessage("F_NO_MODULE"));
        return 0;
    }
}

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

/********************************************************************
 * Input params
 ********************************************************************/

$arParams["HLBLOCK_ID"] = intval($arParams["HLBLOCK_ID"]);
$arParams["FILTER_NAME"] = isset($arParams["FILTER_NAME"])? $arParams["FILTER_NAME"] : 'arrFilter';

/********************************************************************
 * /Input params
 ********************************************************************/

// hlblock info
$hlblock_id = $arParams['HLBLOCK_ID'];
if (empty($hlblock_id))
{
    ShowError(GetMessage('HLBLOCK_LIST_NO_ID'));
    return 0;
}
$hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
if (empty($hlblock))
{
    ShowError(GetMessage('HLBLOCK_LIST_404'));
    return 0;
}

// check rights
if (isset($arParams['CHECK_PERMISSIONS']) && $arParams['CHECK_PERMISSIONS'] == 'Y' && !$USER->isAdmin())
{
    $operations = HL\HighloadBlockRightsTable::getOperationsName($hlblock_id);
    if (empty($operations))
    {
        ShowError(GetMessage('HLBLOCK_LIST_404'));
        return 0;
    }
}


$entity = HL\HighloadBlockTable::compileEntity($hlblock);


// uf info
$fields = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('HLBLOCK_'.$hlblock['ID'], 0, LANGUAGE_ID);

// start query
$mainQuery = new Entity\Query($entity);
$mainQuery->setSelect(array('*'));

// execute query
//	->setGroup($group)
//	->setOptions($options);
$result = $mainQuery->exec();
$result = new CDBResult($result);

// build results
$rows = array();
$tableColumns = array();
while ($row = $result->fetch())
{
    foreach ($row as $k => $v)
    {
        $arUserField = $fields[$k];

        if ($k == 'ID')
        {
            $tableColumns['ID'] = true;
            continue;
        }
        if ($arUserField['SHOW_IN_LIST'] != 'Y')
        {
            continue;
        }

        $html = call_user_func_array(
            array($arUserField['USER_TYPE']['CLASS_NAME'], 'getadminlistviewhtml'),
            array(
                $arUserField,
                array(
                    'NAME' => 'FIELDS['.$row['ID'].']['.$arUserField['FIELD_NAME'].']',
                    'VALUE' => htmlspecialcharsbx(is_array($v) ? implode(', ', $v) : $v)
                )
            )
        );

        $tableColumns[$k] = true;
        $row[$k] = $html;
    }

    $rows[] = $row;
}

$arResult['rows'] = $rows;
$arResult['fields'] = $fields;
$arResult['tableColumns'] = $tableColumns;


$this->IncludeComponentTemplate();
