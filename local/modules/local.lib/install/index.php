<?
	Class local_lib extends CModule
	{
	    public $MODULE_ID = "local.lib";
	    public $MODULE_NAME;
        public $MODULE_VERSION;
        public $MODULE_VERSION_DATE;
        public $MODULE_DESCRIPTION;
        public $PARTNER_NAME;

        public function __construct()
        {
            $arModuleVersion = array();

            $path = str_replace('\\', '/', __FILE__);
            $path = substr($path, 0, strlen($path) - strlen("/index.php"));
            include($path."/version.php");

            if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
            {
                $this->MODULE_VERSION = $arModuleVersion["VERSION"];
                $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            }

            $this->MODULE_NAME = 'Библиотека функций';
            $this->MODULE_DESCRIPTION = 'Библиотека вспомогательных функций для проекта';
            $this->PARTNER_NAME = 'MaxNeh';
        }

	    function DoInstall()
	    {
            RegisterModule($this->MODULE_ID);
	    }

	    function DoUninstall()
	    {
            UnRegisterModule($this->MODULE_ID);


	    }
	}
