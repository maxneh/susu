<?php
/**
 * Created by PhpStorm.
 * User: MaxNeh
 * Date: 14.02.2018
 * Time: 14:19
 */

namespace Local\Lib\Handlers;

use Bitrix\Main\Loader;
use CFormResult;
use CIMNotify;
use CIBlockSection;

class Form
{

    public static function onAfterResultAdd($WEB_FORM_ID, $resultId)
    {

        Loader::includeModule("iblock");
        Loader::includeModule("form");

        global $APPLICATION;
        $depArr = array();

        // действие обработчика распространяется только на форму с ID=3 и др.
        if ($WEB_FORM_ID == WEB_FORM_ID__SERVICE || $WEB_FORM_ID == WEB_FORM_ID__IDEA || $WEB_FORM_ID == WEB_FORM_ID__HONOR) {

            for ($i = 1; $i <= 3; $i++) {
                $prev_dep = $_REQUEST['form_dropdown_department_' . ($i-1)];
                $curr_dep = $_REQUEST['form_dropdown_department_' . $i];
                if ($prev_dep  == $curr_dep) continue;
                $res = CIBlockSection::GetByID($curr_dep);
                if ($ar_res = $res->GetNext()) $depArr[] = $ar_res['NAME'];
            }

            CFormResult::SetField($resultId, 'department', implode('/', $depArr));

        }
    }

    public static function onAfterResultStatusChange($WEB_FORM_ID, $RESULT_ID, $NEW_STATUS_ID, $CHECK_RIGHTS)
    {
        Loader::includeModule("form");
        Loader::includeModule("im");
    //    global $USER;

        $arAnswer = CFormResult::GetDataByID($RESULT_ID, array(), $arResult,$arAnswer2);

        $text = mb_strimwidth(strip_tags($arAnswer['message'][0]['USER_TEXT']), 0, 100, '...');
        $arMessageFields = array(
            "FROM_USER_ID" => 0,
            "TO_USER_ID" => $arResult['USER_ID'],
            "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
            "NOTIFY_MODULE" => "form",
            "NOTIFY_TAG" => "",
            "NOTIFY_MESSAGE" => "Статус вашей заявки сменился на &laquo;{$arResult['STATUS_TITLE']}&raquo;. Вы писали: {$text} от {$arResult["DATE_CREATE"]}",
        );

        CIMNotify::Add($arMessageFields);
    }
}