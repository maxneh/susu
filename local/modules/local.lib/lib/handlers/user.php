<?php
/**
 * Created by PhpStorm.
 * User: MaxNeh
 * Date: 14.02.2018
 * Time: 14:19
 */

namespace Local\Lib\Handlers;

//use Bitrix\Main\Loader;
//use CFormResult;
//use CIMNotify;
//use CIBlockSection;
use CUser;

class User
{

    function OnAfterUserAddHandler(&$arFields)
    {
        if($arFields["ID"]>0) {
         //   AddMessage2Log("Запись с кодом " . $arFields["ID"] . " добавлена.");
            /*
            * 6 - зарегистрированные пользователи
            * 11 - право голосовать за рейтинг
            * 12 - право голосовать за авторитет
            */
            CUser::SetUserGroup($arFields["ID"], array_merge(CUser::GetUserGroup($arFields["ID"]), array(6,11,12) ));
        //    if (!empty($arFields["PERSONAL_BIRTHDAY"])) AddMessage2Log("ДР пользователя " . $arFields["PERSONAL_BIRTHDAY"] . " добавлено.");
        } else
            AddMessage2Log("Ошибка добавления записи (".$arFields["RESULT_MESSAGE"].").");
    }

 /*   function OnAfterUserUpdateHandler(&$arFields)
    {
        if($arFields["RESULT"]) {
            AddMessage2Log("Запись с кодом " . $arFields["ID"] . " изменена.");
            if (!empty($arFields["PERSONAL_BIRTHDAY"])) AddMessage2Log("ДР пользователя " . $arFields["PERSONAL_BIRTHDAY"] . " добавлено.");
        } else
            AddMessage2Log("Ошибка изменения записи ".$arFields["ID"]." (".$arFields["RESULT_MESSAGE"].").");
    }*/
}