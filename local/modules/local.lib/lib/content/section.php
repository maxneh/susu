<?php
/**
 * Created by PhpStorm.
 * User: MaxNeh
 * Date: 14.02.2018
 * Time: 14:09
 */

namespace Local\Lib\Content;

use Bitrix\Main\Loader;
use CIBlockSection;


class Section
{
    /**
     * @param $arFilter
     * @param $arSelect
     * @return mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getTreeArr($arFilter, $arSelect)
    {
        Loader::includeModule("iblock");

        $dbSection = CIBlockSection::GetList(
            Array(
                'LEFT_MARGIN' => 'ASC',
            ),
            array_merge(
                Array(
                    'ACTIVE' => 'Y',
                    'GLOBAL_ACTIVE' => 'Y'
                ),
                is_array($arFilter) ? $arFilter : Array()
            ),
            false,
            array_merge(
                Array(
                    'ID',
                    'IBLOCK_SECTION_ID'
                ),
                is_array($arSelect) ? $arSelect : Array()
            )
        );

        while( $arSection = $dbSection-> GetNext(true, false) ){

            $SID = $arSection['ID'];
            $PSID = (int) $arSection['IBLOCK_SECTION_ID'];

            $arLinks[$PSID]['CHILDS'][$SID] = $arSection;

            $arLinks[$SID] = &$arLinks[$PSID]['CHILDS'][$SID];
        }


        return array_shift($arLinks);
    }
}