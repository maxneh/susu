<?php
/**
 * Created by PhpStorm.
 * User: MaxNeh
 * Date: 27.02.2018
 * Time: 13:34
 */


namespace Local\Lib\Content;

use Bitrix\Highloadblock\HighloadBlockTable as HL;
use Bitrix\Main\Loader;

class Highload
{
    /**
     * @param $hlId
     * @param $sID
     * @return null
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\SystemException
     */

    public static function getById($hlId, $sID)
    {

        $entity_data_class = self::GetEntityDataClass($hlId);

        $result = null;

        if (!empty($sID)) {
            $row = $entity_data_class::getById($sID)->fetch();
            if (!empty($row)) {
                foreach ($row as $key => $val) {
                    $result[$key] = $val;
                }
            }
        }
        return $result;

    }

    /**
     * @param $hlId
     * @param array $arFilter
     * @param array $arOrder
     * @param array $arSelect
     * @return array|null
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getList($hlId, $arFilter = array(), $arOrder = array(), $arSelect = array('*'))
    {
        if (!$hlId || empty($hlId)) return null;
        $result = null;
        $entity_data_class = self::GetEntityDataClass($hlId);
        $rsData = $entity_data_class::getList(array(
            "select" => $arSelect,
            "filter" => $arFilter,
            "order" => $arOrder
        ));

        while ($arRes = $rsData->Fetch()) {
            $result[] = $arRes;
        }

        return $result;
    }

    /**
     * @param $hlId
     * @param $data
     * @return \Bitrix\Main\Entity\AddResult|bool
     * @throws \Exception
     */
    public static function add($hlId, $data)
    {
        $result = false;

        if (is_array($data)) {
            $entity_data_class = self::GetEntityDataClass($hlId);
            $result = $entity_data_class::add($data);
        }
        return $result;
    }

    /**
     * @param $hlId
     * @param $sID
     * @return \Bitrix\Main\Entity\DeleteResult|bool
     * @throws \Exception
     */
    public static function delete($hlId, $sID)
    {
        $result = false;
        if (!empty($sID)) {
            $entity_data_class = self::GetEntityDataClass($hlId);
            $result = $entity_data_class::delete($sID);
        }
        return $result;
    }

    /**
     * @param $hlId
     * @param $sID
     * @param $data
     * @return \Bitrix\Main\Entity\UpdateResult|bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function update($hlId, $sID, $data)
    {
        $result = false;

        if (is_array($data) && !empty($sID)) {
            $entity_data_class = self::GetEntityDataClass($hlId);
            $result = $entity_data_class::update($sID, $data);

        }
        return $result;
    }

    /**
     * @param $hlId
     * @return \Bitrix\Main\Entity\DataManager|bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    private static function GetEntityDataClass($hlId)
    {

        Loader::includeModule('highloadblock');

        if (empty($hlId) || $hlId < 1) {
            return false;
        }
        $hlblock = HL::getById($hlId)->fetch();
        $entity = HL::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        return $entity_data_class;
    }
}