<?
/**
 * Created by PhpStorm.
 * User: MaxNeh
 * Date: 14.02.2018
 * Time: 14:09
 */

namespace Local\Lib\Content;

use CCalendar;
use CCalendarType;
use CCalendarEvent;


class MyCalendar
{

    public static function GetNearestEventsList($params = array())
    {
        $type = $params['bCurUserList'] ? 'user' : $params['type'];

// Get current user id
        if (!isset($params['userId']) || $params['userId'] <= 0)
            $curUserId = CCalendar::GetCurUserId();
        else
            $curUserId = intval($params['userId']);

        if (!CCalendarType::CanDo('calendar_type_view', $type, $curUserId))
            return 'access_denied';


        $arFilter = array(
            'CAL_TYPE' => $type,
            'FROM_LIMIT' => $params['fromLimit'],
            'TO_LIMIT' => $params['toLimit'],
            'DELETED' => 'N',
            'ACTIVE_SECTION' => 'Y'
        );

        if ($params['bCurUserList'])
            $arFilter['OWNER_ID'] = $curUserId;

        if (isset($params['sectionId']) && $params['sectionId'])
            $arFilter["SECTION"] = $params['sectionId'];

        if ($type == 'user')
            unset($arFilter['CAL_TYPE']);

        $arEvents = CCalendarEvent::GetList(
            array(
                'arFilter' => $arFilter,
                'parseRecursion' => true,
                'fetchAttendees' => true,
                'userId' => $curUserId,
                'fetchMeetings' => $type == 'user',
                'preciseLimits' => true,
                'skipDeclined' => true
            )
        );

        if (CCalendar::Date(time(), false) == $params['fromLimit'])
            $limitTime = time();
        else
            $limitTime = CCalendar::Timestamp($params['fromLimit']);

        $arResult = array();
        $serverOffset = intVal(date("Z"));

        foreach ($arEvents as $event) {
            if ($event['IS_MEETING'] && $event["MEETING_STATUS"] == 'N')
                continue;

            if ($type == 'user' && !$event['IS_MEETING'] && $event['CAL_TYPE'] != 'user')
                continue;

// $serverToTs = timestamp in utc + server offset;
            $serverToTs = (CCalendar::Timestamp($event['DATE_TO']) - $event['TZ_OFFSET_TO']) + $serverOffset;
            if ($event['DT_SKIP_TIME'] == 'Y') {
                $serverToTs += self::DAY_LENGTH;
            }

            if ($serverToTs >= $limitTime) {
                $fromTs = CCalendar::Timestamp($event['DATE_FROM']);
                $toTs = CCalendar::Timestamp($event['DATE_TO']);
                if ($event['DT_SKIP_TIME'] !== "Y") {
                    $fromTs -= $event['~USER_OFFSET_FROM'];
                    $toTs -= $event['~USER_OFFSET_TO'];
                }
                $event['DATE_FROM'] = CCalendar::Date($fromTs, $event['DT_SKIP_TIME'] != 'Y');
                $event['DATE_TO'] = CCalendar::Date($toTs, $event['DT_SKIP_TIME'] != 'Y');
                unset($event['TZ_FROM'], $event['TZ_TO'], $event['TZ_OFFSET_FROM'], $event['TZ_OFFSET_TO']);
                $event['DT_FROM_TS'] = $fromTs;
                $event['DT_TO_TS'] = $toTs;

                $arResult[] = $event;
            }
        }

// Sort by DATE_FROM_TS
        usort($arResult, array('CCalendar', '_NearestSort'));
        return $arResult;
    }
}

?>