<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<? CModule::IncludeModule("iblock");
CModule::IncludeModule("form");

$arDropDown = array();
$arSelect = Array("ID", "NAME", "DEPTH_LEVEL");
$arFilter = Array("IBLOCK_ID" => 15, "SECTION_ID" => (int)$_GET["id"], "ACTIVE" => "Y");
$arDropDown = array(
    "reference" => array('Не выбрано'),
    "reference_id" => array((int)$_GET["id"])

);
$rsSect = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter, false, $arSelect);

if (intval($rsSect->SelectedRowsCount()) > 0 && (int)$_GET["id"] && (int)$_GET["uniqId"]) { ?>
    <?
$uniqId = (int)$_GET["uniqId"];
    while ($arSect = $rsSect->GetNext()) {
        $arDropDown["reference"][] = $arSect["NAME"];
        $arDropDown["reference_id"][] = $arSect["ID"];
        $level = $arSect["DEPTH_LEVEL"];
    }
    $QUESTION_SID = "department_" . $level;
    ++$level; //next level
    $value = CForm::GetDropDownValue($QUESTION_SID, $arDropDown); ?>
    <?= CForm::GetDropDownField(
        $QUESTION_SID,
        $arDropDown,
        $value,
        // "class=\"inputselect\""
        "class=\"inputselect w100\" onchange=\"loadHTML('/local/ajax/departments.php?uniqId=$uniqId&id=' + this . options[this . selectedIndex] . value, 'd_level_$level"."_".$uniqId."');\""
    ); ?>

<? } ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php"); ?>