<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Page\Asset;
?>
<? if ($USER->IsAuthorized()) { ?>
    <div class="baron__free">
        <div class="baron__bar"></div>
    </div>
    </div>
    </main>

        <div class="right-panel">
            <div id="im-right-wrap">
                <? $APPLICATION->IncludeComponent("bitrix:im.messenger", "right-panel", Array(), false, array("HIDE_ICONS" => "Y")); ?>
            </div>

            <? $APPLICATION->IncludeComponent(
                "susu:birthdays.friends",
                "",
                Array(
                    "DAYS_AFTER" => "60",
                    "FORMAT" => "j F",
                    "TITLE" => "Дни рождения друзей",
                    "TITLE_LINK" => "/friends-birthdays/",
                    "COUNT_ITEMS" => 2,
                    "THUMBNAIL_LIST_SIZE" => 42,
                    "PATH_TO_USER" => '/club/user/#user_id#/',
                    "NAME_TEMPLATE" => "#NAME# #LAST_NAME#",
                )
            ); ?>

        </div>
    <footer class="footer">

        <? $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "bottom",
            array(
                "ROOT_MENU_TYPE" => "bottom",
                "MAX_LEVEL" => "2",
                "MENU_CACHE_TYPE" => "A",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "N",
                "MENU_CACHE_GET_VARS" => array(),
                "COMPONENT_TEMPLATE" => "bottom",
                "CHILD_MENU_TYPE" => "left",
                "USE_EXT" => "Y",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N",
                "MENU_THEME" => "site"
            ),
            false
        ); ?>

    </footer>

    </div>


    <div id="popup-form-idea" class="popup-form">
        <? $APPLICATION->IncludeComponent(
            "bitrix:form",
            "popup",
            Array(
                "AJAX_MODE" => "Y",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",

                "CACHE_TIME" => "3600",
                "CACHE_TYPE" => "A",
                "CHAIN_ITEM_LINK" => "",
                "CHAIN_ITEM_TEXT" => "",
                "EDIT_ADDITIONAL" => "N",
                "EDIT_STATUS" => "N",
                "IGNORE_CUSTOM_TEMPLATE" => "N",
                "NOT_SHOW_FILTER" => array("", ""),
                "NOT_SHOW_TABLE" => array("", ""),
                "RESULT_ID" => "",
                "SEF_MODE" => "N",
                "SHOW_ADDITIONAL" => "N",
                "SHOW_ANSWER_VALUE" => "N",
                "SHOW_EDIT_PAGE" => "N",
                "SHOW_LIST_PAGE" => "N",
                "SHOW_STATUS" => "N",
                "SHOW_VIEW_PAGE" => "N",
                "START_PAGE" => "new",
                "SUCCESS_URL" => "",
                "USE_EXTENDED_ERRORS" => "N",
                "VARIABLE_ALIASES" => Array("action" => "action"),
                "WEB_FORM_ID" => WEB_FORM_ID__IDEA
            )
        ); ?>
    </div>
    <div id="popup-form-honor" class="popup-form">
        <? $APPLICATION->IncludeComponent(
            "bitrix:form",
            "popup",
            Array(
                "AJAX_MODE" => "Y",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",

                "CACHE_TIME" => "3600",
                "CACHE_TYPE" => "A",
                "CHAIN_ITEM_LINK" => "",
                "CHAIN_ITEM_TEXT" => "",
                "EDIT_ADDITIONAL" => "N",
                "EDIT_STATUS" => "N",
                "IGNORE_CUSTOM_TEMPLATE" => "N",
                "NOT_SHOW_FILTER" => array("", ""),
                "NOT_SHOW_TABLE" => array("", ""),
                "RESULT_ID" => "",
                "SEF_MODE" => "N",
                "SHOW_ADDITIONAL" => "N",
                "SHOW_ANSWER_VALUE" => "N",
                "SHOW_EDIT_PAGE" => "N",
                "SHOW_LIST_PAGE" => "N",
                "SHOW_STATUS" => "N",
                "SHOW_VIEW_PAGE" => "N",
                "START_PAGE" => "new",
                "SUCCESS_URL" => "",
                "USE_EXTENDED_ERRORS" => "N",
                "VARIABLE_ALIASES" => Array("action" => "action"),
                "WEB_FORM_ID" => WEB_FORM_ID__HONOR
            )
        ); ?>
    </div>
  <?  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/baron.min.js");?>
  <?  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/script.js");?>
<? } else { ?>
    </div> <? /* /.login-page__form-wrap  */ ?>
    <div class="login-page__note">
        <p>Чтобы получить доступ к порталу ЮУрГУ
        <p>
        <p>1. Подготовьте служебную записку на имя начальника Управления информатизации Раенко В. Г., указав подразделение, Ф.И.О.пользователя, его
            должность, список модулей системы «Универис», а также перечислите используемый функционал модулей или уровень доступа, например,
            «просмотр»
            или «ввод и редактирование».</p>
        <p> 2.​ Передайте подготовленный документ на подпись начальнику Управления информатизации Раенко В. Г. – аудитория 113/2. Дополнительно
            получив согласование типовых рабочих мест согласно перечню.</p>
        <p> 3.​ Подписанные документы предоставьте на Вычислительный центр ЮУрГУ в ауд. 244/3Б.</p>
    </div>
    </div><? /* /.col... */ ?>
    </div><? /* /.row */ ?>
    </div><? /* /.container  */ ?>
    </div><? /* /.login-page  */ ?>
<? } ?>
</body>
</html>