$(document).ready(function () {

    baron({
        position: "fixed",
        cssGuru: true,
        resizeDebounce: 0,
        root: '.content',
        scroller: '.baron__scroller',
        bar: '.baron__bar',
        scrollingCls: '_scrolling',
        draggingCls: '_dragging',
    });

    var IMEl = $('#bx-notifier-panel');
// при загрузке проверяем медиа, если меньше то перекидываем в левую панель
    if (window.matchMedia("(max-width: 1024px)").matches) {
        IMEl.detach().appendTo('#im-left-wrap');
        $('.main-wrapper').addClass('short-menu');
    }
    if (window.matchMedia("(max-width: 1400px)").matches) {
        $('.main-wrapper').addClass('short-menu');
    }
// при смене ширины экрана проверяем медиа, если меньше то перекидываем в левую панель иначе в правую
    $(window).resize(function () {
        if (window.matchMedia("(min-width: 1024px)").matches) {
            IMEl.detach().appendTo('#im-right-wrap');
        } else {
            IMEl.detach().appendTo('#im-left-wrap');
        }
    });
    $(window).resize(function () {
        if (window.matchMedia("(min-width: 1400px)").matches) {
            $('.main-wrapper').removeClass('short-menu');
        } else {
            $('.main-wrapper').addClass('short-menu');
        }
    });

    $('.toggle-left-menu').on('click', function () {
        $('.main-wrapper').toggleClass('short-menu')

    });
    $(function () {
        $(".left-panel").swipe({
            swipeRight: function (event, direction, distance, duration, fingerCount) {
                $('.main-wrapper').removeClass('short-menu');
            },
            threshold: 50
        });
    });
    $(function () {
        $(".left-panel").swipe({
            swipeLeft: function (event, direction, distance, duration, fingerCount) {
                $('.main-wrapper').addClass('short-menu');
            },
            threshold: 50
        });
    });
    $('.toggle-footer__button').on('click', function () {
        /*$('.footer').animate(
            {height: "toggle"}, 300, function() {*/
        $('.footer').toggleClass('footer_showed');
        /*       }
           );*/
    });

    $('.page-action__toggle').on('click', function () {
        /*$('.footer').animate(
            {height: "toggle"}, 300, function() {*/
        $('.page-header').toggleClass('page-header_active');
        $('.page-action__content').toggleClass('page-action__content_active');
        $('.page-action__icon').toggleClass('close-icon');

        /*       }
           );*/
    });
    $('#iframe-map').on('click', function () {
        $(this).removeClass('pointer-event-stopped');
    });

  /*  $(function () {
        $(".accordion-js").accordion({
            header: ".accordion-js__header",
            heightStyle: "content",
            collapsible: true
        });
    });*/
});


    Object.filter = function (obj, predicate) {
        var result = {};
        for (var key in obj) {
            if (obj.hasOwnProperty(key) && !predicate(obj[key])) {
                result[key] = obj[key];
            }
        }
        return result;
    };

    BX.addCustomEvent('onImInit', function (data) {
        showWidgetOnImInit(data, 'messages');

        BX.ajax({
            url: data.pathToAjax + '?NOTIFY_LOAD&V=' + data.revision,
            method: 'POST',
            dataType: 'json',
            lsId: 'IM_NOTIFY_LOAD',
            lsTimeout: 5,
            timeout: 30,
            data: {'IM_NOTIFY_LOAD': 'Y', 'IM_AUTO_READ': 'N', 'IM_AJAX_CALL': 'Y', 'sessid': BX.bitrix_sessid()},
            onsuccess: BX.delegate(function (data) {

                var countLastEl = 2, unreadIds = [], lastMess = {}, unread = data.UNREAD_NOTIFY;

                // all ids of messages
                unreadIds = Object.keys(unread);

                // sort desc
                unreadIds.sort(function (a, b) {
                    return b - a
                });

                if (!$.isEmptyObject(unread) && Object.keys(unread).length > 1) {
                    for (var i = 0; i < countLastEl; i++) {
                        lastMess[i] = data.NOTIFY[unreadIds[i]];
                    }
                } else {
                    lastMess = data.NOTIFY;
                }
                var parentEl = $('.widget_notify .widget-items');
                for (var nId in lastMess) {
                    var avatarFrom = {
                            'name': lastMess[nId].userName,
                            'avatar': false,
                            'color': lastMess[nId].userColor
                        },
                        message = {
                            'id': lastMess[nId].id,
                            'date': lastMess[nId].date,
                            'text': lastMess[nId].text
                        };

                    addWidgetItem(parentEl, message, avatarFrom, false, false);
                }
            }, this),
            onfailure: BX.delegate(function () {
                console.log('notify load failure');
            }, this)
        });

    }, this);


    /**onImUpdateSumCounters
     *
     * @param data
     * @param type
     */
    function showWidgetOnImInit(data, type) {

        // чистим старые
        var parentEl = $('.widget_' + type + ' .widget-items');
        parentEl.find('.widget-item').remove();

        var messageO = data.messenger.message,
            countLastEl = 2, // кол-во последних
            lastMess = {}, unreadIds = [], unread = data.messenger.unreadMessage;

        if (!$.isEmptyObject(unread)) {

            // all ids of messages
            for (var chatsId in unread)
                for (var chatId in unread[chatsId]) {
                    unreadIds.push(parseInt(unread[chatsId][chatId]));
                }

            // sort desc
            unreadIds.sort(function (a, b) {
                return b - a
            });

            // last messages
            if (!$.isEmptyObject(messageO) && Object.keys(messageO).length > 1) {
                for (var i = 0; i < countLastEl; i++) {
                    lastMess[i] = messageO[unreadIds[i]];
                }
            } else {
                lastMess = messageO;
            }

            // add new
            for (var mId in lastMess) {
                var message = lastMess[mId];
                var str = message.recipientId, avatarFrom = {}, chatFrom = '';

                if (str.search(/chat/i) + 1) {
                    avatarFrom = data.messenger.chat[str.replace('chat', '')];
                    chatFrom = '<i>' + data.messenger.users[message.senderId].name + ':</i> ';
                } else {
                    avatarFrom = data.messenger.users[message.senderId];
                }

                addWidgetItem(parentEl, message, avatarFrom, chatFrom);

            }

        }
    }

    BX.addCustomEvent('onPopupShow', BX.delegate(function (popup) {
        if (popup.uniquePopupId == 'bx-messenger-popup-messenger') {
            console.log('IM popup open');
            $('#bx-messenger-popup-messenger').prepend('<div class="toggle-im-contacts toggle-im-contacts_hide"></div>');

            $('.toggle-im-contacts').on('click', function () {
                if ($(this).hasClass('toggle-im-contacts_show')) {
                    $('.bx-messenger-box-contact').css('display', 'none');
                } else {
                    $('.bx-messenger-box-contact').css('display', 'block');
                }

                $(this).toggleClass('toggle-im-contacts_show');
                $(this).toggleClass('toggle-im-contacts_hide');
            });
        }
    }));

    BX.addCustomEvent("onPullEvent-im", BX.delegate(function (command, params) {

        // console.log('Events of moduleName', command, params);

        var parent = $('.widget_messages .widget-items'),
            el = parent.find('.widget-item'),
            user = {},
            chatFrom = false,
            message = params.message;

        switch (command) {
            case 'message':
                // only new
                if ((typeof el.first().data('messageid') == 'undefined' || params.message.id > el.first().data('messageid'))
                    && (typeof BXIM !== 'undefined' && params.message.senderId != BXIM.userId)) {
                    if (el.length > 1) {
                        el.last().remove();
                    }
                    user = params.users[params.message.senderId];
                } else return false;
                break;
            case 'messageChat':
                // only new
                if ((typeof el.first().data('messageid') == 'undefined' || params.message.id > el.first().data('messageid'))
                    && (typeof BXIM !== 'undefined' && params.message.senderId != BXIM.userId)) {
                    if (el.length > 1) {
                        el.last().remove();
                    }
                    user = params.chat[params.message.chatId];
                    chatFrom = '<i>' + params.users[params.message.senderId].name + ':</i> ';
                } else return false;
                break;
            case 'notify':
                parent = $('.widget_notify .widget-items');
                el = parent.find('.widget-item');
                // only new
                if (typeof el.first().data('messageid') == 'undefined' || params.id > el.first().data('messageid')) {
                    if (el.length > 1) {
                        el.last().remove();
                    }
                    user = {
                        'color': params.userColor,
                        'name': params.userName,
                        'avatar': false
                    };
                    message = params;
                    parent.prev(".bx-notifier-indicator").find('.bx-notifier-indicator-count').html(params.counter);
                } else {
                    return false;
                }
                break;
            case 'deleteNotifies':
                $('.widget_notify .widget-items').find('.widget-item[data-messageid=' + Object.keys(params.id) + ']').remove();
                return false;

            case 'readMessage':
            case 'readMessageChat':
                $('.widget_messages .widget-items .widget-item').remove();
                return false;
            case 'readNotifyList':
                for (var id in params.list) {
                    $('.widget_notify .widget-items').find('.widget-item[data-messageid=' + params.list[id] + ']').remove();
                }
                return false;

            default:
                return false;
        }

        addWidgetItem(parent, message, user, chatFrom, true);

    }, this));


    /**
     * add on page widget item
     * @param parentEl
     * @param message
     * @param avatarFrom
     * @param chatFrom
     * @param before
     */
    function addWidgetItem(parentEl, message, avatarFrom, chatFrom, before) {
        if (typeof message == 'undefined' || $.isEmptyObject(message)) return false;

        chatFrom = chatFrom || '';
        before = before || false;
        var img = avatarFrom.avatar ? '<div class="widget-item__img" style="background-color:' + avatarFrom.color + '"><img src="' + avatarFrom.avatar + '" alt="" ></div>' : '';
        var html = '<div class="widget-item" data-messageid="' + message.id + '">' + img +
            '            <div class="widget-item__title">' + avatarFrom.name + '</div>' +
            '            <div class="widget-item__date">' + BX.date.format("H:i, d F", message.date, Date.now(), false) + '</div>' +
            '            <p class="widget-item__text">' + chatFrom + message.text + '</p>' +
            '        </div>';


        if (before) parentEl.prepend(html);
        else parentEl.append(html);

        //вернем клики по тексту
        parentEl.find('a').bind("click", function () {
            window.location = $(this).attr('href');
        });

    }



BX.ready(function () {

    var addIdea = BX.PopupWindowManager.create("my_idea", null, {
        content: BX('popup-form-idea'),
        closeIcon: {right: "10px", top: "10px"},
        zIndex: 0,
        offsetLeft: 0,
        offsetTop: 1,
        overlay: true,
        draggable: {restrict: false},
    });
    $('#popup-idea-link').click(function () {
        addIdea.show();
    });
    var addHonor = BX.PopupWindowManager.create("my_honor", null, {
        content: BX('popup-form-honor'),
        closeIcon: {right: "10px", top: "10px"},
        zIndex: 0,
        offsetLeft: 0,
        offsetTop: 1,
        overlay: true,
        draggable: {restrict: false},
    });
    $('#popup-honor-link').click(function () {
        addHonor.show();
    });
    $('.popup-window-overlay').click(function () {
        BX.PopupWindowManager.getCurrentPopup().close();
    });

    /*  ﻿﻿BX.PopupWindowManager.onPopupWindowIsInitialized( 'bx-messenger-popup-messenger',function (t) {
          console.log(t);
      });*/
});