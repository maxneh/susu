<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!--TPL: <?= $this->GetFolder() ?>-->
<script>
    var currentFilter = '<?echo CUtil::JSEscape($arResult["CURRENT_FILTER"])?>';
    var arFilters = ['simple', 'adv'];

    function BXSetFilter(currentFilterNew) {
        if (currentFilter == currentFilterNew)
            return;

        for (var i = 0; i < arFilters.length; i++) {
            var obTabContent = document.getElementById('bx_users_filter_' + arFilters[i]);
            var obTab = document.getElementById('bx_users_selector_tab_' + arFilters[i]);

            if (null != obTabContent) {
                obTabContent.style.display = (currentFilterNew == arFilters[i] ? 'block' : 'none');
                currentFilter = currentFilterNew;
            }

            if (null != obTab) {
                obTab.className = (currentFilterNew == arFilters[i] ? 'bx-selected' : '');
            }
        }
    }
</script>

<div class="bx-users-selector-filter" id="bx_users_filter_simple"<?= $arResult["CURRENT_FILTER"] == 'adv' ? ' style="display: none;"' : '' ?>>
    <a href="#" class="reset-filter pull-right" data-action="del_filter"><span>Сбросить фильтр</span></a>
    <div class="clearfix"></div>
    <form action="<?= $arResult["Urls"]["UserSearch"] ?>" class="bx-selector-form user-filter-form" name="bx_users_filter_simple_form"
          id="bx_users_filter_simple_form" method="post">
        <?
        foreach ($arResult["Params"]["UserSearch"] as $key => $value)
            echo "<input type=\"hidden\" name=\"" . $key . "\" value=\"" . $value . "\" />";
        ?>
        <input type="hidden" name="current_view" value="<?= htmlspecialcharsbx($arResult['CURRENT_VIEW']) ?>"/>
        <input type="hidden" name="current_filter" value="simple"/>
        <div class="row">
            <? foreach ($arResult["UserFieldsSearchSimple"] as $userFieldName => $userFieldDescr): ?>
                <div class="filter__row col-xs-12 col-sm-6 col-md-4">

                    <div>
                        <label><?= $userFieldDescr["TITLE"] ?></label><?
                        if ($userFieldDescr["TYPE"] == "exact"):
                            echo $userFieldDescr["STRING"];
                        elseif ($userFieldDescr["TYPE"] == "select"):
                            ?><select  class="filter-control" name="<?= $userFieldDescr["NAME"] ?>">
                            <option value=""></option>
                            <?
                            foreach ($userFieldDescr["VALUES"] as $keyTmp => $valTmp):?>
                                <option value="<?= $keyTmp ?>"<?= (($keyTmp == $userFieldDescr["VALUE"]) ? " selected" : "") ?>><?= $valTmp ?></option>
                            <? endforeach; ?>
                            </select><?
                        elseif ($userFieldDescr["TYPE"] == "string"):
                            ?><input class="filter-control" type="text" name="<?= $userFieldDescr["NAME"] ?>" value="<?= $userFieldDescr["VALUE"]
                        ?>" /><?

                        endif;
                        ?></div>
                </div>
            <? endforeach; ?>
            <? /* foreach ($arResult["UserPropertiesSearchSimple"] as $userFieldName => $userFieldDescr): ?>
                    <div class="filter__row col-xs-12 col-sm-6 col-md-3">
                        <div><label><?= $userFieldDescr["EDIT_FORM_LABEL"] ?></label>
                            <?
                            $APPLICATION->IncludeComponent(
                                'bitrix:system.field.edit',
                                $userFieldDescr['USER_TYPE_ID'],
                                array(
                                    "arUserField" => $userFieldDescr,
                                    'form_name' => 'bx_users_filter_simple_form',
                                    "bVarsFromForm" => true,
                                    "arUserField"
                                ),
                                null,
                                array('HIDE_ICONS' => 'Y')
                            );
                            ?></div>
                    </div>
                <? endforeach; */ ?>

        </div>
        <div class="row">
            <?
            $uniqId = (time() + rand(0, 1000));
            CModule::IncludeModule("iblock");
            CModule::IncludeModule("form");

            $QUESTION_SID = "department_1";
            $arDropDown = array(
                "reference" => array('Не выбрано'),
                "reference_id" => array('')

            );
            $arSelect = Array("ID", "NAME", "DEPTH_LEVEL");
            $arFilter = Array("IBLOCK_ID" => 15, 'DEPTH_LEVEL' => '1', "ACTIVE" => "Y");

            $rsSect = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter, false, $arSelect);
            while ($arSect = $rsSect->GetNext()) {
                $arDropDown["reference"][] = $arSect["NAME"];
                $arDropDown["reference_id"][] = $arSect["ID"];
            }

            $value = CForm::GetDropDownValue($QUESTION_SID, $arDropDown); ?>
            <div class="filter__row col-xs-12">
                <label>Подразделение</label>
                <input id="dep_filter" type="hidden" name="flt_uf_user_department" value="">
            </div>
            <div class="filter__row col-xs-12 col-sm-6 col-md-4">
                    <?= CForm::GetDropDownField(
                        $QUESTION_SID,
                        $arDropDown,
                        $value,
                        "class=\"inputselect w100\" onchange=\"loadHTML('/local/ajax/departments.php?uniqId=$uniqId&id=' + this . options[this . selectedIndex] . value, 'd_level_2_$uniqId');\""
                    ); ?>
            </div>
            <div id="d_level_2_<?= $uniqId ?>" class="filter__row col-xs-12 col-sm-6 col-md-4"></div>
            <div id="d_level_3_<?= $uniqId ?>" class="filter__row col-xs-12 col-sm-6 col-md-4"></div>

        </div>
        <br>
        <div class="row">
            <div class="col-xs-12">
                <input type="submit" name="set_filter" value="<? echo GetMessage('SONET_C241_T_DO_SEARCH') ?>" class="hide"/>
                <!--      <input type="reset" name="del_filter" value="<? /* echo GetMessage('SONET_C241_T_DO_CANCEL') */ ?>" class="hide"
                           onclick="window.location='<? /*= $arResult["Urls"]["UserSearch"] */ ?>'"/>-->
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function () {
        $('.reset-filter').on('click', function (e) {
            e.preventDefault();
            var form = $('#bx_users_filter_simple_form');
            form[0].reset();
            form.find('.filter-control').each(function () {
                $(this).val('');
            });

            form.find("#dep_filter").val('');
            var data = BX.ajax.prepareData(BX.ajax.prepareForm(BX('bx_users_filter_simple_form')).data);
            filterAjaxSend(data);
            return false;
        });

        $('.filter-control').each(function () {
            $(this).on('change', function (event) {
                var data = BX.ajax.prepareData(BX.ajax.prepareForm(BX('bx_users_filter_simple_form')).data);
                filterAjaxSend(data);
            });
        });

        $("body").on('change', '#bx_users_filter_simple_form .inputselect', function (event) {
            $("#dep_filter").val($(this).val());
            var data = BX.ajax.prepareData(BX.ajax.prepareForm(BX('bx_users_filter_simple_form')).data);
            filterAjaxSend(data);
        });


        function filterAjaxSend(data) {
            BX.ajax({
                url: '/spravochnik/kontakty-sotrudnikov/ajax.php',
                data: data,
                method: 'POST',
                // dataType: 'raw',
                timeout: 30,
                async: true,
                processData: true,
                scriptsRunFirst: true,
                emulateOnload: true,
                start: true,
                cache: false,
                onsuccess: function (data) {
                    $(".users-list").html(data);
                },
                onfailure: function () {

                }
            });
        }
    });

</script>

<div class="users-list">
    <? if (strlen($arResult["ERROR_MESSAGE"]) <= 0): ?>
        <? if (count($arResult["SEARCH_RESULT"]) > 0): ?>
            <div class="row">
                <? foreach ($arResult["SEARCH_RESULT"] as $v): ?>
                    <div class="user-list__item col-xs-12 col-md-4">
                        <div class="row">
                            <div class="user-list__image col-3"> <?= $v["IMAGE_IMG"] ?></div>
                            <div class="user-list__properties col-9">
                                <p class="small-text"> <?= $v['WORK_POSITION'] ?></p>
                                <p class=""><a href="<?= $v["URL"] ?>"><span><?= $v["NAME"] ?> <?= $v["SECOND_NAME"] ?> <?= $v["LAST_NAME"] ?></span></a>
                                </p>
                                <p class="small-text"> <?= $v['WORK_PROFILE'] ?></p>
                                <p>  <?= $v['WORK_PHONE'] ?></p>
                                <p>  <?= $v['PERSONAL_PHONE'] ?></p>
                                <p>  <?= $v['EMAIL'] ?></p>
                                <? if ($v["UserPropertiesMain"]["SHOW"] == "Y"): ?>
                                    <? foreach ($v["UserPropertiesMain"]["DATA"] as $fieldName => $arUserField): ?>
                                        <? if ((is_array($arUserField["VALUE"]) && count($arUserField["VALUE"]) > 0) || (!is_array($arUserField["VALUE"]) && StrLen($arUserField["VALUE"]) > 0)): ?>
                                            <? //= $arUserField["EDIT_FORM_LABEL"] ?>
                                            <?
                                            if ($arUserField["FIELD_NAME"] == "UF_DEPARTMENT")
                                                $arUserField["SETTINGS"]["SECTION_URL"] = $arParams["PATH_TO_CONPANY_DEPARTMENT"];
                                            $APPLICATION->IncludeComponent(
                                                "bitrix:system.field.view",
                                                $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                                array("arUserField" => $arUserField),
                                                null,
                                                array("HIDE_ICONS" => "Y")
                                            );
                                            ?>
                                            <br/>
                                        <? endif; ?>
                                    <? endforeach; ?>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
            <? if (strlen($arResult["NAV_STRING"]) > 0): ?>
                <p><?= $arResult["NAV_STRING"] ?></p>
            <? endif; ?>

        <? else: ?>
            <? if (!$arResult["ShowResults"]): ?>
                <?= GetMessage("SONET_C241_T_NOT_FILTERED") ?>
            <? else: ?>
                <?= GetMessage("SONET_C241_T_NOT_FOUND") ?>
            <? endif; ?>
        <? endif; ?>
    <? else: ?>
        <?= ShowError($arResult["ERROR_MESSAGE"]); ?>
    <? endif; ?>
</div>
