<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if ($arParams["ALLOW_CREATE_GROUP"] == "Y") {
    ?>
    <div class="sonet-add-button">
        <a class="btn soc-add-btn" href="<?= $arParams["~HREF"] ?>"
           title="<?= GetMessage("SONET_C36_T_CREATE") ?>"><?= GetMessage("SONET_C36_T_CREATE") ?></a>
    </div>
    <? } ?>