<?php
/**
 * Created by PhpStorm.
 * User: MaxNeh
 * Date: 30.01.2018
 */

CModule::IncludeModule("iblock");
/*var_dump($arResult['SECTION']['USER_FIELDS']['UF_GALLERY_CAT']['SETTINGS']['HLBLOCK_ID']);
var_dump($arResult['SECTION']['USER_FIELDS']['UF_GALLERY_CAT']['VALUE']);*/

$hlID = $arResult['SECTION']['USER_FIELDS']['UF_GALLERY_CAT']['SETTINGS']['HLBLOCK_ID'];
$sID = $arResult['SECTION']['USER_FIELDS']['UF_GALLERY_CAT']['VALUE'];
if ($hlID && $sID) {
    $category = \Local\Lib\Content\Highload::getByID($hlID, $sID);
    $arResult['SECTION']['UF_CAT_ICON'] = $category['UF_CAT_ICON'];
    $arResult['SECTION']['CATEGORY_NAME'] = $category['UF_GALLERY_CAT'];
}
$rootSectionID = $arResult['GALLERY']['ID'] != null ? $arResult['GALLERY']['ID'] : false;
// выбрать нужно id раздела, его имя и ссылку. Можно добавить любые другие поля
$arSelect = array(
    "ID",
    "NAME",
    "SECTION_PAGE_URL",
    "DETAIL_PAGE_URL",
    "UF_DATE",
    'CREATED_BY'
);

$arFilter = array(
    '!ID' => $arResult["SECTION"]["ID"],
//    'CREATED_BY' => $arResult['SECTION']['CREATED_BY'],
    'IBLOCK_ID' => $arParams["IBLOCK_ID"],
    'SECTION_ID' => $rootSectionID,
    "ACTIVE" => "Y",
    "CHECK_PERMISSIONS" => "Y",
//    '>UF_DATE' => $arResult["SECTION"]["USER_FIELDS"]['UF_DATE']["VALUE"]
    '>ID' => $arResult["SECTION"]["ID"]

);

//$rsSect = CIBlockSection::GetList(array('UF_DATE' => 'ASC'), $arFilter, false, $arSelect);
$rsSect = CIBlockSection::GetList(array('DATE_CREATE' => 'ASC'), $arFilter, false, $arSelect);

$rsSect->SetUrlTemplates($arParams["SECTION_URL"]);
$rsSect->NavStart(1);
while ($arSect = $rsSect->GetNext()) {
    $arResult["TOLEFT"] = $arSect;
    if ($rootSectionID) {
        CPGalleryInterface::HandleUserAliases(array($arSect['CREATED_BY']), $arParams["IBLOCK_ID"]);
        $arResult["TOLEFT"]['DETAIL_PAGE_URL'] = CPGalleryInterface::GetPathWithUserAlias($arSect["DETAIL_PAGE_URL"], $arSect['CREATED_BY'], $arParams["IBLOCK_ID"]);
    }
}

$arFilter = array(
    '!ID' => $arResult["SECTION"]["ID"],
//    'CREATED_BY' => $arResult['SECTION']['CREATED_BY'],
    'IBLOCK_ID' => $arParams["IBLOCK_ID"],
    'SECTION_ID' => $rootSectionID,
//    '<UF_DATE' => $arResult["SECTION"]["USER_FIELDS"]['UF_DATE']["VALUE"],
    '<ID' => $arResult["SECTION"]["ID"],
    "ACTIVE" => "Y",
    "CHECK_PERMISSIONS" => "Y");
//$rsSect = CIBlockSection::GetList(array('UF_DATE' => 'DESC'), $arFilter, false, $arSelect);
$rsSect = CIBlockSection::GetList(array('ID' => 'DESC'), $arFilter, false, $arSelect);
$rsSect->SetUrlTemplates($arParams["SECTION_URL"]);
$rsSect->NavStart(1);
while ($arSect = $rsSect->GetNext()) {
    $arResult["TORIGHT"] = $arSect;
    if ($rootSectionID) {
        CPGalleryInterface::HandleUserAliases(array($arSect['CREATED_BY']), $arParams["IBLOCK_ID"]);
        $arResult["TORIGHT"]['DETAIL_PAGE_URL'] = CPGalleryInterface::GetPathWithUserAlias($arSect["DETAIL_PAGE_URL"], $arSect['CREATED_BY'], $arParams["IBLOCK_ID"]);
    }
}

// в $arResult["TORIGHT"] и $arResult["TOLEFT"] лежат массивы с информацией о соседних разделах
?>