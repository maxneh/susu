<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!--TPL: <?= $this->GetFolder() ?>-->
<?
if (!$this->__component->__parent || strpos($this->__component->__parent->__name, "photogallery") === false) {
    $GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/photogallery/templates/.default/style.css');
    $GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/photogallery/templates/.default/themes/gray/style.css');
}

$GLOBALS['APPLICATION']->AddHeadScript("/bitrix/components/bitrix/photogallery.section.list/templates/.default/script.js");

$res = $arResult["SECTION"];
?>


<? $APPLICATION->IncludeComponent('susu:list.detail.header', '',
    array(
        'IBLOCK_TYPE' => 'gallery',
        'TITLE' => $res['NAME'],
        'DATE' => '',
        'SECTION_URL' => '',
        "EDIT_LINK" => $arParams["PERMISSION"] >= "U" ? $arResult["SECTION"]["EDIT_LINK"] : '',
        'SECTION_ID' => $res['USER_FIELDS']['UF_GALLERY_CAT']['VALUE'],
        'SECTION_ICON' => $res['UF_CAT_ICON'],
        'SECTION_NAME' => $res['CATEGORY_NAME'],
        'TOLEFT_URL' => $arResult["TOLEFT"]['DETAIL_PAGE_URL'],
        'TORIGHT_URL' => $arResult["TORIGHT"]['DETAIL_PAGE_URL'],
        'LIKE' => array(
            "IBLOCK_ID" => $res["IBLOCK_ID"],
            "IBLOCK_TYPE" => $res["IBLOCK_TYPE"],
            "ELEMENT_ID" => $res["ID"],
            "ENTITY_TYPE_ID" => "IBLOCK_SECTION",
            "ENTITY_ID" => $res["ID"],
            "PATH_TO_USER_PROFILE" => ""),
        'SHAREIT' => Array(
            'LINK_TITLE'=>$res["NAME"],
            'LINK_URL' =>$APPLICATION->GetCurUri()
        ),
        'FAVOR'=>Array(
//            "AJAX_MODE" => "Y",
            "ENTITY_TYPE_ID" => "PHOTOALBUM",
            "ENTITY_ID" => $res["ID"],
            'LINK_TITLE'=>$res["NAME"],
            'LINK_URL' =>$APPLICATION->GetCurUri()
        ),
        'SHOW_COMMENTS' => 'N',
        'COMMENTS_CNT' => false
    ),
    $component
) ?>


<div class="photo-album-item photo-album-<?= ($res["ACTIVE"] != "Y" ? "nonactive" : "active") ?> <?= (
!empty($res["PASSWORD"]) ? "photo-album-password" : "") ?>" id="photo_album_info_<?= $res["ID"] ?>" <?
if ($res["ACTIVE"] != "Y" || !empty($res["PASSWORD"])) {
    $sTitle = GetMessage("P_ALBUM_IS_NOT_ACTIVE");
    if ($res["ACTIVE"] != "Y" && !empty($res["PASSWORD"]))
        $sTitle = GetMessage("P_ALBUM_IS_NOT_ACTIVE_AND_PASSWORDED");
    elseif (!empty($res["PASSWORD"]))
        $sTitle = GetMessage("P_ALBUM_IS_PASSWORDED");
    ?> title="<?= $sTitle ?>" <?
}
?>>
    <? if (0 > 1) { ?>
        <? if ($arParams["PERMISSION"] >= "U"): ?>
            <noindex>
                <div class="photo-top-controls">

                    <a rel="nofollow" class="photo-control-album-edit"
                       href="<?= $arResult["SECTION"]["EDIT_LINK"] ?>"><?= GetMessage("P_SECTION_EDIT") ?></a>
                    <a rel="nofollow" href="<?= $arResult["SECTION"]["NEW_LINK"] ?>"
                       onclick="EditAlbum('<?= CUtil::JSEscape($arResult["SECTION"]["~NEW_LINK"]) ?>'); return false;"><?= GetMessage("P_ADD_ALBUM") ?></a>
                    <a rel="nofollow" href="<?= $arResult["SECTION"]["UPLOAD_LINK"] ?>" target="_self"><?= GetMessage("P_UPLOAD") ?></a>

                </div>
            </noindex>
        <? endif; ?>
    <? } ?>
    <div class="empty-clear"></div>