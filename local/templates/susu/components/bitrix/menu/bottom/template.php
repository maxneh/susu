<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

    <button class="toggle-footer__button"><i class="toggle-footer__button-icon"></i> Весь сайт</button>

<? if (!empty($arResult)) { ?>
    <ul class="bottom-menu">

    <?
    $previousLevel = 0;
foreach ($arResult

    as $arItem) { ?>

    <? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) { ?>
        <?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
    <? } ?>

    <? if ($arItem["IS_PARENT"]){ ?>

    <? if ($arItem["DEPTH_LEVEL"] == 1){ ?>
    <li class="bottom-menu__root"><a href="<?= $arItem["LINK"] ?>"
                                     class="<? if ($arItem["SELECTED"]) { ?>bottom-menu__root_selected<? } ?>">
        <span><?= $arItem["TEXT"] ?></span>
    </a>
    <ul class="bottom-submenu">
    <? } else { ?>
    <li<? if ($arItem["SELECTED"]) { ?> class="item-selected"<? } ?>><a href="<?= $arItem["LINK"] ?>" class="parent">
        <span><?= $arItem["TEXT"] ?></span>
    </a>
    <ul class="bottom-submenu">
    <? } ?>
    <? } else { ?>
        <? if ($arItem["PERMISSION"] > "D") { ?>
            <? if ($arItem["DEPTH_LEVEL"] == 1) { ?>
                <li class="bottom-menu__root">
                    <a href="<?= $arItem["LINK"] ?>" class="<? if ($arItem["SELECTED"]) { ?>root-item-selected<? } else { ?>root-item<? } ?>">
                        <span><?= $arItem["TEXT"] ?></span>
                    </a>
                </li>
            <? } else { ?>
                <li class="bottom-menu__item<? if ($arItem["SELECTED"]) { ?> bottom-menu__item_selected<? } ?>">
                    <a href="<?= $arItem["LINK"] ?>">
                        <span><?= $arItem["TEXT"] ?></span>
                    </a>
                </li>
            <? } ?>
        <? } else { ?>
            <? if ($arItem["DEPTH_LEVEL"] == 1) { ?>
                <li class="bottom-menu__root">
                    <a href="" class="<? if ($arItem["SELECTED"]) { ?>root-item-selected<? } else { ?>root-item<? } ?>"
                       title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>">
                        <span><?= $arItem["TEXT"] ?></span>
                    </a></li>
            <? } else { ?>
                <li class="bottom-menu__item bottom-menu__item_denied">
                    <a href="" class="" title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>">
                        <span><?= $arItem["TEXT"] ?></span>
                    </a>
                </li>
            <? } ?>
        <? } ?>
    <? } ?>
    <? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>
<? } ?>
    <? if ($previousLevel > 1) {//close last item tags?>
        <?= str_repeat("</ul></li>", ($previousLevel - 1)); ?>
    <? } ?>
    </ul>
    <div class="menu-clear-left"></div>
<? } ?>