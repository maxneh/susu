<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div class="left-menu">

        <? foreach ($arResult as $arItem): ?>
        <? $board = preg_match('/(^\/board\/)/', $APPLICATION->GetCurUri())?>
        <? $pers = preg_match('/(^\/personal\/)/', $APPLICATION->GetCurUri());?>
        <? $kal = preg_match('/(^\/kalendar\/)/', $APPLICATION->GetCurUri());?>
            <? if ($arItem["SELECTED"] && !($board || $pers || $kal)): ?>
                <div class="left-menu__item">
                    <a href="<?= $arItem["LINK"] ?>" class="selected">
                        <? if (isset($arItem["PARAMS"]['ICON'])) { ?><i class="left-menu__icon icon icon-<?= $arItem["PARAMS"]['ICON'] ?>"></i> <?
                        } ?>
                        <span><?= $arItem["TEXT"] ?></span></a>
                </div>
            <? else: ?>
                <div class="left-menu__item">
                    <a href="<?= $arItem["LINK"] ?>">
                        <? if (isset($arItem["PARAMS"]['ICON'])) { ?><i class="left-menu__icon icon icon-<?= $arItem["PARAMS"]['ICON']
                        ?>"></i> <?  } ?>
                        <span><?= $arItem["TEXT"]?></span></a>
                </div>
            <? endif ?>

        <? endforeach ?>

    </div>
<? endif ?>