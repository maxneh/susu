<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<!--TPL: <?= $this->GetFolder()?>-->
<div id="bx-notifier-panel" class="bx-notifier-panel">
    <div class="widget widget_messages">
        <a href="javascript:void(0)" class="bx-notifier-indicator bx-notifier-message widget__link"
           title="<?= GetMessage('IM_MESSENGER_OPEN_MESSENGER_2'); ?>">
            <h4 class="widget__title"><i class="widget__icon icon icon-messages"></i><span>Сообщения</span>
                <i class="badge bx-notifier-indicator-count"></i>
            </h4>
        </a>
        <div class="widget-items"></div>
    </div>
    <div class="widget widget_notify">
        <a href="javascript:void(0)" class="bx-notifier-indicator bx-notifier-notify widget__link"
           title="<?= GetMessage('IM_MESSENGER_OPEN_NOTIFY'); ?>">
            <h4 class="widget__title"><i class="widget__icon icon icon-notify"></i><span>Уведомления</span>
                <i class="badge bx-notifier-indicator-count"></i>
            </h4>
        </a>
        <div class="widget-items"></div>
    </div>
</div>


<script type="text/javascript">
    <?=CIMMessenger::GetTemplateJS(Array(), $arResult)?>
</script>