<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/********************************************************************
 * Input params
 ********************************************************************/
$arParams["THUMBNAIL_SIZE"] = ($arParams["THUMBNAIL_SIZE"] > 0 ? $arParams["THUMBNAIL_SIZE"] : 200);

$temp = array("STRING" => preg_replace("/[^0-9]/is", "/", $arParams["THUMBNAIL_SIZE"]));
list($temp["WIDTH"], $temp["HEIGHT"]) = explode("/", $temp["STRING"]);
/*$width = (intVal($temp["WIDTH"]) > 0 ? intVal($temp["WIDTH"]) : 340);
$height = (intVal($temp["HEIGHT"]) > 0 ? intVal($temp["HEIGHT"]) : 190);*/
$width = 340;
$height = 190;


/********************************************************************
 * /Input params
 ********************************************************************/
?>

<!--TPL: <?= $this->GetFolder()?>-->

    <span class="category-icon icon icon-<?=$arResult['SECTION']['UF_CAT_ICON'] ?>" title="<?= $arResult['SECTION']['NAME'] ?>"></span>
    <div class="preview-album">
        <? foreach ($arResult['ELEMENTS_LIST'] as $item) {
            $image = $item['PICTURE'];
            if (!empty($image)) {
                $tmpImage = CFile::ResizeImageGet($image, array("width" => $width, "height" => $height), BX_RESIZE_IMAGE_EXACT, false);
                $image['src'] = $tmpImage['src'];
            }
            print '<img src="' . $image['src'] . '" alt="' . $arElement['NAME'] . '" title="' . $arElement['NAME'] . '" border="0" class="img-responsive"/>';
        } ?>
    </div>


