<?php
/**
 * Created by PhpStorm.
 * User: MaxNeh
 * Date: 27.02.2018
 * Time: 14:33
 */

$hlID = $arResult['SECTION']['USER_FIELDS']['UF_GALLERY_CAT']['SETTINGS']['HLBLOCK_ID'];
$sID = $arResult['SECTION']['USER_FIELDS']['UF_GALLERY_CAT']['VALUE'];
if ($hlID && $sID) {
    $category = \Local\Lib\Content\Highload::getById($hlID, $sID);
    $arResult['SECTION']['UF_CAT_ICON'] = $category['UF_CAT_ICON'];
    $arResult['SECTION']['NAME'] = $category['UF_GALLERY_CAT'];
}