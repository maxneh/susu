$(document).ready(function () {
    var speed = 400,
        $li;

if ($('.preview-album-slider').length){
    $('body').on('init', '.preview-album-slider', function (slick) {
        $(slick.target).find('button').append('<span></span>');

        $li = $(slick.target).find('.slick-dots li');
        animateSpan(0, $li);
    });

    $('.preview-album-slider').slick({
        dots: true,
        autoplay: false,
        fade: true,
        autoplaySpeed: speed,
        arrows: false,

    });

    $('body').on('mouseover', '.preview-album-slider',function () {
        $(this).slick('play')
    });
    $('body').on('mouseout', '.preview-album-slider', function () {
        $(this).slick('pause')
    });

    $('body').on('afterChange', '.preview-album-slider', function (event, slick, currentSlide, nextSlide) {
        $li = $(event.target).find('.slick-dots li');
        // console.log($li);
        $li.find('button span').stop(true, true);
        $li.find('button').removeClass('filled');

        if (currentSlide == 0) {
            $li.find('button span').css({
                'transform': 'scaleX(0)',
                'border-spacing': 0
            });
        } else {
            // всем до текущего слайда назначить класс, чтобы они были заполнены
            for (var i = 0; i < currentSlide; i++) {
                $li.eq(i).find('button').addClass('filled');
            }

            // всем после текущего слайда убрать классы и ширину, чтобы были пустыми

            for (var i = currentSlide + 1; i < $li.length + 1; i++) {
                $li.eq(i - 1).find('button').removeClass('filled')
                    .find('span').css({
                    'transform': 'scaleX(0)',
                    'border-spacing': 0
                });
            }
        }

        animateSpan(currentSlide, $li);
    });

    function animateSpan(currentSlide, $li) {
        var $currentBtn = $li.eq(currentSlide).find('span');

        $currentBtn.animate({borderSpacing: 1}, {
            step: function (now, fx) {
                $(this).css('transform', 'scaleX(' + now + ')');
            },
            duration: speed
        }, 'linear');
    }
}
});