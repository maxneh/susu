<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<!--TPL: <?= $this->GetFolder()?>-->
<? $uniqId = (time()+rand(0, 1000))?>


<div class="webform-result-text-message">
    <?= $arResult["FORM_NOTE"] ?>
</div>

<? if ($arResult["isFormNote"] != "Y") {
    ?>
    <?= $arResult["FORM_HEADER"] ?>

    <div class="webform">
        <?
        if ($arResult["isFormTitle"] == "Y") {
            if ($arResult["isFormTitle"]) { ?>
                <h2 class="webform__title"><?= $arResult["FORM_TITLE"] ?></h2>
            <? }
        } ?>
        <? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>
        <?
        /***********************************************************************************
         * form questions
         ***********************************************************************************/
        ?>
        <div class="webform__questions row">

            <?
            foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
                //print_r($arQuestion);
                if ($FIELD_SID == 'message') {
                    CModule::IncludeModule("iblock");

                    $QUESTION_SID = "department_1";
                    $arDropDown = array(
                        "reference" => array('Не выбрано'),
                        "reference_id" => array('0')

                    );
                    $arSelect = Array("ID", "NAME", "DEPTH_LEVEL");
                    $arFilter = Array("IBLOCK_ID" => 15, 'DEPTH_LEVEL' => '1', "ACTIVE" => "Y");

                    $rsSect = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter, false, $arSelect);
                    while ($arSect = $rsSect->GetNext()) {
                        $arDropDown["reference"][] = $arSect["NAME"];
                        $arDropDown["reference_id"][] = $arSect["ID"];
                    }

                    $value = CForm::GetDropDownValue($QUESTION_SID, $arDropDown); ?>
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="webform__q-item col-xs-12 col-md-5">
                                <label>Подразделение</label>
                                <?= CForm::GetDropDownField(
                                    $QUESTION_SID,
                                    $arDropDown,
                                    $value,
                                    "class=\"inputselect\" onchange=\"loadHTML('/local/ajax/departments.php?uniqId=$uniqId&id=' + this . options[this . selectedIndex] . value, 'd_level_2_$uniqId');\""
                                ); ?>
                            </div>
                            <div class="col-md-1"></div>

                            <div id="d_level_2_<?=$uniqId?>" class="webform__q-item col-xs-12 col-md-5"></div>
                            <div class="col-md-1"></div>
                            <div id="d_level_3_<?=$uniqId?>" class="webform__q-item col-xs-12 col-md-5"></div>
                        </div>
                    </div>

                <? } ?>
                <div class="webform__q-item webform__q-item_<?= $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?> <?= $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea' ? 'col-xs-11' : 'col-xs-12 col-md-5' ?>">
                    <label>
                        <? if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])): ?>
                            <span class="error-fld" title="<?= htmlspecialcharsbx($arResult["FORM_ERRORS"][$FIELD_SID]) ?>"></span>
                        <? endif; ?>
                        <?= $arQuestion["CAPTION"] ?><? if ($arQuestion["REQUIRED"] == "Y"): ?><?= $arResult["REQUIRED_SIGN"]; ?><? endif; ?>
                        <?= $arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />" . $arQuestion["IMAGE"]["HTML_CODE"] : "" ?>
                    </label>
                    <?= $arQuestion["HTML_CODE"] ?>
                </div>
                <div class="col-md-1"></div>
                <?
            } //endwhile
            ?>

            <div class="webform__q-item webform__q-item_submit col-xs-12">

                <input <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?>
                        type="submit" class="btn_blue" name="web_form_submit"
                        value="<?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>"/>
                <!--                &nbsp;<input type="reset" value="--><? //= GetMessage("FORM_RESET"); ?><!--"/>-->

            </div>
        </div>
        <p>
            <?= $arResult["REQUIRED_SIGN"]; ?> - <?= GetMessage("FORM_REQUIRED_FIELDS") ?>
        </p>
    </div>

    <?= $arResult["FORM_FOOTER"] ?>
    <?
} //endif (isFormNote)
?>

<script language="javascript">
    var request, dest;

    function processStateChange() {
        if (request.readyState == 4) {
            contentDiv = document.getElementById(dest);
            if (request.status == 200) {
                response = request.responseText;
                contentDiv.innerHTML = response;
            } else {
                contentDiv.innerHTML = "Error: Status " + request.status;
            }
        }
    }

    function loadHTML(URL, destination) {
        dest = destination;

        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
            request.onreadystatechange = processStateChange;
            request.open("GET", URL, true);
            request.send(null);
        } else if (window.ActiveXObject) {
            request = new ActiveXObject("Microsoft.XMLHTTP");
            if (request) {
                request.onreadystatechange = processStateChange;
                request.open("GET", URL, true);
                request.send();
            }
        }
    }
</script>
