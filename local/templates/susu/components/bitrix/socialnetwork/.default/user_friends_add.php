<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$pageId = "";
include("util_menu.php");
?>

<?
$componentParameters = Array(
    "PATH_TO_USER" => $arResult["PATH_TO_USER"],
    "PAGE_VAR" => $arResult["ALIASES"]["page"],
    "USER_VAR" => $arResult["ALIASES"]["user_id"],
    "SET_NAV_CHAIN" => $arResult["SET_NAV_CHAIN"],
    "SET_TITLE" => $arResult["SET_TITLE"],
    "ID" => $arResult["VARIABLES"]["user_id"],
    "NAME_TEMPLATE" => $arParams["NAME_TEMPLATE"],
    "SHOW_LOGIN" => $arParams["SHOW_LOGIN"],
);

?>

<? if ($_REQUEST['IFRAME'] == 'Y') {
    $_REQUEST['sessid'] = bitrix_sessid();
    $APPLICATION->SetTitle("Добавить в друзья");
    $APPLICATION->IncludeComponent(
        "bitrix:socialnetwork.pageslider.wrapper",
        "",
        array(
            'POPUP_COMPONENT_NAME' => "bitrix:socialnetwork.user_friends_add",
            "POPUP_COMPONENT_TEMPLATE_NAME" => "",
            "POPUP_COMPONENT_PARAMS" => $componentParameters,
        )
    );
    ?>
    <script>
        BX.ready(function () {
            BX.SidePanel.Instance.bindAnchors({
                rules: [
                    {
                        condition: [
                            '/club/user/(\\d+)/'
                        ],
                        loader: 'loader',
                        options: {
                            width: 900
                        }
                    },
                ]
            });
        });
    </script>
    <?
} else {
    $APPLICATION->IncludeComponent(
        "bitrix:socialnetwork.user_friends_add",
        "",
        $componentParameters
    );
} ?>
