<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


/********************************************************************
 * Input params
 ********************************************************************/
$arParams["SHOW_TAGS"] = ($arParams["SHOW_TAGS"] == "Y" && IsModuleInstalled("search") ? "Y" : "N");
/********************************************************************
 * /Input params
 ********************************************************************/
$URL_NAME_DEFAULT = array(
    "search" => "PAGE_NAME=search",
    "detail_list" => "PAGE_NAME=detail_list&SECTION_ID=#SECTION_ID#&ELEMENT_ID=#ELEMENT_ID#",
    "section_edit" => "PAGE_NAME=section_edit&SECTION_ID=#SECTION_ID#&ACTION=#ACTION#",
    "upload" => "PAGE_NAME=upload&SECTION_ID=#SECTION_ID#&ACTION=upload"
);

foreach ($URL_NAME_DEFAULT as $URL => $URL_VALUE) {
    $arParams[strToUpper($URL) . "_URL"] = trim($arResult["URL_TEMPLATES"][strToLower($URL)]);
    if (empty($arParams[strToUpper($URL) . "_URL"]))
        $arParams[strToUpper($URL) . "_URL"] = $APPLICATION->GetCurPageParam($URL_VALUE, array("PAGE_NAME", "SECTION_ID", "ELEMENT_ID", "ACTION", "sessid", "edit", "order"));
    $arParams["~" . strToUpper($URL) . "_URL"] = $arParams[strToUpper($URL) . "_URL"];
    $arParams[strToUpper($URL) . "_URL"] = htmlspecialcharsbx($arParams["~" . strToUpper($URL) . "_URL"]);
}
if (is_array($arResult["MENU_VARIABLES"]) && isset($arResult["MENU_VARIABLES"]["ALL"]))
    $arResult = $arResult + $arResult["MENU_VARIABLES"]["ALL"];

if (!isset($_REQUEST["photo_list_action"]))
    ob_start();

?><? $arResultGalleries = $APPLICATION->IncludeComponent(
    "bitrix:photogallery.gallery.list",
    "ascetic",
    Array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "USER_ID" => "0",
        "SORT_BY" => "ID",
        "SORT_ORD" => "DESC",
        "INDEX_URL" => $arResult["URL_TEMPLATES"]["index"],
        "GALLERY_URL" => $arResult["URL_TEMPLATES"]["gallery"],
        "GALLERY_EDIT_URL" => $arResult["URL_TEMPLATES"]["gallery_edit"],
        "UPLOAD_URL" => $arResult["URL_TEMPLATES"]["upload"],
        "ONLY_ONE_GALLERY" => $arParams["ONLY_ONE_GALLERY"],
        "GALLERY_SIZE" => $arParams["GALLERY_SIZE"],
        "PAGE_ELEMENTS" => 10,
        "PAGE_NAVIGATION_TEMPLATE" => $arParams["PAGE_NAVIGATION_TEMPLATE"],
        "DATE_TIME_FORMAT" => $arParams["DATE_TIME_FORMAT_SECTION"],
        "SHOW_PHOTO_USER" => $arParams["SHOW_PHOTO_USER"],
        "SECTION_FILTER" => array(">ELEMENTS_CNT" => 0),
        "GALLERY_AVATAR_SIZE" => $arParams["GALLERY_AVATAR_SIZE"],
        "SET_STATUS_404" => "N",

        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
        "SET_TITLE" => "N",

        "GALLERY_AVATAR_SIZE" => $arParams["GALLERY_AVATAR_SIZE"],
        "SHOW_PAGE_NAVIGATION" => "none"
    ),
    $component,
    array("HIDE_ICONS" => "Y")); ?><?
if (!isset($_REQUEST["photo_list_action"]))
    $sGalleryList = ob_get_clean();

if (empty($sGalleryList) && !isset($_REQUEST["photo_list_action"])) {
    $sError = GetMessage("P_ERROR1") . " ";
    if ($GLOBALS["USER"]->IsAuthorized()) {
        if (empty($arResult["MY_GALLERY"]) && $arResult["I"]["ACTIONS"]["CREATE_GALLERY"] == "Y")
            $sError .= GetMessage("P_ERROR2");
        elseif (!empty($arResult["MY_GALLERY"]))
            $sError .= GetMessage("P_ERROR3");
    }
    $sError = str_replace(array("#CREATE_GALLERY#", "#UPLOAD#"), array($arResult["LINK"]["NEW"], $arResult["MY_GALLERY"]["LINK"]["UPLOAD"]), $sError);
    ?>
    <div class="photo-note-box">
        <div class="photo-note-box-text">
            <?= $sError ?>
        </div>
    </div>
    <?
    return false;
} ?>




<div class="photo-page-main">
    <? $APPLICATION->IncludeComponent(
        "susu:photogallery.section.list",
        "",
        Array(
            "USE_FILTER" => "N",
            "FILTER_NAME" => 'arrFilterGallery',
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "PHOTO_LIST_MODE" => $arParams["PHOTO_LIST_MODE"],
            "SHOWN_ITEMS_COUNT" => $arParams["SHOWN_ITEMS_COUNT"],
            'MAX_SECTIONS_CNT' => 0,

            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
            "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
            "DATE_TIME_FORMAT" => $arParams["DATE_TIME_FORMAT_SECTION"],

            "PAGE_NAVIGATION_TEMPLATE" => $arParams["PAGE_NAVIGATION_TEMPLATE"],
            "PAGE_ELEMENTS" => $arParams["SECTION_PAGE_ELEMENTS"],
            "SORT_BY" => $arParams["SECTION_SORT_BY"],
            "SORT_ORD" => $arParams["SECTION_SORT_ORD"],

            "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
            "SHOW_TAGS" => $arParams["SHOW_TAGS"],

            "SECTION_URL" => $arResult["URL_TEMPLATES"]["gallery"],
            "SECTION_EDIT_URL" => $arResult["URL_TEMPLATES"]["section_edit"],
            "SECTION_EDIT_ICON_URL" => $arResult["URL_TEMPLATES"]["section_edit_icon"],
            "DETAIL_URL" => $arResult["URL_TEMPLATES"]["detail"],
            "SEARCH_URL" => $arResult["URL_TEMPLATES"]["search"],
            "UPLOAD_URL" => $arResult["URL_TEMPLATES"]["upload"],
            "DETAIL_EDIT_URL" => $arResult["URL_TEMPLATES"]["detail_edit"],

            "ALBUM_PHOTO_THUMBS_SIZE" => $arParams["ALBUM_PHOTO_THUMBS_SIZE"],
            "ALBUM_PHOTO_SIZE" => $arParams["ALBUM_PHOTO_SIZE"],
            "SECTION_LIST_THUMBNAIL_SIZE" => $arParams["SECTION_LIST_THUMBNAIL_SIZE"],
            "SET_STATUS_404" => $arParams["SET_STATUS_404"],

            "SET_TITLE" => "N",
            "SHOW_RATING" => $arParams["USE_RATING"],
            "SHOW_SHOWS" => $arParams["SHOW_SHOWS"],
            "SHOW_COMMENTS" => $arParams["USE_COMMENTS"],
            "SHOW_DATE" => $arParams["SHOW_DATE"],
            "SHOW_DESRIPTION" => $arParams["SHOW_DESRIPTION"],

            "USE_RATING" => $arParams["USE_RATING"],
            "MAX_VOTE" => $arParams["MAX_VOTE"],
            "VOTE_NAMES" => $arParams["VOTE_NAMES"],
            "DISPLAY_AS_RATING" => $arParams["DISPLAY_AS_RATING"],
            "RATING_MAIN_TYPE" => $arParams["RATING_MAIN_TYPE"],

            "USE_COMMENTS" => $arParams["USE_COMMENTS"],
            "COMMENTS_TYPE" => $arParams["COMMENTS_TYPE"],

            "COMMENTS_COUNT" => $arParams["COMMENTS_COUNT"],
            "PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
            "FORUM_ID" => $arParams["FORUM_ID"],
            "USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
            "URL_TEMPLATES_READ" => $arParams["URL_TEMPLATES_READ"],
            "URL_TEMPLATES_PROFILE_VIEW" => $arParams["URL_TEMPLATES_PROFILE_VIEW"],
            "POST_FIRST_MESSAGE" => $arParams["POST_FIRST_MESSAGE"],
            "PREORDER" => $arParams["PREORDER"],
            "SHOW_LINK_TO_FORUM" => $arParams["SHOW_LINK_TO_FORUM"] == "Y" ? "Y" : "N",

            "BLOG_URL" => $arParams["BLOG_URL"],
            "PATH_TO_BLOG" => $arParams["PATH_TO_BLOG"],

            "PATH_TO_USER" => $arParams["PATH_TO_USER"],
            "NAME_TEMPLATE" => $arParams["NAME_TEMPLATE"],
            "SHOW_LOGIN" => $arParams["SHOW_LOGIN"],

            "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
            "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
        ),
        $component,
        array("HIDE_ICONS" => "Y")
    )
    ?>
    <?
    if ($arParams["SET_TITLE"] != "N"):
        $GLOBALS["APPLICATION"]->SetTitle(GetMessage("P_PHOTO"));
    endif;
    ?>
    <div class="empty-clear"></div>
</div>



