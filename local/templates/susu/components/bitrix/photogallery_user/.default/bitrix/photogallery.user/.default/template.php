<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult["GALLERY"]) || !$this->__component->__parent)
    return false;

/********************************************************************
 * Input params
 ********************************************************************/
$arParams["GALLERY_AVATAR_SIZE"] = intVal(intVal($arParams["GALLERY_AVATAR_SIZE"]) > 0 ? $arParams["GALLERY_AVATAR_SIZE"] : 50);
/********************************************************************
 * /Input params
 ********************************************************************/
?>

<div class="photo-item photo-gallery-item">
    <div class="row">
        <div class="col-8">
            <div class="photo-gallery-avatar <?= (empty($arResult["GALLERY"]["PICTURE"]["SRC"]) ? "photo-gallery-avatar-empty" : "")
            ?>" <? if (!empty($arResult["GALLERY"]["PICTURE"])) { ?> style="background-image:url('<?= $arResult["GALLERY"]["PICTURE"]["SRC"] ?>');"<? } ?>></div>
            <div class="photo-gallery-name"><h3><?= $arResult["GALLERY"]["NAME"] ?></h3></div>

            <?
            if (!empty($arResult["GALLERY"]["DESCRIPTION"])) {
                ?>
                <div class="photo-gallery-description"><?= $arResult["GALLERY"]["DESCRIPTION"] ?></div>
                <?
            }
            ?>
        </div>
        <div class="col-4">
            <!--   <? if ($arParams["PERMISSION"] >= "U") { ?>
                <noindex>

                    <div class="photo-controls photo-controls-buttons photo-controls-gallery">
                        <ul class="photo-controls">
                            <li class="photo-control">
                                <a target="_self" href="<?= $arResult["GALLERY"]["LINK"]["EDIT"] ?>"><?= GetMessage("P_EDIT") ?></a>
                            </li>
                            <li class="photo-control photo-control-album-add">
                                <a onclick="EditAlbum('<?= CUtil::JSEscape(htmlspecialcharsbx($arResult["GALLERY"]["LINK"]["~NEW_ALBUM"])) ?>'); return false;"
                                   <?
                                   ?>rel="nofollow"
                                   href="<?= $arResult["GALLERY"]["LINK"]["NEW_ALBUM"] ?>"><span><?= GetMessage("P_ADD_ALBUM") ?></span></a>
                            </li>
                            <?
                            ?>
                            <li class="photo-control photo-control-last photo-control-album-upload">
                                <a target="_self" rel="nofollow"
                                   href="<?= $arResult["GALLERY"]["LINK"]["UPLOAD"] ?>"><span><?= GetMessage("P_UPLOAD") ?></span></a>
                            </li>
                        </ul>
                    </div>
                </noindex>
                <?
            }
            ?>
-->
        </div>
    </div>
</div>
