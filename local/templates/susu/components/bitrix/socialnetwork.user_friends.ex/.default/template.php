<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
    <!--TPL: <?= $this->GetFolder()?>-->
<?
if (strlen($arResult["FatalError"]) > 0) {
    ?><span class='errortext'><?= $arResult["FatalError"] ?></span><br/><br/><?
} else {

    if (strlen($arResult["ErrorMessage"]) > 0) {
        ?><span class="errortext"><?= $arResult["ErrorMessage"] ?></span><br/><br/><?
    } ?>
    <div class="sonet-members-item">

        <? if ($arResult["CurrentUserPerms"] && $arResult["CurrentUserPerms"]["IsCurrentUser"]) { ?>
            <div class="sonet-add-button">
                <a href="/spravochnik/kontakty-sotrudnikov/" class="btn soc-add-btn">Добавить друга</a>
            </div>
        <? } ?>

        <? if (is_array($arResult["Friends"]) && is_array($arResult["Friends"]["List"])) {
            ?>
            <div class="sonet-members-wrap row">
                <? foreach ($arResult["Friends"]["List"] as $arFriend) { ?>
                    <div class="sonet-members-member-block col-xs-12 col-sm-6 col-md-4">
                        <div class="sonet-members-member-img"
                             style="<?= (is_array($arFriend["USER_PERSONAL_PHOTO_IMG"]) &&
                             strlen($arFriend["USER_PERSONAL_PHOTO_IMG"]["src"]) > 0 ?
                                 "background: url('" . $arFriend["USER_PERSONAL_PHOTO_IMG"]["src"] . "') no-repeat 0 0;" :
                                 "") ?>">

                        </div>
                        <? $arUserTmp = array(
                            "ID" => $arFriend["USER_ID"],
                            "NAME" => htmlspecialcharsback($arFriend["USER_NAME"]),
                            "LAST_NAME" => htmlspecialcharsback($arFriend["USER_LAST_NAME"]),
                            "SECOND_NAME" => htmlspecialcharsback($arFriend["USER_SECOND_NAME"]),
                            "LOGIN" => htmlspecialcharsback($arFriend["USER_LOGIN"])
                        ); ?>
                        <? if ($arFriend["SHOW_PROFILE_LINK"]) { ?>
                            <a href="<?= htmlspecialcharsback($arFriend["USER_PROFILE_URL"]) ?>"
                               class="sonet-members-member-link">
                            <span><?= CUser::FormatName(str_replace(array("#NOBR#", "#/NOBR#"), array("", ""), $arParams["NAME_TEMPLATE"]), $arUserTmp,
                                    $arParams["SHOW_LOGIN"] != "N") ?></span>
                            </a>
                        <? } else {
                            ?><span class="sonet-members-member-link"><?= CUser::FormatName(str_replace(array("#NOBR#", "#/NOBR#"), array("", ""),
                            $arParams["NAME_TEMPLATE"]), $arUserTmp, $arParams["SHOW_LOGIN"] != "N") ?></span>
                        <? } ?>
                        <div class="sonet-members-command-block">
                            <a class="sonet-members-command" href="/club/messages/chat/<?= $arFriend['USER_ID'] ?>/" onclick="if (typeof(BX) != 'undefined' && BX.IM) {
                                    BXIM.openMessenger(<?= $arFriend['USER_ID'] ?>); return false;
                                    } else {
                                    window.open('/club/messages/chat/<?= $arFriend['USER_ID'] ?>/',
                                    '',
                                    'location=yes,status=no,scrollbars=yes,resizable=yes,width=700,height=550,top='+Math.floor((screen.height - 550)/2-14)+',left='+Math.floor((screen.width - 700)/2-5));
                                    return false; }"><span>Написать</span></a>
                            <a class="sonet-members-command" href="/club/user/<?= $arFriend['USER_ID'] ?>/friends/delete/"><span>Удалить из друзей</span></a>

                        </div>
                    </div>
                <? } ?>
            </div>
        <? } else echo GetMessage("SONET_UFE_T_NO_FRIENDS"); ?>

        <? if (StrLen($arResult["Friends"]["NAV_STRING"]) > 0) { ?>
            <div class="sonet-members-nav"><?= $arResult["Friends"]["NAV_STRING"] ?></div>
        <? } ?>

    </div>
<? } ?>