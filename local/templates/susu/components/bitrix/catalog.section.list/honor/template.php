<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strTitle = "";
?>
<!--TPL: <?= $this->GetFolder()?>-->
<div class="board-of-honor">
    <?
    $TOP_DEPTH = $arResult["SECTION"]["DEPTH_LEVEL"];
    $CURRENT_DEPTH = $TOP_DEPTH;

//   echo "<pre>";
//   print_r($arResult["SECTIONS"]);
    foreach ($arResult["SECTIONS"] as $arSection) {
        $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
        $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));

        if ($arSection["DEPTH_LEVEL"] == 1) { ?>
            <div id="<?= $this->GetEditAreaId($arSection['ID']); ?>" class="bh-level row">
                <div class="bh-level__title col-md-4 col-xs-12"><?= $arSection["NAME"] ?></div>
                <div class="bh-level__desc col-md-8 col-xs-12"><?= $arSection["DESCRIPTION"] ?></div>
            </div>
        <? } else {
            if (isset($arSection['USERS'])) {
                ?>
                <div id="<?= $this->GetEditAreaId($arSection['ID']); ?>" class="bh-nom">
                    <div class='bh-nom__title'><?= $arSection["NAME"] ?></div>
                    <div class='bh-nom__desc'><?= $arSection["DESCRIPTION"] ?></div>
                </div>
                <div class="row bh-users">
                    <?
                    foreach ($arSection['USERS'] as $arUser) {
                        ?>
                        <div class="bh-user col-md-4 col-xs-12">
                            <div class="bh-user__avatar" style="background-image: url(<?= $arUser["PERSONAL_PHOTO_PATH"] ?>);"></div>
                            <div class="bh-user__name">
                                <?= !empty($arUser['NAME']) ? "<p>" . $arUser['NAME'] . "</p><br>" : ""; ?>
                                <?= !empty($arUser['SECOND_NAME']) ? "<p>" . $arUser['SECOND_NAME'] . "</p><br>" : "" ?>
                                <?= !empty($arUser['LAST_NAME']) ? "<p>" . $arUser['LAST_NAME'] . "</p>" : "" ?>
                            </div>

                        </div>
                    <? } ?>
                </div>
            <? } ?>
        <? } ?>
    <? } ?>
</div>


