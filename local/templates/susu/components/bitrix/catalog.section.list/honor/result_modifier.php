<?php

if (0 < $arResult['SECTIONS_COUNT']) {
    $userCount = false;
    $arSelect = array('ID');
    $arMap = array();


    foreach ($arResult['SECTIONS'] as $key => $arSection) {
        $arMap[$arSection['ID']] = $key;
        $filter = Array(
            "UF_HONOR" => $arSection['ID'],
            "ACTIVE" => "Y",
        );
        $by = "active";
        $order = "desc";
        $rsUsers = CUser::GetList($by, $order, $filter, array('SELECT'=>array("UF_HONOR", "UF_HONOR_PHOTO"))); // выбираем пользователей

        if ($rsUsers->SelectedRowsCount()) {
            while ($arUser = $rsUsers->NavNext()) {
//                print "<pre>";
//                var_dump($arUser);
                $arUser["PERSONAL_PHOTO_HTML"] = CFile::ShowImage($arUser["UF_HONOR_PHOTO"], 194, 270, "class='img-responsive'", false, false);
                $arUser["PERSONAL_PHOTO_PATH"] = CFile::GetPath($arUser["UF_HONOR_PHOTO"]);
                $arResult['SECTIONS'][$key]['USERS'][$arUser['ID']] = $arUser;
            }

        }
    }


}
?>