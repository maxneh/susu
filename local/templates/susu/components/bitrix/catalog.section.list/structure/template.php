<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);



$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?>
<!--TPL: <?= $this->GetFolder()?>-->
<div class="bx_catalog_text clearfix">
    <? $this->AddEditAction($arResult['SECTION']['ID'], $arResult['SECTION']['EDIT_LINK'], $strSectionEdit);
    $this->AddDeleteAction($arResult['SECTION']['ID'], $arResult['SECTION']['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams); ?>

    <h2 class="" id="<? echo $this->GetEditAreaId($arResult['SECTION']['ID']); ?>">
        <?= isset($arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]) &&
        $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != ""
            ? $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]
            : $arResult['SECTION']['NAME'];
        ?></h2>


    <?
    if (0 < $arResult["SECTIONS_COUNT"]) {
        ?>
        <ul class="bx_catalog_text_ul">
            <? foreach ($arResult['SECTIONS'] as &$arSection) { ?>
                <? $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams); ?>
                <li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                    <h4 class="bx_catalog_text_title">
                        <a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a>
                    </h4>
                </li>
            <? } ?>
            <? unset($arSection); ?>
        </ul>
    <? } ?>
</div>

<div class="description">
    <?= $arResult['SECTION']['DESCRIPTION'] ?>
</div>

<h2>Сотрудники подразделения</h2>

<div class="user-list-wrap">
    <? global $UserDepFilter;

    $UserDepFilter = array("UF_USER_DEPARTMENT" => $arResult['SECTION']['ID'], "ACTIVE" => "Y"); ?>

    <? $APPLICATION->IncludeComponent(
        "susu:user.list",
        ".default",
        array(
            "FILTER_NAME" => 'UserDepFilter',
            "ALLOW_RATING_SORT" => "N",
            "CACHE_TIME" => "3600",
            "CACHE_TYPE" => "A",
            "DATE_TIME_FORMAT" => "d.m.Y H:i:s",
            "ITEMS_COUNT" => "",
            "NAME_TEMPLATE" => "",
            "PAGE_VAR" => "",
            "PATH_TO_MESSAGES_CHAT" => "",
            "PATH_TO_MESSAGE_FORM" => "",
            "PATH_TO_SEARCH" => "",
            "PATH_TO_SEARCH_INNER" => "",
            "PATH_TO_USER" => "/club/user/#user_id#/",
            "PATH_TO_USER_FRIENDS_ADD" => "",
            "RATING_ID" => "3",
            "RATING_TYPE" => "like",
            "SET_NAV_CHAIN" => "N",
            "SHOW_RATING" => "N",
            "SHOW_USERS_WITHOUT_FILTER_SET" => "Y",
            "SHOW_YEAR" => "N",
            "USER_FIELDS_LIST" => array(
                0 => "EMAIL",
                1 => "PERSONAL_PHONE",
                2 => "PERSONAL_MOBILE",
                3 => "WORK_POSITION",
            ),
            "USER_PROPERTIES_LIST" => array(
                0 => "UF_USER_DEPARTMENT",
            ),
            "USER_PROPERTIES_SEARCH_ADV" => array(),
            "USER_VAR" => "",
            "COMPONENT_TEMPLATE" => ".default"
        ),
        false
    ); ?>

</div>