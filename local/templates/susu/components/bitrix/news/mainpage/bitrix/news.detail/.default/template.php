<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? $date = FormatDate(array(
    "tommorow" => "tommorow, j F",      // выведет "завтра, 9 июля", если дата завтрашний день
    "today" => "today, j F",              // выведет "сегодня, 9 июля", если дата текущий день
    "yesterday" => "yesterday, j F",       // выведет "вчера, 9 июля", если дата прошлый день
    "d" => 'j F',                   // выведет "9 июля", если месяц прошел
    "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
), MakeTimeStamp($arResult["DISPLAY_ACTIVE_FROM"]), time()
);
?>
<!--TPL: <?= $this->GetFolder()?>-->
<? $APPLICATION->IncludeComponent('susu:list.detail.header', '',
    array(
        'IBLOCK_TYPE' => 'news',
        'TITLE' => $arResult["NAME"],
        'DATE' => $date,
        'SECTION_URL' => $arResult['SECTION_URL'],
        'SECTION_ID' => $arResult['IBLOCK_SECTION_ID'],
        'SECTION_ICON' => $arResult['SECTION']['UF_CAT_ICON'],
        'SECTION_NAME' =>$arResult['SECTION']['NAME'],
        'TOLEFT_URL' => $arResult["TOLEFT"]["URL"],
        'TORIGHT_URL' => $arResult["TORIGHT"]['URL'],
        'LIKE' => Array(
            "IBLOCK_ID" => "3",
            "IBLOCK_TYPE" => "news",
            "ELEMENT_ID" => $arResult["ID"],
            "ENTITY_TYPE_ID" => "IBLOCK_ELEMENT",
            "ENTITY_ID" => $arResult["ID"],
            "OWNER_ID" => $res["AUTHOR_ID"],
            "PATH_TO_USER_PROFILE" => ""
        ),
        'SHAREIT' => Array(
            'LINK_TITLE'=>$arResult["NAME"],
            'LINK_URL' =>$arResult['DETAIL_PAGE_URL'],
            'BACK_URL' =>$APPLICATION->getCurUri()
        ),
        'FAVOR'=>Array(
//            "AJAX_MODE" => "Y",
            "ENTITY_TYPE_ID" => "NEWS",
            "ENTITY_ID" => $arResult["ID"],
            'LINK_TITLE'=>$arResult["NAME"],
            'LINK_URL' =>$arResult['DETAIL_PAGE_URL']
        ),
        'SHOW_COMMENTS' => 'Y',
        'COMMENTS_CNT'=>$arResult["PROPERTIES"]["FORUM_MESSAGE_CNT"]["VALUE"],
    ),
    $component
) ?>


<div class="news-detail">
    <div class="row">
        <div class="col-12"><? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arResult["DETAIL_PICTURE"])): ?>
                <img
                        class="detail_picture"
                        border="0"
                        src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>"
                        width="<?= $arResult["DETAIL_PICTURE"]["WIDTH"] ?>"
                        alt="<?= $arResult["DETAIL_PICTURE"]["ALT"] ?>"
                        title="<?= $arResult["DETAIL_PICTURE"]["TITLE"] ?>"
                        style="float:left; margin-right: 12px"/>
            <? endif ?>
            <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arResult["FIELDS"]["PREVIEW_TEXT"]): ?>
            <div class="news-detail__preview-text">   <? echo $arResult["PREVIEW_TEXT"]; ?></div>
            <? endif; ?>
        </div>

        <? if ($arResult["NAV_RESULT"]): ?>
            <? if ($arParams["DISPLAY_TOP_PAGER"]): ?><?= $arResult["NAV_STRING"] ?><br/><? endif; ?>
            <? echo $arResult["NAV_TEXT"]; ?>
            <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?><br/><?= $arResult["NAV_STRING"] ?><? endif; ?>
        <? elseif (strlen($arResult["DETAIL_TEXT"]) > 0): ?>
            <div class="col-12">    <? echo $arResult["DETAIL_TEXT"]; ?></div>
        <? endif ?>
    </div>
    <br/>

    <?
    if (array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y") {
        ?>
        <div class="news-detail-share">
            <noindex>
                <?
                $APPLICATION->IncludeComponent("bitrix:main.share", "", array(
                    "HANDLERS" => $arParams["SHARE_HANDLERS"],
                    "PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
                    "PAGE_TITLE" => $arResult["~NAME"],
                    "SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                    "SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                    "HIDE" => $arParams["SHARE_HIDE"],
                ),
                    $component,
                    array("HIDE_ICONS" => "Y")
                );
                ?>
            </noindex>
        </div>
        <?
    }
    ?>
    <? if ($arParams["USE_REVIEW"] == "Y") {?>
    <? $comments_cnt = $arResult["PROPERTIES"]["FORUM_MESSAGE_CNT"]["VALUE"]; ?>
    <h2 id="comment-anchor">Комментарии (<?= $comments_cnt ? $comments_cnt : 0 ?>)</h2>
    <?}?>
</div>

