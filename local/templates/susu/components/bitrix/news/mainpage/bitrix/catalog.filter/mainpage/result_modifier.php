<?php
/**
 * Created by PhpStorm.
 * User: MaxNeh
 * Date: 27.02.2018
 * Time: 11:32
 */

$arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'ACTIVE'=>'Y');
$db_list = CIBlockSection::GetList(Array("ID"=>"ASC"), $arFilter, false, Array('ID','NAME', "UF_CAT_ICON"));
while($uf_value = $db_list->GetNext()){
    $uf_fields[$uf_value["ID"]]=$uf_value;
}

foreach ($arResult['arrSection'] as $key => $item) {

    $arResult['SECTION_ICONS'] = $uf_fields;

}
