<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
?>
<? $uniqId = (time() + rand(0, 1000)) ?>
<!--TPL: <?= $this->GetFolder() ?>-->
<div class="news-filter">
    <a href="#" class="reset-filter pull-right" data-action="del_filter"><span>Сбросить фильтр</span></a>
    <form id="news-filter-form" name="<? echo $arResult["FILTER_NAME"] . "_form" ?>" action="<? echo $arResult["FORM_ACTION"] ?>" method="post">
        <? foreach ($arResult["ITEMS"] as $arItem):
            if (array_key_exists("HIDDEN", $arItem)):
                echo $arItem["INPUT"];
            endif;
        endforeach; ?>

        <?
        CModule::IncludeModule("iblock");
        CModule::IncludeModule("form");

        $QUESTION_SID = "department_1";
        $arDropDown = array(
            "reference" => array('Не выбрано'),
            "reference_id" => array('')

        );
        $arSelect = Array("ID", "NAME", "DEPTH_LEVEL");
        $arFilter = Array("IBLOCK_ID" => 15, 'DEPTH_LEVEL' => '1', "ACTIVE" => "Y");

        $rsSect = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter, false, $arSelect);
        while ($arSect = $rsSect->GetNext()) {
            $arDropDown["reference"][] = $arSect["NAME"];
            $arDropDown["reference_id"][] = $arSect["ID"];
        }

        $value = CForm::GetDropDownValue($QUESTION_SID, $arDropDown); ?>

        <input id="dep_filter" type="hidden" name="<?= $arResult["FILTER_NAME"] ?>_DEPARTMENT" value="">
        <div class="news-filter-row">
            <div class="">
                <label>Подразделение</label>
                <?= CForm::GetDropDownField(
                    $QUESTION_SID,
                    $arDropDown,
                    $value,
                    "class=\"inputselect\" onchange=\"loadHTML('/local/ajax/departments.php?uniqId=$uniqId&id=' + this . options[this . selectedIndex] . value, 'd_level_2_$uniqId');\""
                ); ?>
            </div>
            <div id="d_level_2_<?= $uniqId ?>" class=""></div>
            <div id="d_level_3_<?= $uniqId ?>" class=""></div>
        </div>

        <div class="clearfix"></div>
        <div class="news-filter-row">

            <? $sectionFf = $request->getPost($arResult['FILTER_NAME'] . "_ff"); ?>
            <span class="news-filter-text">Рубрика: </span>

            <? foreach ($arResult['arrSection'] as $key => $item) { ?>
                <? if ($key) { ?>
                    <label class="news-filter-category checkmark">
                        <input type="checkbox" name="<?= $arResult["FILTER_NAME"] ?>_ff[SECTION_ID][]" value="<?= $key ?>" class="checkmark__input
                    filter-control"
                            <?= ($key == $sectionFf['SECTION_ID']) ? 'checked' : '' ?>
                        >
                        <div class="checkmark__icon icon icon-<?= $arResult['SECTION_ICONS'][$key]['UF_CAT_ICON'] ?>"
                             title="<?= $arResult['SECTION_ICONS'][$key]['NAME'] ?>"></div>
                    </label>
                <? } ?>
            <? } ?>

            &nbsp;
            <span class="news-filter-header">Дата: </span>
            <label class="news-filter-text"><span class="news-filter-text">c</span><input class="date-control filter-control" type="text"
                                                                                          name="<?= $arResult["FILTER_NAME"] ?>_DATE_ACTIVE_FROM_1"
                                                                                          onclick="BX.calendar({node: this, field: this, bTime: false});"
                                                                                          value="<?= $request->getPost($arResult['FILTER_NAME'] . '_DATE_ACTIVE_FROM_1') ?>"><span
                        class="date-control-icon"></span>
            </label>
            &nbsp;
            <label class="news-filter-text"><span class="news-filter-text">до</span><input class="date-control filter-control" type="text"
                                                                                           name="<?= $arResult["FILTER_NAME"] ?>_DATE_ACTIVE_FROM_2"
                                                                                           onclick="BX.calendar
        ({node: this,
         field: this, bTime: false});"
                                                                                           value="<?= $request->getPost($arResult['FILTER_NAME'] . '_DATE_ACTIVE_FROM_2') ?>"><span
                        class="date-control-icon"></span>
            </label>
            <div>
                <input type="submit" name="set_filter" value="<?= GetMessage("IBLOCK_SET_FILTER") ?>" class="hide"/>
            </div>
        </div>

    </form>

</div>
<script>
    $(document).ready(function () {
        $('.reset-filter').on('click', function (e) {
            e.preventDefault();
            var form = $('#news-filter-form');
            //  form.append('<input type="hidden" name="del_filter" value="Y">');
            form[0].reset();
            $("#dep_filter").val('');
            var data = BX.ajax.prepareData(BX.ajax.prepareForm(BX('news-filter-form')).data);
            filterAjaxSend(data);
            return false;
        });

        $('.filter-control').each(function () {
            $(this).on('change', function (event) {
                var data = BX.ajax.prepareData(BX.ajax.prepareForm(BX('news-filter-form')).data);
                filterAjaxSend(data);
            });
        });

        $("body").on('change', '#news-filter-form .inputselect', function (event) {
            $("#dep_filter").val($(this).val());
            var data = BX.ajax.prepareData(BX.ajax.prepareForm(BX('news-filter-form')).data);
            filterAjaxSend(data);
        });


        function filterAjaxSend(data) {
            BX.ajax({
                url: '/news-ajax.php',
                data: data,
                method: 'POST',
                // dataType: 'raw',
                timeout: 30,
                async: true,
                processData: true,
                scriptsRunFirst: true,
                emulateOnload: true,
                start: true,
                cache: false,
                onsuccess: function (data) {
                    $(".news-list").html(data);
                },
                onfailure: function () {

                }
            });
        }
    });

</script>