<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<!--TPL: <?= $this->GetFolder()?>-->
<div class="news-list news-list_more">

    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br/>
    <? endif; ?>

    <div class="row">

    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>

        <? $date = FormatDate(array(
            "tommorow" => "tommorow, j F",      // выведет "завтра, 9 июля", если дата завтрашний день
            "today" => "today, j F",              // выведет "сегодня, 9 июля", если дата текущий день
            "yesterday" => "yesterday, j F",       // выведет "вчера, 9 июля", если дата прошлый день
            "d" => 'j F',                   // выведет "9 июля", если месяц прошел
            "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
        ), MakeTimeStamp($arItem["DISPLAY_ACTIVE_FROM"]), time()
        );
        ?>

        <div class="news-item col-xs-12 col-md-4">
            <div class="news-item__img">
                <span class="category-icon icon icon-<?= $arItem['SECTION']['UF_CAT_ICON'] ?>" title="<?= $arItem['SECTION']['NAME'] ?>"></span>
                <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img class="preview_picture" border="0" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                                                     alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                                                     title="<?= $arItem["NAME"] ?>"/></a>
                <? endif ?>
            </div>
            <div class="news-item__info">
                <? if ($arParams["DISPLAY_DATE"] != "N" && $arItem["DISPLAY_ACTIVE_FROM"]): ?>
                    <time class="news-item__date"><?= $date ?></time>
                <? endif ?>
                <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><h4 class="news-item__title"><span><? echo $arItem["NAME"] ?></span></h4></a>
                <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                    <div class="news-item__description"><? echo $arItem["PREVIEW_TEXT"]; ?></div>
                <? endif; ?>
                <div class="news-item__socio">
                    <div class="pull-left">
                        <?
                        $arRatingParams = Array(
                            "IBLOCK_ID" => $arItem["IBLOCK_ID"],
                            "IBLOCK_TYPE" => $arItem["IBLOCK_TYPE"],
                            "ELEMENT_ID" => $arItem["ID"],
                            "ENTITY_TYPE_ID" => "IBLOCK_ELEMENT",
                            "ENTITY_ID" => $arItem["ID"],
                            "PATH_TO_USER_PROFILE" => ""
                        );

                        $GLOBALS["APPLICATION"]->IncludeComponent("bitrix:rating.vote", "like", $arRatingParams, false, array("HIDE_ICONS" => "Y"));
                        ?>
                        <?$arShareParams = array(
                            "LINK_TITLE" => $arItem["NAME"],
                            "LINK_URL" => $arItem["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]['SRC'],
                            "BACK_URL" => $APPLICATION->GetCurUri(),
                        )?>
                        <? $GLOBALS["APPLICATION"]->IncludeComponent("susu:shareit", "", $arShareParams, $component);?>
                        <?$arFavorParams = array(
//                            "AJAX_MODE" => "Y",
                            "ENTITY_TYPE_ID" => "BOARD",
                            "ENTITY_ID" => $arItem["ID"],
                            "LINK_TITLE" => $arItem["NAME"],
                            "LINK_URL" => $arItem["DETAIL_PAGE_URL"],
                        )?>
                        <? $GLOBALS["APPLICATION"]->IncludeComponent("susu:favorites", "", $arFavorParams, $component);?>
                    </div>
                    <div class="pull-right">
                        <? if (isset($arItem["DISPLAY_PROPERTIES"]["FORUM_MESSAGE_CNT"])): ?>
                            <a href="<?=$arItem["DETAIL_PAGE_URL"] ?>#comment-anchor"> <i class="comments-icon"><?= $arItem["DISPLAY_PROPERTIES"]["FORUM_MESSAGE_CNT"]["VALUE"] ?></i></a>
                        <? else: ?>
                            <a href="<?=$arItem["DETAIL_PAGE_URL"] ?>#comment-anchor"><i class="comments-icon">0</i></a>
                        <? endif ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>


        </div>
    <? endforeach; ?>
    </div>

    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br/><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>

</div>
