<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
?>
<!--TPL: <?= $this->GetFolder() ?>-->
<div class="news-filter">
    <a href="#" class="reset-filter" data-action="del_filter" ><span>Сбросить фильтр</span></a>
    <form id="news-filter-form" name="<? echo $arResult["FILTER_NAME"] . "_form" ?>" action="<? echo $arResult["FORM_ACTION"] ?>" method="post">
        <? foreach ($arResult["ITEMS"] as $arItem):
            if (array_key_exists("HIDDEN", $arItem)):
                echo $arItem["INPUT"];
            endif;
        endforeach; ?>
        <? $sectionFf = $request->getPost($arResult['FILTER_NAME'] . "_ff"); ?>


        <? /* foreach ($arResult["ITEMS"] as $arItem): ?>
            <? if (!array_key_exists("HIDDEN", $arItem)): ?>
                <div class="filter__item">
                    <div><?= $arItem["NAME"] ?>:</div>
                    <div><?= $arItem["INPUT"] ?></div>
                </div>
            <? endif ?>
        <? endforeach; */ ?>
        <span class="news-filter__text">Рубрика: </span>

        <? foreach ($arResult['arrSection'] as $key => $item) { ?>
            <? if ($key) { ?>
                <label class="news-filter-category checkmark">
                    <input type="checkbox" name="<?= $arResult["FILTER_NAME"] ?>_ff[SECTION_ID][]" value="<?= $key ?>" class="checkmark__input
                    filter-control"
                        <?= ($key == $sectionFf['SECTION_ID']) ? 'checked' : '' ?>
                    >
                    <div class="checkmark__icon icon icon-<?= $arResult['SECTION_ICONS'][$key]['UF_CAT_ICON'] ?>"
                         title="<?= $arResult['SECTION_ICONS'][$key]['NAME'] ?>"></div>
                </label>
            <? } ?>
        <? } ?>
        &nbsp;

        <span class="news-filter__text">Дата: </span>
        <label class="news-filter__line"><span class="news-filter__text">c</span><input class="date-control filter-control" type="text" name="<?= $arResult["FILTER_NAME"] ?>_DATE_ACTIVE_FROM_1"
                       onclick="BX.calendar({node: this, field: this, bTime: false});"
                       value="<?= $request->getPost($arResult['FILTER_NAME'] . '_DATE_ACTIVE_FROM_1') ?>"><span class="date-control-icon"></span>
        </label>
        &nbsp;
        <label class="news-filter__line"><span class="news-filter__text">до</span><input class="date-control filter-control" type="text" name="<?= $arResult["FILTER_NAME"] ?>_DATE_ACTIVE_FROM_2" onclick="BX.calendar
        ({node: this,
         field: this, bTime: false});"
                        value="<?= $request->getPost($arResult['FILTER_NAME'] . '_DATE_ACTIVE_FROM_2') ?>"><span class="date-control-icon"></span>
        </label>
        <div>
            <input type="submit" name="set_filter" value="<?= GetMessage("IBLOCK_SET_FILTER") ?>" class="hide"/>
        </div>


    </form>

</div>
<script>
    $(document).ready(function () {
        $('.reset-filter').on('click', function (e) {
            e.preventDefault();
            var form = $('#news-filter-form');
            //  form.append('<input type="hidden" name="del_filter" value="Y">');
            form[0].reset();
            var data = BX.ajax.prepareData(BX.ajax.prepareForm(BX('news-filter-form')).data);
            filterAjaxSend(data);
            return false;
        });

        $('.filter-control').each(function () {
            $(this).on('change', function (event) {
                var data = BX.ajax.prepareData(BX.ajax.prepareForm(BX('news-filter-form')).data);
                filterAjaxSend(data);
            });
        });

        function filterAjaxSend(data) {
            BX.ajax({
                url: '/board/ajax.php',
                data: data,
                method: 'POST',
                // dataType: 'raw',
                timeout: 30,
                async: true,
                processData: true,
                scriptsRunFirst: true,
                emulateOnload: true,
                start: true,
                cache: false,
                onsuccess: function (data) {
                    $(".news-list").html(data);
                },
                onfailure: function () {

                }
            });
        }
    });

</script>