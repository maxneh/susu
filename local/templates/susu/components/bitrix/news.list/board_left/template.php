<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<!--TPL: <?= $this->GetFolder()?>-->



    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>


        <?/* $date = FormatDate(array(
            "tommorow" => "tommorow, j F",      // выведет "завтра, 9 июля", если дата завтрашний день
            "today" => "today, j F",              // выведет "сегодня, 9 июля", если дата текущий день
            "yesterday" => "yesterday, j F",       // выведет "вчера, 9 июля", если дата прошлый день
            "d" => 'j F',                   // выведет "9 июля", если месяц прошел
            "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
        ), MakeTimeStamp($arItem["DISPLAY_ACTIVE_FROM"]), time()
        );

         if ($arParams["DISPLAY_DATE"] != "N" && $arItem["DISPLAY_ACTIVE_FROM"]): ?>
            <time class="news-item__date"><?= $date ?></time>
        <? endif */?>

        <div class="widget-item">
            <div class="widget-item__type"><?= $arItem["SECTION"]["NAME"] ?></div>
            <p class="widget-item__text"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><span><? echo $arItem["NAME"] ?></span></a></p>
        </div>
    <? endforeach; ?>

