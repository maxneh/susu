<?php
/**
 * Created by PhpStorm.
 * User: MaxNeh
 * Date: 25.04.2018
 * Time: 17:52
 */

foreach ($arResult["ITEMS"] as $key => $arItem) {
    $arFilter = Array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $arResult['IBLOCK_SECTION_ID'], 'ACTIVE' => 'Y');
    $db_list = CIBlockSection::GetList(Array("ID" => "ASC"), $arFilter, false, Array('ID', "UF_CAT_ICON", 'NAME'));
    if ($arCategory = $db_list->GetNext()) {
        $arResult["ITEMS"][$key]['SECTION'] = $arCategory;
    }
}