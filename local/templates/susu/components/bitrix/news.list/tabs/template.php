<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<!--TPL: <?= $this->GetFolder()?>-->
<div class="news-list">

    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br/>
    <? endif; ?>


    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>

        <div class="news-item news-item_<?= $key % 2 > 0 ? 'odd' : 'even' ?>">
            <div class="news-item__img">
                <span class="icon-category_<?=$arItem['IBLOCK_SECTION_ID']?>"></span>
                <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img class="preview_picture" border="0" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                                                     alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                                                     title="<?= $arItem["NAME"] ?>"/></a>
                <? endif ?>
            </div>
            <div class="news-item__info">
                <? if ($arParams["DISPLAY_DATE"] != "N" && $arItem["DISPLAY_ACTIVE_FROM"]): ?>
                    <time class="news-item__date"><? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></time>
                <? endif ?>
                <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><h4  class="news-item__title"><span><? echo $arItem["NAME"] ?></span></h4></a>
                <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
                    <div class="news-item__description"><? echo $arItem["PREVIEW_TEXT"]; ?></div>
                <?endif;?>
                <div class="news-item__socio">
                    <?= $arItem["DISPLAY_PROPERTIES"]["FORUM_MESSAGE_CNT"]["VALUE"] ?>
                </div>
            </div>


            <? if (isset($arItem["DISPLAY_PROPERTIES"]["FORUM_MESSAGE_CNT"])): ?>
                <span class="news-date-time">|&nbsp;<img src="<?= $templateFolder ?>/images/comments.gif" width="10" height="10" border="0" alt="">&nbsp;комментариев: </span>
            <? endif ?>

            <? if (isset($arItem["DISPLAY_PROPERTIES"]["rating"])): ?>
                <span class="news-date-time">|&nbsp;<img src="<?= $templateFolder ?>/images/rating.gif" width="11" height="11" border="0" alt="">&nbsp;Рейтинг: <?= $arItem["DISPLAY_PROPERTIES"]["rating"]["VALUE"] ?></span>
            <? endif ?>
        </div>
    <? endforeach; ?>

    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br/><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>

</div>
