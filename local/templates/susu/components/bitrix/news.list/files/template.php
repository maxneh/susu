<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<!--TPL: <?= $this->GetFolder()?>-->
<div class="files-list">

    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br/>
    <? endif; ?>

    <div class="files-list__row row">
        <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="files-list__col  <?= isset($arParams["NUM_COLS"]) ? "files-list__col_".$arParams["NUM_COLS"]: ''?>">
                <div class="files-item">
                    <div class="files-item__info"> <? $extension = 'doc' ?>
                        <i class="files-item__ext files-item__ext_<?= $extension ?>"></i>
                        <div class="files-item__name"><? echo $arItem["NAME"] ?></div>
                    </div>
                    <div class="files-item__buttons">
                        <a href="<?= $arItem["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]['SRC'] ?>" class="btn_blue files-item__download">Скачать</a>
                        <?$arShareParams = array(
                            "LINK_TITLE" => $arItem["NAME"],
                            "LINK_URL" => $arItem["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]['SRC'],
                            "BACK_URL" => $APPLICATION->GetCurUri(),
                        )?>
                        <? $GLOBALS["APPLICATION"]->IncludeComponent("susu:shareit", "", $arShareParams, $component);?>
                        <?$arFavorParams = array(
                            "ENTITY_TYPE_ID" => "FILES",
                            "ENTITY_ID" => $arItem["ID"],
                            "LINK_TITLE" => $arItem["NAME"],
                            "LINK_URL" => $arItem["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]['SRC'],
                        )?>
                        <? $GLOBALS["APPLICATION"]->IncludeComponent("susu:favorites", "", $arFavorParams, $component);?>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br/><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>

</div>
