<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    "NUM_COLS" => array(
        "NAME" => 'Кол-во колонок',
        "TYPE" => "STRING",
        "DEFAULT" => "3",
    ),
);
?>
