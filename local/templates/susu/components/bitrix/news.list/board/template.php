<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<!--TPL: <?= $this->GetFolder()?>-->
<div class="news-list">

    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br/>
    <? endif; ?>


    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>

        <? $date = FormatDate(array(
            "tommorow" => "tommorow, j F",      // выведет "завтра, 9 июля", если дата завтрашний день
            "today" => "today, j F",              // выведет "сегодня, 9 июля", если дата текущий день
            "yesterday" => "yesterday, j F",       // выведет "вчера, 9 июля", если дата прошлый день
            "d" => 'j F',                   // выведет "9 июля", если месяц прошел
            "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
        ), MakeTimeStamp($arItem["DISPLAY_ACTIVE_FROM"]), time()
        );

        ?>

        <div class="news-item row">
            <div class="col-xs-12 col-sm-4">

                <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                        <img class="preview_picture" border="0" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                                                     alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                                                     title="<?= $arItem["NAME"] ?>"/>
                    </a>
                <? endif ?>
            </div>

            <div class="col-xs-12 col-sm-8">
                <? if ($arParams["DISPLAY_DATE"] != "N" && $arItem["DISPLAY_ACTIVE_FROM"]): ?>
                    <time class="news-item__date"><?= $date ?></time>
                <? endif ?>
                <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><h4 class="news-item__title"><span><? echo $arItem["NAME"] ?></span></h4></a>
               <? $obParser = new CTextParser;
                if($arParams["PREVIEW_TRUNCATE_LEN"] > 0)
                $arItem["PREVIEW_TEXT"] = $obParser->html_cut($arItem["PREVIEW_TEXT"] != '' ? $arItem["PREVIEW_TEXT"]: $arItem["DETAIL_TEXT"] ,
                    $arParams["PREVIEW_TRUNCATE_LEN"]);?>
                    <div class="news-item__description"><? echo $arItem["PREVIEW_TEXT"]; ?></div>

                <div class="news-item__socio">
                    <div class="pull-left">
                        <?
                        $arRatingParams = Array(
                            "IBLOCK_ID" => $arItem["IBLOCK_ID"],
                            "IBLOCK_TYPE" => $arItem["IBLOCK_TYPE"],
                            "ELEMENT_ID" => $arItem["ID"],
                            "ENTITY_TYPE_ID" => "IBLOCK_ELEMENT",
                            "ENTITY_ID" => $arItem["ID"],
                            "PATH_TO_USER_PROFILE" => ""
                        );

                        $GLOBALS["APPLICATION"]->IncludeComponent("bitrix:rating.vote", "like", $arRatingParams, false, array("HIDE_ICONS" => "Y"));
                        ?>
                        <? $APPLICATION->IncludeComponent(
                            "susu:shareit",
                            "",
                            Array(
                                'LINK_URL' => $arItem["DETAIL_PAGE_URL"],
                                'LINK_TITLE' => $arItem["NAME"],
                            ), $component
                        ); ?>
                        <?$arFavorParams = array(
//                            "AJAX_MODE" => "Y",
                            "ENTITY_TYPE_ID" => "BOARD",
                            "ENTITY_ID" => $arItem["ID"],
                            "LINK_TITLE" => $arItem["NAME"],
                            "LINK_URL" => $arItem["DETAIL_PAGE_URL"],
                        )?>
                        <? $GLOBALS["APPLICATION"]->IncludeComponent("susu:favorites", "", $arFavorParams, $component);?>
                    </div>
                    <div class="pull-right">
                        <? if (isset($arItem["DISPLAY_PROPERTIES"]["FORUM_MESSAGE_CNT"])): ?>
                            <a href="<?=$arItem["DETAIL_PAGE_URL"] ?>#comment-anchor"  title="Комментарии"><i class="comments-icon"><?= $arItem["DISPLAY_PROPERTIES"]["FORUM_MESSAGE_CNT"]["VALUE"] ?></i></a>
                        <? elseif ($arParams["USE_REVIEW"] == "Y"): ?>
                            <a href="<?=$arItem["DETAIL_PAGE_URL"] ?>#comment-anchor" title="Комментарии"><i class="comments-icon">0</i></a>
                        <? endif ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>


        </div>
    <? endforeach; ?>

    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br/><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>

</div>
