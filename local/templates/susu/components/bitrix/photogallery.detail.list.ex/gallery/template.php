<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult["ELEMENTS_LIST"]))
	return true;


// Javascript for iblock.vote component which used for rating
// file script1.js was special renamed from script.js for prevent auto-including this file to the body of the ajax requests
if ($arParams["USE_RATING"] == "Y" && $arParams["DISPLAY_AS_RATING"] != "rating_main")
	$GLOBALS['APPLICATION']->AddHeadScript('/bitrix/components/bitrix/iblock.vote/templates/ajax_photo/script1.js');

CJSCore::Init(array('window', 'ajax', 'tooltip', 'popup'));

/********************************************************************
				Input params
********************************************************************/
// PICTURE

$temp = array("STRING" => preg_replace("/[^0-9]/is", "/", $arParams["THUMBNAIL_SIZE"]));
list($temp["WIDTH"], $temp["HEIGHT"]) = explode("/", $temp["STRING"]);
$width = (intVal($temp["WIDTH"]) > 0 ? intVal($temp["WIDTH"]) : 340);
$height = (intVal($temp["HEIGHT"]) > 0 ? intVal($temp["HEIGHT"]) : 190);



if ($arParams["PICTURES_SIGHT"] != "standart" && intVal($arParams["PICTURES"][$arParams["PICTURES_SIGHT"]]["size"]) > 0)
	$arParams["THUMBNAIL_SIZE"] = $arParams["PICTURES"][$arParams["PICTURES_SIGHT"]]["size"];

$arParams["ID"] = md5(serialize(array("default", $arParams["FILTER"], $arParams["SORTING"])));
$arParams["SHOW_RATING"] = ($arParams["SHOW_RATING"] == "N" ? "N" : "Y");
$arParams["SHOW_SHOWS"] = ($arParams["SHOW_SHOWS"] == "N" ? "N" : "Y");
$arParams["SHOW_COMMENTS"] = ($arParams["SHOW_COMMENTS"] == "N" ? "N" : "Y");
$arParams["COMMENTS_TYPE"] = (strToLower($arParams["COMMENTS_TYPE"]) == "blog" ? "blog" : "forum");
$arParams["SHOW_DATETIME"] = ($arParams["SHOW_DATETIME"] == "Y" ? "Y" : "N");
$arParams["SHOW_DESCRIPTION"] = ($arParams["SHOW_DESCRIPTION"] == "Y" ? "Y" : "N");

// PAGE
$arParams["SHOW_PAGE_NAVIGATION"] = (in_array($arParams["SHOW_PAGE_NAVIGATION"], array("none", "top", "bottom", "both")) ?
		$arParams["SHOW_PAGE_NAVIGATION"] : "bottom");
$arParams["NEW_DATE_TIME_FORMAT"] = trim(!empty($arParams["NEW_DATE_TIME_FORMAT"]) ? $arParams["NEW_DATE_TIME_FORMAT"] :
	$DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")));

$arParams["GROUP_DATE"] = ($arParams["GROUP_DATE"] == "Y" ? "Y" : "N");
/********************************************************************
				Input params
********************************************************************/

$ucid = CUtil::JSEscape($arParams["~UNIQUE_COMPONENT_ID"]);

if (!empty($arResult["ERROR_MESSAGE"])):
?>
<div class="photo-error">
	<?=ShowError($arResult["ERROR_MESSAGE"])?>
</div>
<?
endif;

if (in_array($arParams["SHOW_PAGE_NAVIGATION"], array("top", "both")) && !empty($arResult["NAV_STRING"])):
?>
<div class="photo-navigation photo-navigation-top">
	<?=$arResult["NAV_STRING"]?>
</div>
<?
endif;
$current_date = "";
?>
    <!--TPL: <?= $this->GetFolder()?>-->

<div class="photo-items-list photo-photo-list row" id="photo_list_<?= htmlspecialcharsEx($arParams["~UNIQUE_COMPONENT_ID"])?>">
<?/* Used to show 'More photos' in js*/
if($_REQUEST['get_elements_html']){ob_start();}?>
<?
foreach ($arResult["ELEMENTS_LIST"] as $key => $arItem)
{

	if ($arParams["SHOW_DATE"] == "Y")
	{
		$this_date = PhotoFormatDate($arItem["~DATE_CREATE"], "DD.MM.YYYY HH:MI:SS", "d.m.Y");
		if ($this_date != $current_date)
		{
			$current_date = $this_date;
			?><div class="group-by-days photo-date"><?= PhotoDateFormat($arParams["NEW_DATE_TIME_FORMAT"], MakeTimeStamp($this_date, "DD.MM.YYYY"))?></div><?
		}
	}

	$arItem["TITLE"] = htmlspecialcharsEx($arItem["~PREVIEW_TEXT"]);
	$alt = $arItem["TITLE"];
	if ($alt === '')
		$alt = $arItem["NAME"];

	if ($arParams['MODERATION'] == 'Y' && $arParams["PERMISSION"] >= "W")
	{
		$bNotActive = $arItem["ACTIVE"] != "Y";
		$arItem["TITLE"] .= '['.GetMessage("P_NOT_MODERATED").']';
	}

	if ($arParams["DRAG_SORT"] == "Y")
		$arItem["TITLE"] .= " - ".GetMessage("START_DRAG_TO_SORT");
	$arItem["TITLE"] = trim($arItem["TITLE"], " -");

    $image = $arItem['PROPERTIES']['REAL_PICTURE']['VALUE'];

    if (!empty($image)) {
        $tmpImage = CFile::ResizeImageGet($image, array("width" => $width, "height" => $height), BX_RESIZE_IMAGE_EXACT, false);

        $imageSrc = $tmpImage['src'];
    }


?>
   	<div id="photo_cont_<?=$arItem["ID"]?>" class="col-xs-12 col-sm-4 photo-item-cont <?if ($arParams["PERMISSION"] >= "X"){echo ' photo-item-cont-moder';}?>"
              title="<?= $arItem["TITLE"]?>">
			<a class="photo-item-inner"
               href="<?=$arItem["URL"]?>"
               id="photo_<?=$arItem["ID"]?>">
				<img src="<?= $imageSrc?>" border="0" alt="<?= $alt?>" class="img-responsive"/>
				<?if($bNotActive):?>
				<img class="bxph-warn-icon" src="/bitrix/components/bitrix/photogallery.detail.list.ex/templates/.default/images/not-approved.png" />
					<?if ($arParams["PERMISSION"] >= "X"  /* TODO : add buttons for fast approving a deleting for moderators*/):?>
						<span class="bxph-warn-link" style="top: <?= (round($arParams['THUMBNAIL_SIZE'] / 2) - 25)?>px; width: <?= $arParams['THUMBNAIL_SIZE']?>px;"><?= GetMessage("P_ACTIVATE")?></span>
						<span class="bxph-warn-link" style="top: <?= (round($arParams['THUMBNAIL_SIZE'] / 2) + 1)?>px; width: <?= $arParams['THUMBNAIL_SIZE']?>px"><?= GetMessage("P_DELETE")?></span>
					<?endif;?>
				<?endif;?>
			</a>
		</div>
<?
};

if($_REQUEST['get_elements_html']){$elementsHTML = ob_get_clean();}
?>
</div>
<div class="empty-clear"></div>

<?if ($arResult["MORE_PHOTO_NAV"] == "Y"):?>
<div id="photo-more-photo-link-cont-<?= $ucid?>" class="photo-show-more">
	<img class="show-more-wait" src="/bitrix/components/bitrix/photogallery.detail.list.ex/templates/.default/images/wait.gif" />
	<a id="photo-more-photo-link-<?= $ucid?>" href="javascript:void(0);" title="<?= GetMessage("P_SLIDER_MORE_PHOTOS_TITLE")?>"><?= GetMessage("P_SLIDER_MORE_PHOTOS")?></a>
</div>
<?endif;?>

<?
if ($_REQUEST["return_array"] == "Y" && $_REQUEST["UCID"] == $arParams["~UNIQUE_COMPONENT_ID"])
{
	$APPLICATION->RestartBuffer();
	?><script>window.bxphres = {
		items: <?= CUtil::PhpToJSObject($arResult["ELEMENTS_LIST_JS"])?>,
		currentPage: '<?= intVal($arResult["NAV_RESULT_NavPageNomer"])?>',
		itemsPageSize: '<?= intVal($arResult["NAV_RESULT_NavPageSize"])?>',
		itemsCount: '<?= intVal($arResult["ALL_ELEMENTS_CNT"])?>',
		pageCount: '<?= intVal($arResult["NAV_RESULT_NavPageCount"])?>'
	};
	<?if($_REQUEST['get_elements_html']):?>
		window.bxphres.elementsHTML = '<?= CUtil::JSEscape(trim($elementsHTML))?>';
	<?endif;?>
	</script><?
	die();
}
?>

<script>
BX.ready(function(){
	if (!top.oBXPhotoList)
	{
		top.oBXPhotoList = {};
		top.oBXPhotoSlider = {};
	}

	var pPhotoCont<?= $ucid?> = BX('photo_list_<?= $ucid?>');
	// Used for load more photos and also for drag'n'drop sorting
	top.oBXPhotoList['<?= $ucid?>'] = new window.BXPhotoList({
		uniqueId: '<?= $ucid?>',
		actionUrl: '<?= CUtil::JSEscape($arParams["ACTION_URL"])?>',
		actionPostUrl: <?= ($arParams['CHECK_ACTION_URL'] == 'Y' ? 'false' : 'true')?>,
		itemsCount: '<?= intVal($arResult["ALL_ELEMENTS_CNT"])?>',
		itemsPageSize: '<?= intVal($arResult["NAV_RESULT_NavPageSize"])?>',
		navName: 'PAGEN_<?= intVal($arResult["NAV_RESULT_NavNum"])?>',
		currentPage: '<?= intVal($arResult["NAV_RESULT_NavPageNomer"])?>',
		pageCount: '<?= intVal($arResult["NAV_RESULT_NavPageCount"])?>',
		items: <?= CUtil::PhpToJSObject($arResult["ELEMENTS_LIST_JS"])?>,
		pElementsCont: pPhotoCont<?= $ucid?>,
		initDragSorting: '<?= $arParams["DRAG_SORT"]?>',
		sortedBySort: '<?= $arParams["SORTED_BY_SORT"]?>',
		morePhotoNav: '<?= $arResult["MORE_PHOTO_NAV"]?>',
		thumbSize: '<?= $arParams["THUMBNAIL_SIZE"]?>',
		canModerate: <?= ($arParams['MODERATION'] == "Y" && $arParams["PERMISSION"] >= 'X' ? "true" : "false")?>
	});

	top.oBXPhotoSlider['<?= $ucid?>'] = new window.BXPhotoSlider({
		uniqueId: '<?= $ucid?>',
		currentItem: '<?= $arParams["CURRENT_ELEMENT_ID"]?>',
		id: '<?= $arParams["~JSID"]?>',
		userSettings: <?= CUtil::PhpToJSObject($arParams["USER_SETTINGS"])?>,
		actionUrl: '<?= CUtil::JSEscape($arParams["ACTION_URL"])?>',
		responderUrl: '/bitrix/components/bitrix/photogallery.detail.list.ex/responder.php',
		actionPostUrl: <?= ($arParams['CHECK_ACTION_URL'] == 'Y' ? 'false' : 'true')?>,
		sections: <?= CUtil::PhpToJSObject(array(array(
				"ID" => $arResult['SECTION']["ID"],
				"NAME" => $arResult['SECTION']['NAME']
			)))?>,
		items: <?= CUtil::PhpToJSObject($arResult["ELEMENTS_LIST_JS"])?>,
		itemsCount: '<?= intVal($arResult["ALL_ELEMENTS_CNT"])?>',
		itemsPageSize: '<?= intVal($arResult["NAV_RESULT_NavPageSize"])?>',
		currentPage: '<?= intVal($arResult["NAV_RESULT_NavPageNomer"])?>',
		useComments: '<?= $arParams["USE_COMMENTS"]?>',
		//useRatings: '<?//= $arParams["USE_RATING"]?>//',
		useRatings: 'N',
		showViewsCont: '<?= $arParams["SHOW_SHOWS"]?>',
		commentsCount: '<?= $arParams["COMMENTS_COUNT"]?>',
		pElementsCont: pPhotoCont<?=$ucid?>,
		reloadItemsOnload: <?= ($arResult["MIN_ID"] > 0 ? $arResult["MIN_ID"] : 'false')?>,
		itemUrl: '<?= $arParams["DETAIL_ITEM_URL"]?>',
		itemUrlHash: 'photo_<?=$arParams['SECTION_ID']?>_#ELEMENT_ID#',
		sectionUrl: '<?= $arParams["ALBUM_URL"]?>',
		permissions:
			{
				view: '<?= $arParams["PERMISSION"] >= 'R'?>',
				edit:  '<?= $arParams["PERMISSION"] >= 'U'?>',
				moderate:  '<?= $arParams["PERMISSION"] >= 'X'?>',
				viewComment: <?= $arParams["COMMENTS_PERM_VIEW"] == "Y" ? 'true' : 'false'?>,
				addComment: <?= $arParams["COMMENTS_PERM_ADD"] == "Y" ? 'true' : 'false'?>
			},
		userUrl: '<?= $arParams["PATH_TO_USER"]?>',
		showTooltipOnUser: 'N',
		showSourceLink: 'Y',
		moderation: '<?= $arParams['MODERATION']?>',
		commentsType: '<?= $arParams["COMMENTS_TYPE"]?>',
		cacheRaitingReq: <?= ($arParams["DISPLAY_AS_RATING"] == "rating_main" ? 'true' : 'false')?>,
		sign: '<?= $arResult["SIGN"]?>',
		reqParams: <?= CUtil::PhpToJSObject($arResult["REQ_PARAMS"])?>,
		checkParams: <?= CUtil::PhpToJSObject($arResult["CHECK_PARAMS"])?>,
		MESS: {
			from: '<?= GetMessageJS("P_SLIDER_FROM")?>',
			slider: '<?= GetMessageJS("P_SLIDER_SLIDER")?>',
			slideshow: '<?= GetMessageJS("P_SLIDER_SLIDESHOW")?>',
			slideshowTitle: '<?= GetMessageJS("P_SLIDER_SLIDESHOW_TITLE")?>',
			addDesc: '<?= GetMessageJS("P_SLIDER_ADD_DESC")?>',
			addComment: '<?= GetMessageJS("P_SLIDER_ADD_COMMENT")?>',
			commentTitle: '<?= GetMessageJS("P_SLIDER_COMMENT_TITLE")?>',
			save: '<?= GetMessageJS("P_SLIDER_SAVE")?>',
			cancel: '<?= GetMessageJS("P_SLIDER_CANCEL")?>',
			commentsCount: '<?= GetMessageJS("P_SLIDER_COM_COUNT")?>',
			moreCom: '<?= GetMessageJS("P_SLIDER_MORE_COM")?>',
			moreCom2: '<?= GetMessageJS("P_SLIDER_MORE_COM2")?>',
			album: '<?= GetMessageJS("P_SLIDER_ALBUM")?>',
			author: '<?= GetMessageJS("P_SLIDER_AUTHOR")?>',
			added: '<?= GetMessageJS("P_SLIDER_ADDED")?>',
			edit: '<?= GetMessageJS("P_SLIDER_EDIT")?>',
			del: '<?= GetMessageJS("P_SLIDER_DEL")?>',
			bigPhoto: '<?= GetMessageJS("P_SLIDER_BIG_PHOTO")?>',
			smallPhoto: '<?= GetMessageJS("P_SLIDER_SMALL_PHOTO")?>',
			rotate: '<?= GetMessageJS("P_SLIDER_ROTATE")?>',
			saveDetailTitle: '<?= GetMessageJS("P_SLIDER_SAVE_DETAIL_TITLE")?>',
			DarkBG: '<?= GetMessageJS("P_SLIDER_DARK_BG")?>',
			LightBG: '<?= GetMessageJS("P_SLIDER_LIGHT_BG")?>',
			delItemConfirm: '<?= GetMessageJS("P_DELETE_ITEM_CONFIRM")?>',
			shortComError: '<?= GetMessageJS("P_SHORT_COMMENT_ERROR")?>',
			photoEditDialogTitle: '<?= GetMessageJS("P_EDIT_DIALOG_TITLE")?>',
			unknownError: '<?= GetMessageJS("P_UNKNOWN_ERROR")?>',
			sourceImage: '<?= GetMessageJS("P_SOURCE_IMAGE")?>',
			created: '<?= GetMessageJS("P_CREATED")?>',
			tags: '<?= GetMessageJS("P_SLIDER_TAGS")?>',
			clickToClose: '<?= GetMessageJS("P_SLIDER_CLICK_TO_CLOSE")?>',
			comAccessDenied: '<?= GetMessageJS("P_SLIDER_COMMENTS_ACCESS_DENIED")?>',
			views: '<?= GetMessageJS("P_SLIDER_VIEWS")?>',
			notModerated: '<?= GetMessageJS("P_NOT_MODERATED")?>',
			activateNow: '<?= GetMessageJS("P_ACTIVATE")?>',
			deleteNow: '<?= GetMessageJS("P_DELETE")?>',
			bigPhotoDisabled: '<?= GetMessageJS("P_SLIDER_BIG_PHOTO_DIS")?>'
		}
	});
});
</script>
    <h2 class="blue-text">Другие альбомы</h2>
<?
global $arrFilterMoreAlb;
$arrFilterMoreAlb = Array(
    "!ID" => $arResult["SECTION"]['ID'],
    'SECTION_ID'=>$arResult["SECTION"]['IBLOCK_SECTION_ID']
);?>


<?$APPLICATION->IncludeComponent(
    "susu:photogallery.section.list",
    "morealbums",
    Array(
        "FILTER_NAME" => "arrFilterMoreAlb",
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "PHOTO_LIST_MODE" => $arParams["PHOTO_LIST_MODE"],
        "SHOWN_ITEMS_COUNT" => 1,
        'MAX_SECTIONS_CNT' => 3,

        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
        "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
        "DATE_TIME_FORMAT" => $arParams["DATE_TIME_FORMAT_SECTION"],

        "BEHAVIOUR" => (isset($arParams["BEHAVIOUR"]) && $arParams["BEHAVIOUR"] != "") ? $arParams["BEHAVIOUR"] : "SIMPLE",
        "USER_ALIAS" => $arParams["USER_ALIAS"],

        "PAGE_NAVIGATION_TEMPLATE" => $arParams["PAGE_NAVIGATION_TEMPLATE"],
        "PAGE_ELEMENTS" => $arParams["SECTION_PAGE_ELEMENTS"],
        "SORT_BY" => 'UF_DATE',
        "SORT_ORD" => 'DESC',

        "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
        "SHOW_TAGS" => $arParams["SHOW_TAGS"],

//        "SECTION_URL" => $arResult["URL_TEMPLATES"]["section"],
        "SECTION_URL" => $arParams['SECTION_URL'],
        "SECTION_EDIT_URL" => $arResult["URL_TEMPLATES"]["section_edit"],
        "SECTION_EDIT_ICON_URL" => $arResult["URL_TEMPLATES"]["section_edit_icon"],
        "DETAIL_URL" => $arResult["URL_TEMPLATES"]["detail"],
        "SEARCH_URL" => $arResult["URL_TEMPLATES"]["search"],
        "UPLOAD_URL" => $arResult["URL_TEMPLATES"]["upload"],
        "DETAIL_EDIT_URL" => $arResult["URL_TEMPLATES"]["detail_edit"],

        "ALBUM_PHOTO_THUMBS_SIZE" => $arParams["ALBUM_PHOTO_THUMBS_SIZE"],
        "ALBUM_PHOTO_SIZE" => $arParams["ALBUM_PHOTO_SIZE"],
        "SECTION_LIST_THUMBNAIL_SIZE" => $arParams["SECTION_LIST_THUMBNAIL_SIZE"],
        "SET_STATUS_404" => $arParams["SET_STATUS_404"],

        "SET_TITLE"	=>	"N",
        "SHOW_RATING" => $arParams["USE_RATING"],
        "SHOW_SHOWS" => $arParams["SHOW_SHOWS"],
        "SHOW_COMMENTS" => $arParams["USE_COMMENTS"],
        "SHOW_DATE" => $arParams["SHOW_DATE"],
        "SHOW_DESRIPTION" => $arParams["SHOW_DESRIPTION"],

        "USE_FILTER" => 'N',
        "USE_RATING" => $arParams["USE_RATING"],
        "MAX_VOTE" => $arParams["MAX_VOTE"],
        "VOTE_NAMES" => $arParams["VOTE_NAMES"],
        "DISPLAY_AS_RATING" => $arParams["DISPLAY_AS_RATING"],
        "RATING_MAIN_TYPE" => $arParams["RATING_MAIN_TYPE"],

        "USE_COMMENTS" => $arParams["USE_COMMENTS"],
        "COMMENTS_TYPE" => $arParams["COMMENTS_TYPE"],

        "COMMENTS_COUNT" => $arParams["COMMENTS_COUNT"],
        "PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
        "FORUM_ID" => $arParams["FORUM_ID"],
        "USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
        "URL_TEMPLATES_READ" => $arParams["URL_TEMPLATES_READ"],
        "URL_TEMPLATES_PROFILE_VIEW" => $arParams["URL_TEMPLATES_PROFILE_VIEW"],
        "POST_FIRST_MESSAGE" => $arParams["POST_FIRST_MESSAGE"],
        "PREORDER" => $arParams["PREORDER"],
        "SHOW_LINK_TO_FORUM" => $arParams["SHOW_LINK_TO_FORUM"] == "Y" ? "Y" : "N",

        "BLOG_URL" => $arParams["BLOG_URL"],
        "PATH_TO_BLOG" => $arParams["PATH_TO_BLOG"],

        "PATH_TO_USER" => $arParams["PATH_TO_USER"],
        "NAME_TEMPLATE" => $arParams["NAME_TEMPLATE"],
        "SHOW_LOGIN" => $arParams["SHOW_LOGIN"],

        "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
        "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
    ),
    $component,
    array("HIDE_ICONS" => "Y")
)
?>