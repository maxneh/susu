<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if ($arResult["NEED_AUTH"] == "Y") {
    $APPLICATION->AuthForm("");
} elseif (strlen($arResult["FatalError"]) > 0) {
    ?><span class='errortext'><?= $arResult["FatalError"] ?></span><br/><br/><?
} else {
    if (strlen($arResult["ErrorMessage"]) > 0) {
        ?><span class='errortext'><?= $arResult["ErrorMessage"] ?></span><br/><br/><?
    }

    ?>
    <div class="clearfix">

                <div class="socialnetwork-profile__avatar ">
                    <?=$arResult["User"]["PersonalPhotoImg"]?>
                </div>
                <div class="socialnetwork-profile__name"><h3><?= $arResult["User"]["NAME_FORMATTED"] ?></h3></div>

            </div>

    <? } ?>