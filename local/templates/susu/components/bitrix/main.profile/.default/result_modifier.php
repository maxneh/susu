<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


CModule::IncludeModule("iblock");
$arSelect = Array("ID", "NAME", 'IBLOCK_SECTION_ID');
$arFilter = Array("IBLOCK_ID" => 15, "ID"=>$arResult["arUser"]["UF_USER_DEPARTMENT"], "ACTIVE" => "Y");

$rsSect = CIBlockSection::GetList(array('ID' => 'ASC'), $arFilter, false, $arSelect);
while ($arSect = $rsSect->GetNext()) {
    $arResult["arUser"]["UF_USER_DEPARTMENT_NAME"] =$arSect["NAME"] ;
    if ($arSect["IBLOCK_SECTION_ID"]) {
        $arSelect = Array("ID", "NAME", 'SECTION_ID');
        $arFilter = Array("IBLOCK_ID" => 15, "ID"=>$arSect["IBLOCK_SECTION_ID"], "ACTIVE" => "Y");
        $rsSect = CIBlockSection::GetList(array('ID' => 'ASC'), $arFilter, false, $arSelect);
        while ($arSect = $rsSect->GetNext()) {
            $arResult["arUser"]["PARENT_DEPARTMENT_NAME"] =$arSect["NAME"] ;

        }
    }
}