<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>
<!--TPL: <?= $this->GetFolder()?>-->
<div class="bx-auth-profile">

    <? ShowError($arResult["strProfileError"]); ?>
    <?
    if ($arResult['DATA_SAVED'] == 'Y')
        ShowNote(GetMessage('PROFILE_DATA_SAVED'));
    ?>
    <?$class = $_REQUEST['edit'] == 'Y' ? 'hide' : ''?>
    <div id="user-show-fields"  class="<?=$class?>">
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <?= $arResult["arUser"]["PERSONAL_PHOTO_HTML"] ?>
            </div>
            <div class="col-xs-12 col-sm-9">
                <h3 class="user-name"><?= $arResult["arUser"]["NAME"] ?> <? echo $arResult["arUser"]["SECOND_NAME"] ?> <? echo $arResult["arUser"]["LAST_NAME"] ?>
                    <a href="#" class="toggle-edit"><span class="user-edit-icon icon icon-pencil"></span></a></h3>

                <div class="row form-group">
                    <div class="col-xs-5 col-sm-3"><label>Подразделение</label></div>
                    <div class="col-xs-7 col-sm-9"><? echo $arResult["arUser"]["PARENT_DEPARTMENT_NAME"] ?></div>
                </div>
                <div class="row form-group">
                    <div class="col-xs-5 col-sm-3"><label>Кафедра</label></div>
                    <div class="col-xs-7 col-sm-9"><? echo $arResult["arUser"]["UF_USER_DEPARTMENT_NAME"] ?></div>
                </div>
                <div class="row form-group">
                    <div class="col-xs-5 col-sm-3"><label>Должность</label></div>
                    <div class="col-xs-7 col-sm-9"><? echo $arResult["arUser"]["WORK_POSITION"] ?></div>
                </div>
                <div class="row form-group">
                    <div class="col-xs-5 col-sm-3"><label>Эл. почта</label></div>
                    <div class="col-xs-7 col-sm-9"><? echo $arResult["arUser"]["EMAIL"] ?></div>
                </div>
                <div class="row form-group">
                    <div class="col-xs-5 col-sm-3"><label>Мобильный</label></div>
                    <div class="col-xs-7 col-sm-9"><? echo $arResult["arUser"]["PERSONAL_MOBILE"] ?></div>
                </div>
                <div class="row form-group">
                    <div class="col-xs-5 col-sm-3"><label>День рождения</label></div>
                    <div class="col-xs-7 col-sm-9"><? echo $arResult["arUser"]["PERSONAL_BIRTHDAY"] ?></div>
                </div>
            </div>
        </div>

    </div>
    <?$class = $_REQUEST['edit'] == 'Y' ? '': 'hide'?>
    <div id="user-edit-fields" class="<?=$class?>">
        <form method="post" name="form1" action="<?= $arResult["FORM_TARGET"] ?>" enctype="multipart/form-data">
            <?= $arResult["BX_SESSION_CHECK"] ?>
            <input type="hidden" name="lang" value="<?= LANG ?>"/>
            <input type="hidden" name="ID" value=<?= $arResult["ID"] ?>/>

            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <? if (strlen($arResult["arUser"]["PERSONAL_PHOTO"]) > 0) { ?>
                        <br/>
                        <?= $arResult["arUser"]["PERSONAL_PHOTO_HTML"] ?>
                    <? } ?>
                    <?= $arResult["arUser"]["PERSONAL_PHOTO_INPUT"] ?>

                </div>
                <div class="col-xs-12 col-sm-9">


                    <div id="user_div_reg">

                        <div class="row form-group">
                            <div class="col-xs-5 col-sm-3"><label><?= GetMessage('NAME') ?></label></div>
                            <div class="col-xs-7 col-sm-9"><input type="text" name="NAME" maxlength="50" value="<?= $arResult["arUser"]["NAME"] ?>"/></div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-5 col-sm-3"><label><?= GetMessage('SECOND_NAME') ?></label></div>
                            <div class="col-xs-7 col-sm-9"><input type="text" name="SECOND_NAME" maxlength="50"
                                                      value="<?= $arResult["arUser"]["SECOND_NAME"] ?>"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-5 col-sm-3"><label><?= GetMessage('LAST_NAME') ?></label></div>
                            <div class="col-xs-7 col-sm-9"><input type="text" name="LAST_NAME" maxlength="50" value="<?= $arResult["arUser"]["LAST_NAME"] ?>"/>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-xs-5 col-sm-3"><label><?= GetMessage('EMAIL') ?><? if ($arResult["EMAIL_REQUIRED"]) { ?><span
                                            class="starrequired">*</span><? } ?></label></div>
                            <div class="col-xs-7 col-sm-9"><input type="text" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"] ?>"/></div>
                        </div>

                    </div>
                    <div id="user_div_personal">

                        <!--<div class="row form-group">
                        <div class="col-xs-5 col-sm-3"><label><?= GetMessage('USER_PROFESSION') ?></label></div>
                        <div class="col-xs-7 col-sm-9"><input type="text" name="PERSONAL_PROFESSION" maxlength="255"
                                    value="<?= $arResult["arUser"]["PERSONAL_PROFESSION"] ?>"/></div>
                    </div>
                   <div class="row form-group">
                        <div class="col-xs-5 col-sm-3"><label><?= GetMessage('USER_GENDER') ?></label></div>
                        <div class="col-xs-7 col-sm-9">
                            <select name="PERSONAL_GENDER">
                                <option value=""><?= GetMessage("USER_DONT_KNOW") ?></option>
                                <option value="M"<?= $arResult["arUser"]["PERSONAL_GENDER"] == "M" ? " SELECTED=\"SELECTED\"" : "" ?>><?= GetMessage("USER_MALE") ?></option>
                                <option value="F"<?= $arResult["arUser"]["PERSONAL_GENDER"] == "F" ? " SELECTED=\"SELECTED\"" : "" ?>>?= GetMessage("USER_FEMALE") ?></option>
                            </select>
                        </div>
                    </div>-->
                        <div class="row form-group">
                            <div class="col-xs-5 col-sm-3"><label><?= GetMessage("USER_BIRTHDAY_DT") ?> (<?= $arResult["DATE_FORMAT"] ?>):</label></div>
                            <div class="col-xs-7 col-sm-9"><?
                                $APPLICATION->IncludeComponent(
                                    'bitrix:main.calendar',
                                    '',
                                    array(
                                        'SHOW_INPUT' => 'Y',
                                        'FORM_NAME' => 'form1',
                                        'INPUT_NAME' => 'PERSONAL_BIRTHDAY',
                                        'INPUT_VALUE' => $arResult["arUser"]["PERSONAL_BIRTHDAY"],
                                        'SHOW_TIME' => 'N'
                                    ),
                                    null,
                                    array('HIDE_ICONS' => 'Y')
                                );

                                //=CalendarDate("PERSONAL_BIRTHDAY", $arResult["arUser"]["PERSONAL_BIRTHDAY"], "form1", "15")
                                ?>
                            </div>
                        </div>
                        <!-- <div class="row form-group">
                        <div class="col-xs-5 col-sm-3"><label><?= GetMessage('USER_PHONE') ?></label></div>
                        <div class="col-xs-7 col-sm-9"><input type="text" name="PERSONAL_PHONE" maxlength="255" value="<?= $arResult["arUser"]["PERSONAL_PHONE"] ?>"/></div>
                    </div>
                   -->
                        <div class="row form-group">
                            <div class="col-xs-5 col-sm-3"><label><?= GetMessage('USER_MOBILE') ?></label></div>
                            <div class="col-xs-7 col-sm-9"><input type="text" name="PERSONAL_MOBILE" maxlength="255"
                                                      value="<?= $arResult["arUser"]["PERSONAL_MOBILE"] ?>"/>
                            </div>
                        </div>
                    </div>
                    <div id="user_div_work">
                        <!--<div class="row form-group">
                        <div class="col-xs-5 col-sm-3"><label><?= GetMessage('USER_DEPARTMENT') ?></label></div>
                        <div class="col-xs-7 col-sm-9"><input type="text" name="WORK_DEPARTMENT" maxlength="255" value="<?= $arResult["arUser"]["WORK_DEPARTMENT"] ?>"/></div>
                    </div>-->
                        <div class="row form-group">
                            <div class="col-xs-5 col-sm-3"><label><?= GetMessage('USER_POSITION') ?></label></div>
                            <div class="col-xs-7 col-sm-9"><input type="text" name="WORK_POSITION" maxlength="255"
                                                      value="<?= $arResult["arUser"]["WORK_POSITION"] ?>"/></div>
                        </div>
                        <!--<div>
                        <div class="col-xs-5 col-sm-3"><label><?= GetMessage("USER_WORK_PROFILE") ?></label></div>
                        <div><textarea cols="30" row form-groups="5" name="WORK_PROFILE"><?= $arResult["arUser"]["WORK_PROFILE"] ?></textarea></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-5 col-sm-3"><label><?= GetMessage('USER_PHONE') ?></label></div>
                        <div class="col-xs-7 col-sm-9"><input type="text" name="WORK_PHONE" maxlength="255" value="<?= $arResult["arUser"]["WORK_PHONE"] ?>"/>
                        </div>
                    </div>-->

                    </div>


                    <? // ********************* User properties ***************************************************?>
                    <? if ($arResult["USER_PROPERTIES"]["SHOW"] == "Y") { ?>

                        <div id="user_div_user_properties">

                            <? $first = true; ?>
                            <? foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField) { ?>
                                <div class="row form-group">
                                    <div class="field-name col-xs-5 col-sm-3">
                                        <label><? if ($arUserField["MANDATORY"] == "Y") { ?>
                                                <span class="starrequired">*</span>
                                            <? } ?>
                                            <?= $arUserField["EDIT_FORM_LABEL"] ?>:</label>
                                    </div>
                                    <div class="field-value col-xs-7 col-sm-9">
                                        <? $APPLICATION->IncludeComponent(
                                            "bitrix:system.field.edit",
                                            $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                            array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), null, array("HIDE_ICONS" => "Y")); ?></div>
                                </div>
                            <? } ?>

                        </div>
                    <? } ?>
                    <? // ******************** /User properties ***************************************************?>
                    <div class="row form-group">
                        <div class="col-xs-5 col-sm-3"><label><?= GetMessage('LOGIN') ?><span class="starrequired">*</span></label></div>
                        <div class="col-xs-7 col-sm-9"><input type="text" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"] ?>"/></div>
                    </div>

                    <? if ($arResult["arUser"]["EXTERNAL_AUTH_ID"] == '') { ?>
                        <div class="row form-group">
                            <div class="col-xs-5 col-sm-3"><label><?= GetMessage('NEW_PASSWORD_REQ') ?></label></div>
                            <div class="col-xs-7 col-sm-9">
                                <input type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" class="bx-auth-input"/>
                                <? if ($arResult["SECURE_AUTH"]) { ?>
                                    <span class="bx-auth-secure" id="bx_auth_secure" title="<? echo GetMessage("AUTH_SECURE_NOTE") ?>"
                                          style="display:none"><span class="bx-auth-secure-icon"></span></span>
                                    <noscript>
                                <span class="bx-auth-secure" title="<? echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
                                    <span class="bx-auth-secure-icon bx-auth-secure-unlock"></span>
                                </span>
                                    </noscript>
                                    <script type="text/javascript">
                                        document.getElementById('bx_auth_secure').style.display = 'inline-block';
                                    </script>
                                <? } ?>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-5 col-sm-3"><label><?= GetMessage('NEW_PASSWORD_CONFIRM') ?></label></div>
                            <div class="col-xs-7 col-sm-9"><input type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off"/></div>
                        </div>
                        <p><? echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]; ?></p>
                    <? } ?>
                    <div class="form-group">
                        <input type="submit" class="btn_blue" name="save"
                               value="<?= (($arResult["ID"] > 0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))
                               ?>">&nbsp;
                        &nbsp;<input type="reset" class="btn_white" value="<?= GetMessage('MAIN_RESET'); ?>">
                    </div>
                </div>
            </div>
        </form>

        <?
        if ($arResult["SOCSERV_ENABLED"]) {
            $APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
                "SHOW_PROFILES" => "Y",
                "ALLOW_DELETE" => "Y"
            ),
                false
            );
        }
        ?>

    </div>
</div>
<script>
    BX.ready(function () {
        BX.bindDelegate(
            document.body, 'click', {className: 'toggle-edit'},
            function (e) {
                if (!e) {
                    e = window.event;
                }
                BX.toggleClass(BX("user-edit-fields"), ["show", "hide"]);
                BX.toggleClass(BX("user-show-fields"), ["hide", "show"]);
                return BX.PreventDefault(e);
            }
        );
    });
</script>