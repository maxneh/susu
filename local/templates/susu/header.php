<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
use Bitrix\Main\Page\Asset;
?>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?  Asset::getInstance()->addCss("https://fonts.googleapis.com/css?family=Roboto+Slab&amp;subset=cyrillic");?>
    <?  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/baron.css");?>
    <?  Asset::getInstance()->addJs("/bitrix/js/main/jquery/jquery-2.1.3.min.js");?>
    <?  Asset::getInstance()->addJs("/bitrix/js/main/jquery/jquery.touchSwipe.js");?>
    <?//  Asset::getInstance()->addJs("https://code.jquery.com/ui/1.12.1/jquery-ui.js");?>

    <? $APPLICATION->ShowHead(); ?>
    <title><? $APPLICATION->ShowTitle() ?></title>

</head>

<body>

<? $APPLICATION->ShowPanel() ?>



<?  $detect = new Mobile_Detect();
$isMobile = $detect->isMobile()?>

<div class="main-wrapper <?= $isMobile ? 'short-menu': ''?>">
    <? if ($USER->IsAuthorized()) {?>
    <div class="header">
        <button class="toggle-left-menu" title="Развернуть/свернуть меню"><i class="left-menu-icon"></i></button>
        <div class="page-header">
            <span class="page-header__title"><? $APPLICATION->ShowTitle() ?></span>
            <? if($icon = $APPLICATION->GetCurPage() != '/' ? $APPLICATION->GetDirProperty('ACTION_ICON') : 'filter-icon') { ?>
                <button class='page-action__toggle' title='Развернуть/свернуть'><i class='page-action__icon <?= $icon ?>'></i></button>
            <? }  ?>


        </div>

        <div class="header__right">
            <? $APPLICATION->IncludeComponent(
                "bitrix:search.form",
                "flat",
                array(
                    "PAGE" => "/search/",
                    "COMPONENT_TEMPLATE" => "flat",
                    "USE_SUGGEST" => "N"
                ),
                false
            ); ?>
            <a href="javascript:void(0)" id="popup-honor-link" class="header__link" title="Поблагодарить"><i class="honor-icon icon
            icon-heart"></i></a>
            <a href="javascript:void(0)" id="popup-idea-link" class="header__link" title="Отправить идею"><i class="idea-icon icon icon-idea"></i></a>
            <div class="header__login">
                <? $APPLICATION->IncludeComponent("bitrix:system.auth.form", "auth", Array(
                        "REGISTER_URL" => "/auth/",
                        "PROFILE_URL" => "/personal/"
                    )
                ); ?>
            </div>
        </div>
    </div>
    <div class="left-panel">
        <a href="/" class="logo">Главная</a>
        <div class="widget widget_menu"><? $APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
                    "ROOT_MENU_TYPE" => "top",
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "top",
                    "USE_EXT" => "Y",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(
                        0 => "SECTION_ID",
                        1 => "page",
                    ),
                )
            ); ?>
        </div>
        <div class="widget widget_board">
            <a href="/board/"><h4 class="widget__title"><i class="widget__icon icon icon-ads"></i><span>Объявления</span></h4></a>
            <? $APPLICATION->IncludeComponent('bitrix:news.list',
                'board_left',
                array(
                    "IBLOCK_ID" => "12",
                    "IBLOCK_TYPE" => "services",
                    "NEWS_COUNT" => "2",
                    'SORT_BY1'=> 'ACTIVE_FROM',
                    'INCLUDE_IBLOCK_INTO_CHAIN'=> 'N',
                    'ADD_SECTIONS_CHAIN'=> 'N',
                    'SORT_ORDER1'=> 'DESC',
                    "DETAIL_URL" => '/board/#SECTION_ID#/#ELEMENT_ID#/'
                ),
                false );
            ?>
       <!--     <div class="widget-item">
                <div class="widget-item__type">Нужен специалист</div>
                <p class="widget-item__text">Нужен маркетолог в проект по энергетике</p>
            </div>
            <div class="widget-item">
                <div class="widget-item__type">Прочее</div>
                <p class="widget-item__text">Продам а/м Хонда Аккорд 2007 г. Цена: 800 000 руб.</p>
            </div>-->
        </div>
        <div class="widget widget_calendar">
            <a href="/kalendar/"><h4 class="widget__title"><i class="widget__icon icon icon-calendar"></i><span>Календарь событий</span></h4></a>
            <? $APPLICATION->IncludeComponent(
                "susu:calendar.events.list",
                "",
                Array(
                    "CALENDAR_TYPE" => array("events",'user'),
                    "INIT_DATE" => "--текущая дата--",
                    "FUTURE_MONTH_COUNT" => "2",
                    "DETAIL_URL" => "/kalendar/",
                    "EVENTS_COUNT" => "2",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600",
                    "CACHE_NOTES" => ""
                )
            ); ?>
        </div>
        <div id="im-left-wrap"></div>
        <? $APPLICATION->IncludeComponent(
            "susu:birthdays.friends",
            "",
            Array(
                "DAYS_AFTER" => "60",
                "FORMAT" => "j F",
                "TITLE" => "Дни рождения друзей",
                "TITLE_LINK" => "/friends-birthdays/",
                "COUNT_ITEMS" => 2,
                "THUMBNAIL_LIST_SIZE" => 42,
                "PATH_TO_USER" => '/club/user/#user_id#/',
                "NAME_TEMPLATE" => "#NAME# #LAST_NAME#",
            )
        ); ?>

        <?/*if ($isMobile) {
            $APPLICATION->IncludeComponent("bitrix:im.messenger", "right-panel", Array(),false,array("HIDE_ICONS"=>"Y"));
        }*/?>

    </div>
    <main class="content">
        <div class="baron__scroller">

            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "tabs",
                array(
                    "ROOT_MENU_TYPE" => "left",
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "Y",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(
                        0 => "SECTION_ID",
                        1 => "page",
                        2 => "",
                    ),
                    "COMPONENT_TEMPLATE" => "tabs",
                    "DELAY" => "Y",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            ); ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                ".default",
                Array(
                    "START_FROM" => "1",
                    "PATH" => "",
                    "SITE_ID" => SITE_ID
                )
            ); ?>
            <br>
<? } else {?>
    <div class="login-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                    <div class="login-page__logo">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/login-logo.png" alt="ЮУрГУ логотип">
                    </div>
                    <div class="login-page__form-wrap">
                        <p class="login-page__title">Вход</p>

<?}?>