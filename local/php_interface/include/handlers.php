<?php
/**
 * Created by PhpStorm.
 * User: MaxNeh
 * Date: 14.02.2018
 * Time: 14:17
 */

use \Bitrix\Main\Loader;

$eventManager = \Bitrix\Main\EventManager::getInstance();

//page start
AddEventHandler("main", "OnPageStart", "loadLocalLib", 1);
function loadLocalLib()
{
    Loader::includeModule('local.lib');
}

AddEventHandler('form', 'onAfterResultAdd', array('Local\Lib\Handlers\Form', 'onAfterResultAdd'));
AddEventHandler('form', 'onAfterResultStatusChange', array('Local\Lib\Handlers\Form', 'onAfterResultStatusChange'));

AddEventHandler('main', 'OnAfterUserAdd', array('Local\Lib\Handlers\User', 'OnAfterUserAddHandler'));
AddEventHandler('main', 'OnAfterUserUpdate', array('Local\Lib\Handlers\User', 'OnAfterUserUpdateHandler'));
