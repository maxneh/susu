<?php
/**
 * Created by PhpStorm.
 * User: MaxNeh
 * Date: 14.02.2018
 * Time: 14:17
 */

require_once($_SERVER["DOCUMENT_ROOT"].'/Mobile_Detect.php');

/**
 * IBLOCK IDs
 */

define('IBLOCK_ID__NEWS', 3);
define('IBLOCK_ID__STRUCTURE', 15);
define('IBLOCK_ID__GALLERY', 5);
define('IBLOCK_ID__GALLERY_USER', 6);
define('IBLOCK_ID__GALLERY_GROUP', 18);

/**
 * Highload Ids
 */
define('HLBLOCK_ID__GALLERY_CAT', 1);
define('HLBLOCK_ID__GALLERY_CAT_USER', 2);
define('HLBLOCK_ID__FAVORITES', 3);


 /**
  * Forms
  */
 define('WEB_FORM_ID__SERVICE', 3);
 define('WEB_FORM_ID__IDEA', 4);
 define('WEB_FORM_ID__HONOR', 5);