<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if (!\Bitrix\Main\Context::getCurrent()->getRequest()->isAjaxRequest()) die;
?>



<?$APPLICATION->IncludeComponent(
	"bitrix:socialnetwork.user_search", 
	"ajax",
	array(
		"ALLOW_RATING_SORT" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"DATE_TIME_FORMAT" => "d.m.Y H:i:s",
		"ITEMS_COUNT" => "",
		"NAME_TEMPLATE" => "",
		"PAGE_VAR" => "",
		"PATH_TO_MESSAGES_CHAT" => "",
		"PATH_TO_MESSAGE_FORM" => "",
		"PATH_TO_SEARCH" => "",
		"PATH_TO_SEARCH_INNER" => "",
		"PATH_TO_USER" => "/club/user/#user_id#/",
		"PATH_TO_USER_FRIENDS_ADD" => "",
		"RATING_ID" => "3",
		"RATING_TYPE" => "like",
		"SET_NAV_CHAIN" => "N",
		"SET_TITLE" => "N",
		"SHOW_RATING" => "N",
		"SHOW_USERS_WITHOUT_FILTER_SET" => "Y",
		"SHOW_YEAR" => "N",
		"USER_FIELDS_LIST" => array(
			0 => "EMAIL",
			1 => "PERSONAL_PHONE",
			2 => "PERSONAL_MOBILE",
			3 => "WORK_POSITION",
		),
		"USER_FIELDS_SEARCHABLE" => array(
			0 => "LOGIN",
			1 => "NAME",
			2 => "SECOND_NAME",
			3 => "LAST_NAME",
			4 => "PERSONAL_BIRTHDAY",
			5 => "PERSONAL_PROFESSION",
			6 => "PERSONAL_GENDER",
			7 => "PERSONAL_COUNTRY",
			8 => "PERSONAL_STATE",
			9 => "PERSONAL_CITY",
			10 => "PERSONAL_ZIP",
			11 => "PERSONAL_STREET",
			12 => "PERSONAL_MAILBOX",
			13 => "WORK_COMPANY",
			14 => "WORK_DEPARTMENT",
			15 => "WORK_POSITION",
			16 => "WORK_COUNTRY",
			17 => "WORK_STATE",
			18 => "WORK_CITY",
			19 => "WORK_ZIP",
			20 => "WORK_STREET",
			21 => "WORK_MAILBOX",
		),
		"USER_FIELDS_SEARCH_ADV" => array(
		),
		"USER_FIELDS_SEARCH_SIMPLE" => array(
			0 => "NAME",
			1 => "SECOND_NAME",
			2 => "LAST_NAME",
		),
		"USER_PROPERTIES_LIST" => array(
			0 => "UF_USER_DEPARTMENT",
		),
		"USER_PROPERTIES_SEARCH_ADV" => array(
		),
		"USER_PROPERTIES_SEARCH_SIMPLE" => array(
			0 => "UF_USER_DEPARTMENT",
		),
		"USER_PROPERTY_SEARCHABLE" => array(
			0 => "UF_USER_DEPARTMENT",
		),
		"USER_VAR" => "",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
