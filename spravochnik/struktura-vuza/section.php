<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Справочник");
$APPLICATION->ShowNavChain();
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"structure",
	array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_GROUPS" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COUNT_ELEMENTS" => "N",
		"IBLOCK_ID" => "15",
		"IBLOCK_TYPE" => "guide",
		"SECTION_CODE" => array_pop(explode('/', $_REQUEST['SECTION_PATH'])),
		"SECTION_FIELDS" => array(
			0 => "",
			1 => "",
		),
//		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "3",
		"VIEW_MODE" => "TEXT",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>