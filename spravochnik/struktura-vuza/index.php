<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Справочник");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>


    <div class="row">
        <div class="col-xs-12 col-md-4">
            <h2 class="blue-text fz36">Ректорат</h2>
            <?
            $filter = Array(
                "UF_REKTORAT" => 1,
                "ACTIVE" => "Y",
            );
            $rsUsers = CUser::GetList(($by = "active"), ($order = "desc"), $filter); // выбираем пользователей
            // $is_filtered = $rsUsers->is_filtered; // отфильтрована ли выборка ?
            //  $rsUsers->NavStart(50); // разбиваем постранично по 50 записей
            //  echo $rsUsers->NavPrint(GetMessage("PAGES")); // печатаем постраничную навигацию
            while ($arUser = $rsUsers->GetNext()) {
                ?>
                <p class="rektorat">
                    <a href="/club/user/<?= $arUser['ID'] ?>/">
                        <span class="rektorat__link"><?= $arUser['NAME'] ?> <?= $arUser['SECOND_NAME'] ?> <?= $arUser['LAST_NAME'] ?></span>
                    </a>
                    <br>
                    <i class="fw400"><?= $arUser['WORK_POSITION'] ?></i>
                </p>

            <? } ?>
        </div>
        <? $arSections = \Local\Lib\Content\Section::getTreeArr(
            Array(
                'IBLOCK_ID' => IBLOCK_ID__STRUCTURE
            ),
            Array(
                'NAME',
                'SECTION_PAGE_URL',
                'DEPTH_LEVEL'
            )
        ); ?>

        <div class="col-xs-12 col-md-4">
            <h2 class="blue-text fz36">Высшие школы и институты</h2>
            <div class="dep">
                <?
                $arDepartment_1 = $arSections['CHILDS']['132']['CHILDS']; // первый уровень
                foreach ($arDepartment_1 as $arDepartment_2) { ?>
                    <div class="dep__panel">
                        <div class="dep__header">
                            <a href="<?= $arDepartment_2["SECTION_PAGE_URL"] ?>"><?= $arDepartment_2['NAME'] ?></a>
                            <span class="dep__toggle"><i class="icon icon-plus"></i></span>
                        </div>
                        <div class="dep__content">
                            <? foreach ($arDepartment_2['CHILDS'] as $arDepartment_3) { ?>
                                <a href="<?= $arDepartment_3['SECTION_PAGE_URL'] ?>" class="dep__link"><?= $arDepartment_3['NAME'] ?></a>
                            <? } ?>
                        </div>
                    </div>
                <? } ?>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <h2 class="blue-text fz36">Подразделения</h2>
            <div class="dep accordion-js">
                <?
                $arDepartment_1 = $arSections['CHILDS']['133']['CHILDS']; // первый уровень
                foreach ($arDepartment_1 as $arDepartment_2) { ?>
                    <div class="dep__panel">
                        <div class="dep__header"><a href="<?= $arDepartment_2["SECTION_PAGE_URL"] ?>"><?= $arDepartment_2['NAME'] ?></a>
                            <span class="dep__toggle"><i class="icon icon-plus"></i></span>
                        </div>
                        <div class="dep__content">
                            <? foreach ($arDepartment_2['CHILDS'] as $arDepartment_3) { ?>
                                <a href="<?= $arDepartment_3['SECTION_PAGE_URL'] ?>" class="dep__link"><?= $arDepartment_3['NAME'] ?></a>
                            <? } ?>
                        </div>
                    </div>

                <? } ?>
            </div>
        </div>
    </div>
    <script>
        function openFirstPanel() {
            $('.dep .dep__panel:first-child').addClass('active').find('.dep__content').slideDown();
        }

        (function ($) {

            var allPanels = $('.dep .dep__panel'),
                allPanelsContent = allPanels.find('.dep__content').hide();

            openFirstPanel();

            $('.dep  .dep__header > .dep__toggle').click(function () {
                $this = $(this);
                $panel = $this.closest('.dep__panel');
                $target = $panel.find('.dep__content');

                if ($panel.hasClass('active')) {
                    $panel.removeClass('active');
                    $target.slideUp();
                } else {
                    var activePanels = $this.closest('.dep').find('.dep__panel');
                    activePanels.removeClass('active');
                    activePanels.find('.dep__content').slideUp();
                    $panel.addClass('active');
                    $target.slideDown();
                }

                return false;
            });

        })(jQuery);
    </script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>